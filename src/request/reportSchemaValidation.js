import HJoi from '@hapi/joi';
HJoi.objectId = require('joi-objectid')(HJoi);

module.exports = {
    validationSchemas: {
        create: HJoi.object().keys({
            comment: HJoi.string().trim().max(50).required().label('Comment'),
            reportedOn: HJoi.string().required().label('Reoprted for'),
            identity: HJoi.objectId().required().label('Identity'),
            silentNotification: HJoi.boolean().required().label('Notification'),
            silentUser: HJoi.boolean().required().label('User'),
        })
    }
};
