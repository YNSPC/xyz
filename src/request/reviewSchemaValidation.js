import HJoi from '@hapi/joi';

module.exports = {
    validationSchemas: {
        create: HJoi.object().keys({
            placeId: HJoi.string().trim().max(25).required().label('Place Id'),
            review: HJoi.string().trim().max(50).required().label('Review'),
            rating: HJoi.number().required().label('Rating')
        }),
        update: HJoi.object().keys({
            review: HJoi.string().trim().allow("", null).label('Review'),
            rating: HJoi.number().allow(null).label('Rating')
        })
    }
};
