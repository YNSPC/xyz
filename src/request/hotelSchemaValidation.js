import HJoi from '@hapi/joi';
HJoi.objectId = require('joi-objectid')(HJoi);

module.exports = {
    validationSchemas: {
        detail: HJoi.object().keys({
            hotelId: HJoi.objectId().required().label('Hotel id')
        })
    }
};
