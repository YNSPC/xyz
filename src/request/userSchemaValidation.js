import HJoi from '@hapi/joi';
HJoi.objectId = require('joi-objectid')(HJoi);

module.exports = {
    validationSchemas: {

        registerSchema: HJoi.object().keys({
            firstName: HJoi.string().trim().max(25).required().label('First name'),
            lastName: HJoi.string().trim().max(25).required().label('Last Name'),
            profilePicture: HJoi.string().optional().allow('').label('Profile Picture'),
            dateOfBirth: HJoi.string().optional().allow('').label('Date of birth'),
            email: HJoi.string().required().email().label('Email'),
            password: HJoi.string().trim().required().label('Password'),
            gender: HJoi.string().trim().required().label('Gender'),
            // userName: HJoi.string().trim().allow("", null).optional().min(8).max(25).label('User Name'),
            termsAndCondition: HJoi.boolean().required().label('Terms And Condition')
                .error( () => 'Please accept out terms and condition before you continue'),
            privacyPolicy: HJoi.boolean().required().label('Privacy Policy')
                .error( () => 'Please accept out privacy policy before you continue'),
            deviceId: HJoi.string().trim().required().label('Device id'),
            deviceToken: HJoi.string().trim().required().label('Device token'),
            deviceType: HJoi.number().integer().default(1).label('Device type'),
        }),

        facebookLoginSchema: HJoi.object().keys({
            access_token: HJoi.string().trim().required().label('Access token'),
            deviceId: HJoi.string().trim().required().label('Device id'),
            deviceToken: HJoi.string().trim().required().label('Device token'),//.min(150).max(500)
            deviceType: HJoi.number().integer().optional().default(1).label('Device type'),
        }),

        googleLoginSchema: HJoi.object().keys({
            accessToken: HJoi.string().trim().required().label("Access token"),
            deviceId: HJoi.string().trim().required().label("Device id"),
            deviceToken: HJoi.string()
                .required()

                .label("Device token"),
            deviceType: HJoi.number()
                .integer()
                .optional()
                .default(1)
                .label("Device type"),
        }),
            //.min(150)
            //.max(500)
        loginSchema: HJoi.object().keys({
            userName: HJoi.string().trim().required().label('userName'), //email or userName itself
            password: HJoi.string().trim().required().label('Password'),
            deviceId: HJoi.string().trim().required().label('Device id'),
            deviceToken: HJoi.string().trim().required().label('Device token'),//min(150).max(500).
            /*.error(errors => {
                return errors.map(err => {
                    switch (err.type) {
                        case "any.empty":
                            return { message: "this is to be implemented later!! YNSPC!"};
                        case "string.min":
                            return { message: `Value should have at least ${err.context.limit} characters!`};
                        case "string.max":
                            return { message: `Value should have at most ${err.context.limit} characters!`};
                        default:
                            break;
                    }
                });
            })*/
            deviceType: HJoi.number().integer().default(1).label('Device type'),
        }),

        favouriteTagSchema: HJoi.object().keys({
            tags: HJoi
                .array()
                .required()
                .items(
                    HJoi.objectId().error(() => "Please select a valid tag.")
                )
                .default([])
                .label("Tags")
        }),

        loginAdminSchema: HJoi.object().keys({
            userName: HJoi.string().trim().required().label('userName'),
            password: HJoi.string().trim().required().label('Password')
        }),
    }
};
