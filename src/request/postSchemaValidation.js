import HJoi from '@hapi/joi';
import { enums } from '../helpers/constants/database';
import { enumerations } from '../helpers/enumerations';
HJoi.objectId = require('joi-objectid')(HJoi);

const mediaUrl = HJoi
  .object()
  .keys({
    smallUrl: HJoi.string().trim().required().label('Small url link'),
    mediumUrl: HJoi.string().trim().required().label('Medium url link'),
    largeUrl: HJoi.string().trim().required().label('Large url link'),
    originalUrl: HJoi.string().trim().required().label('Original url link')
});

const media = HJoi
  .object()
  .required()
  .keys({
    mediaType: HJoi.string().trim().required().valid(...enumerations.Media_Type).label('Media Type'),
    mediaUrl: mediaUrl
})

const tripData = HJoi.object().keys({
    currentDay: HJoi.string().trim().required().label("Current Day"),
    description: HJoi.string().trim().required().label("Description"),
    location: HJoi.string().trim().required().label("Location"),
    media: HJoi.array().items(media).required(),
    tags: HJoi.array().items(HJoi.objectId()).optional().default([])
});

module.exports = {
    validationSchemas: {
        create: HJoi.object().keys({
            postType: HJoi.string().trim().valid(...enumerations.PostType).required().label('Post Type'),
            title: HJoi.string().trim().max(160).optional().allow("").label('Title'),
            description: HJoi.string().optional().allow("").label('Description'),
            visibility: HJoi.string().trim().valid(...enumerations.Visibility).required().label('Visibility'),
            location: HJoi.string().trim().optional().allow("").label('Location'),
            media: HJoi
              .when(
                'postType',
                {
                    is: enums.POST_TYPE.IMAGE_VIDEO,
                    then: HJoi
                      .array()
                      .items(media)
                    ,
                    otherwise: HJoi
                      .optional()
                      .default([])
                }),
            tags: HJoi.array().items(HJoi.objectId()).optional().default([]),
            tripStart: HJoi.string().optional().allow("").label("Start Date"),
            tripCoverPhoto: media.optional().allow("").label("Cover Photo"),
            tripInfo: HJoi.when('postType', { is: enums.POST_TYPE.TRIP, then: HJoi.array().items(tripData).required(), otherwise: HJoi.optional().default([]) }),
        })
    }
};
