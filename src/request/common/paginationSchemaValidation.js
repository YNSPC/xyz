import HJoi from '@hapi/joi';

module.exports = {
  validationSchemas: {
    paginate: HJoi.object().keys({
      limit: HJoi.number().required().label('Limit'),
      page: HJoi.number().required().label('Page'),
    })
  }
};
