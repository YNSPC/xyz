import HJoi from '@hapi/joi';
import {enums} from "../helpers/constants/database";
HJoi.objectId = require('joi-objectid')(HJoi);

const contentInComment = HJoi.object().keys({
    data: HJoi.string().trim().required().label("Data value"),
    dataType: HJoi.string().valid("text", "image").required().label("Data type")
});

module.exports = {
    validationSchemas: {
        create: HJoi.object().keys({
            postId: HJoi.objectId().required().label('Post'),
            comment: HJoi.array().items(contentInComment).required().label('Comment content'),
            replyTo: HJoi.objectId().optional().allow("").label('Replying to'),
        })
    }
};
