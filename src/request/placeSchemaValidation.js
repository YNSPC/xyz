import HJoi from '@hapi/joi';
HJoi.objectId = require('joi-objectid')(HJoi);

module.exports = {
    validationSchemas: {
        detail: HJoi.object().keys({
            placeId: HJoi.objectId().required().label('Place id')
        })
    }
};
