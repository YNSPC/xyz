import HJoi from '@hapi/joi';
HJoi.objectId = require('joi-objectid')(HJoi);

module.exports = {
    validationSchemas: {
        create: HJoi.object().keys({
            slug: HJoi.string().trim().max(50).required().label('Slug'),
            title: HJoi.string().trim().max(50).required().label('Title'),
            content: HJoi.string().trim().label('Content'),
        }),
        update: HJoi.object().keys({
            slug: HJoi.string().trim().max(50).label('Slug'),
            title: HJoi.string().trim().max(50).label('Title'),
            content: HJoi.string().trim().label('Content'),
        })
    }
};
