import HJoi from '@hapi/joi';
import { enums } from '../helpers/constants/database';
import { enumerations } from '../helpers/enumerations';
HJoi.objectId = require('joi-objectid')(HJoi);

const memberData = HJoi.object().keys({
    userId: HJoi.objectId().required().label("Requested member id"),
    invitedOn: HJoi.string().required().label("Invited date"),
    updatedAt: HJoi.string().required().label("Updated date"),
    status: HJoi.string().trim().required().label("Member status"),
});

const eventLocation = HJoi.object().keys({
    address: HJoi.string().required().label("Location name"),
    location: HJoi.object().keys({
        type: HJoi.string().required().label("Location type"),
        coordinates: HJoi.array().items(
          HJoi.number()
            .min(-180)
            .max(180)
            .required(),
          HJoi.number()
            .min(-90)
            .max(90)
            .required()
        )
    })
}).required();

const eventOptionalLocation = HJoi.object().keys({
    address: HJoi.string().required().label("Location name"),
    location: HJoi.object().keys({
        type: HJoi.string().required().label("Location type"),
        coordinates: HJoi.array().items(
          HJoi.number()
            .min(-180)
            .max(180)
            .required(),
          HJoi.number()
            .min(-90)
            .max(90)
            .required()
        )
    })
});

module.exports = {
    validationSchemas: {
        create: HJoi.object().keys({
            name: HJoi.string().trim().min(4).required().label("Event name"),
            totalTravellers: HJoi.number().required().label("Total traveller"), 
            description: HJoi.string().trim().optional().label("Event description"),
            duration: HJoi.number().required().label("Event duration"),
            date: HJoi.string().trim().required().label("Event date"),
            period: HJoi.string().trim().required().label("Event period"),
            from: eventLocation,
            to: eventLocation,
            categories: HJoi.array().items(HJoi.objectId().label("CategoryId")).required(),
            picture: HJoi.string().trim().required().label("Event picture"),
            // organizer: HJoi.objectId().label("Organizer"),
            // status:  HJoi.string().trim().required().label("Event status"),
            // location: HJoi.string().trim().required().label("Location"),
            // private: HJoi.boolean().required().label("Private state"),
            members: HJoi.array().items(memberData)//.required()
        }),

        update: HJoi.object().keys({
            name: HJoi.string().trim().min(4).label("Event name"),
            totalTravellers: HJoi.number().label("Total traveller"), 
            description: HJoi.string().trim().optional().label("Event description"),
            duration: HJoi.number().label("Event duration"),
            date: HJoi.string().trim().label("Event date"),
            period: HJoi.string().trim().label("Event period"),
            from: eventOptionalLocation,
            to: eventOptionalLocation,
            categories: HJoi.array().items(HJoi.objectId().label("CategoryId")),
            picture: HJoi.string().trim().label("Event picture"),
            // organizer: HJoi.objectId().label("Organizer"),
            // status:  HJoi.string().trim().label("Event status"),
            // location: HJoi.string().trim().label("Location"),
            // private: HJoi.boolean().label("Private state"),
            members: HJoi.array().items(memberData)//
        }),

        invitationToEvent: HJoi.object().keys({
            eventId: HJoi.objectId().required().label("Event id"),
            userId: HJoi.objectId().required().label("User id"),
        }),

        responseToInvitation: HJoi.object().keys({
            eventId: HJoi.objectId().required().label("Event id"),
            // userId: HJoi.objectId().required().label("User"),
            status: HJoi.string().trim().required().label("Member status"),
        }),

        mongoId: HJoi.object().keys({
          eventId: HJoi.objectId().required().label("Identity"),
        })
    }
};