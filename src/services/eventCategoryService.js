import {
    listEventCategory,
} from "../repositories/eventCategoryRepository";

export const listEventCategoryService = async (payload) => {
    return listEventCategory(payload);
};
