import {upperCaseFirstLetter} from "../helpers/common-scripts";
import {notifications} from "../helpers/constants/notification";
import {sendMessage} from "../modules/googleFcm";

const getNotificationTemplate = (type) => {
    return notifications[type];
};

const parseMessage = (messageToParse, mapObject) => {
    const regularExpression = new RegExp(Object.keys(mapObject).join("|"),"gi");

    return messageToParse.replace(regularExpression, function(matched){
        return mapObject[matched.toLowerCase()];
    });
};

export const sendNotification = async ({ notificationType, fcmTokens, mapObject, notificationData }) => {
    if ( userInfoOfOneWeWantToFollow.fcmTokens.length > 0 ) {
        const notificationData = {};
        const mapObject = {
            "#user": `${upperCaseFirstLetter(loggedUser.firstName)} ${upperCaseFirstLetter(loggedUser.lastName)}`
        };
        const notificationTemplate = notifications.follow;
        const message = parseMessage(notificationTemplate.message, mapObject);
        try {
            const fcmResponse = await sendMessage({
                tokens: fcmTokens,
                badge: 1,
                title,
                message,
                notificationData
            });
            console.log("fcm response", fcmResponse);
        }
        catch (e) {
            console.log("fcm error handled", e)
        }
    }
}
