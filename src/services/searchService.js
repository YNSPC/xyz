import { searchedUser } from "../repositories/userRepository";
import { searchedPlace } from "../repositories/placesRepository";
  
export const placeAndUserSearchService = async (searchQuery, searcher) => {
    const userSearch = await searchedUser(searchQuery.search, searcher);
    const placeSearch = await searchedPlace(searchQuery.search);

    return {
        users: userSearch,
        places: placeSearch
    }
};
