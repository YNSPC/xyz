import { systemConfig } from "../config"
import { currentDateTimeConsole } from "../utils/dateUtil"
import { COLORS } from "../console/config"
import colorsSchemes from 'colors';

export const logging = ({ action, data, isLoggableOverride= false }) => {
    if ( !systemConfig.isLoggable && !isLoggableOverride ) return false;

    switch (action) {
        case "info":
            console.info(`${`[ ${currentDateTimeConsole} ]:`.green.bold} ${data.white.bold}`)
            break;

        case "error":
            console.error(`${`[ ${currentDateTimeConsole} ]:`.red.bold} ${data.red.bold}`)
            break;

        case "warning":
            console.warn(data);
            break;

        case "debug":
            console.debug(
                `[ ${currentDateTimeConsole.yellow.bold} ]: \n`,
                "Data Type".green, typeof data, '\n',
                "Relative Data Type".blue, Object.prototype.toString.call(data), '\n',
                "Content".green, data, '\n'
            )
            break;

        case "table":
            console.table(data);
            break;

        case "trace":
            console.trace(data);
            break;

        case "group":
            console.group()
            break;

        case "groupEnd":
            console.groupEnd()
            break;

        case "groupCollapsed":
            console.groupCollapsed()
            break;

        case "profile":
            break;

        case "profileEnd":
            break;

        case "time":
            break;

        case "timeEnd":
            break;

        case "count":
            break;

        case "countEnd":
            break;

        default:
            // console.log( COLORS.bright, COLORS.BGwhite, COLORS.black, data, COLORS.reset);
            console.log(`${`[ ${currentDateTimeConsole} ]:`.yellow.bold} ${data.yellow.bold}`)
    }
};
