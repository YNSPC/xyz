import {deepCopy, getBaseUrl, getValidPage, sallowCopy} from "../helpers/common-scripts";
import {
    createReport,
    paginatedListing
} from "../repositories/reportRepository";

import {
    findPostByIdPromise, 
    getReportDetailViaPost
} from "../repositories/postRepository";
import {projections} from "../helpers/projections/projection";

export const addReportService = async (payload, reporter) => {
    const data = deepCopy(payload);
    data.reportedBy = reporter._id;

    return await createReport(data);
};

export const listReportService = async (requestQuery) => {
    const page = await getValidPage(requestQuery.page);
    const { sortBy, sortDesc, limit, search } = requestQuery;

    const options = {
        limit,
        page,
        search,
        sorting: sortBy && sortDesc
            ? { [sortBy]: sortDesc }
            : { name: "asc" }
    };

    return paginatedListing(options);
};

export const reportDetailService = async (requestQuery) => {
    let detail = {};
    switch (requestQuery.reportedOn) {
        case "post_old":
            detail = findPostByIdPromise(requestQuery.identity)
                .populate({
                    path: 'author',
                    select: {
                        ...projections.USER.PROFILE_PICTURE,
                        _id: 1,
                        fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
                    }
                })
                // .populate("totalComments")
                .select({
                    _id: 1,
                    title: 1,
                    media: {
                        $map:{
                            input: "$media",
                            as: "row",
                            in: {
                                _id: "$$row._id",
                                mediaType: "$$row.mediaType",
                                mediaUrl: {
                                    originalUrl: { $concat: [ getBaseUrl(), "$$row.mediaUrl.originalUrl" ] },
                                    smallUrl: { $concat: [ getBaseUrl(), "$$row.mediaUrl.smallUrl" ] },
                                    mediumUrl: { $concat: [ getBaseUrl(), "$$row.mediaUrl.mediumUrl" ] },
                                    largeUrl: { $concat: [ getBaseUrl(), "$$row.mediaUrl.largeUrl" ] },
                                },
                            }
                        }
                    },
                    createdAt: 1,
                    updatedAt: 1,
                    postType: 1,
                    description: 1,
                    location: 1,
                    totalLikes: { $size: "$likedBy" }
                })
                
                ;
            break;

        case "post":
            detail = await getReportDetailViaPost(requestQuery.identity);
            break;
    }

    return detail;
};