import {
  findRemoteConfig
} from "../repositories/sessionRepository";

export const getRemoteConfigService = async () => {
  return findRemoteConfig();
};
