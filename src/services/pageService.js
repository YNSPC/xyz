import Errors from "../helpers/errors";

export const createPageService = async (payload) => {
    if (![].includes(payload.slug))
        throw Errors.customError("", 422);
};
