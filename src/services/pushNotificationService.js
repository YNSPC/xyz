import { notifications } from "../helpers/constants/notification";
import { parseMessage, sendMessage } from "../modules/googleFcm";
import { createNotification } from "../repositories/notificationRepository";
import { getMyFollowerList, userDetailWithFcm, usersFcmTokens } from "../repositories/userRepository"

const handlePushNotificationService = async (segment, receiverFcms, notificationSlug, payload) => {
    let notificationData = {};
    let mapObject = {};
    let notificationTemplate = "";

    if (notificationSlug === "newEvent") {
        notificationData.eventId = payload.eventId;
    
        mapObject["#event"] = `${payload.eventName}`;
        mapObject["#organizer"] = payload.organizerName;
    
        notificationTemplate = notifications.newEventCreated;
    } else if (notificationSlug === "joinEvent") {
        notificationData.eventId = payload.eventId;
    
        mapObject["#event"] = `${payload.eventName}`;
        mapObject["#member"] = payload.memberName;
    
        notificationTemplate = notifications.joinEvent;
    }

    const message = parseMessage(notificationTemplate.message, mapObject);
    const title = parseMessage(notificationTemplate.title, mapObject);
    const notificationPayload = {
        sender: payload.sender,
        receiver: payload.receiver,
        title,
        data: notificationData,
        notificationType: notificationSlug,
        segment: segment
    };

    await createNotification(notificationPayload);

    try {
        const fcmResponse = await sendMessage({
            tokens: receiverFcms,
            badge: 1,
            title: parseMessage(notificationTemplate.title, mapObject),
            message: message,
            notificationData
        });
        console.log("fcm response", fcmResponse);
    }
    catch (e) {
        console.log("fcm error handled", e.message)
    }
}

export const handleEventCreatedNotificationService = async ({event, loggedUser}) => {
    // event organizers followers, will be list of followers and their list of fcms
    const followersOfEventOrganizer = await getMyFollowerList(event.organizer);
    const userWithFollowersFcms = await usersFcmTokens(followersOfEventOrganizer.followers.map(follower => follower.userId));
    // event detail is already present
    console.log(followersOfEventOrganizer.followers.map(follower => follower.userId));
    console.log(userWithFollowersFcms);
    if (userWithFollowersFcms.length > 0) {
        for (const followerFcm of userWithFollowersFcms) {
            if(followerFcm.fcmTokens.length > 0) {
                const payload = {
                    eventId: event._id.toString(),
                    eventName: event.name,
                    organizerName: `${followersOfEventOrganizer.firstName} ${followersOfEventOrganizer.lastName}`,
                    sender: event.organizer,
                    receiver: followerFcm._id,
                }
                handlePushNotificationService("event", followerFcm.fcmTokens, 'newEvent', payload);
            }
        }
    }
}

export const handleEvenJoinNotificationService = async ({event, loggedUser}) => {
    const organizerFcms = await userDetailWithFcm({ userId: event.organizer }, { showTag: false, showFcm: true});
    if(organizerFcms.fcmTokens.length > 0) {
        const payload = {
            eventId: event._id.toString(),
            eventName: event.name,
            memberName: `${loggedUser.firstName} ${loggedUser.lastName}`,
            sender: loggedUser._id,
            receiver: event.organizer,
        }
        handlePushNotificationService("event", organizerFcms.fcmTokens, 'joinEvent', payload);
    }
}
