import uuid from "uuid/v4";
import { objectId } from "../helpers/common-scripts";
import { 
  createChat,
  isFirstChat,
  paginatedChatHistory,
  paginatedChatUserListing,
  updateChatUserList,
  updateOrCreateUserHistory,
  removeChatFromMyself
} from "../repositories/chatRepository";

export const saveChatService = async (payload) => {
  const chatPayload = {
    ...payload,
    sender: payload.sender.id,
    messageId: uuid()
  };

  if (isFirstChat(payload)) {
    const filterForSender = {
      _id: objectId(payload.sender.id)
    };
    const updateQueryForSender = {
      $addToSet: {
        chatWith: payload.receiver
      }
    };

    await updateChatUserList(filterForSender, updateQueryForSender);
    const filterForReceiver = {
      _id: objectId(payload.receiver)
    };
    const updateQueryForReceiver = {
      $addToSet: {
        chatWith: payload.sender.id
      }
    };
    await updateChatUserList(filterForReceiver, updateQueryForReceiver);
  }

  // await updateOrCreateUserHistory(payload);

  return createChat(chatPayload);
};

export const editChatService = async (user, payload, options) => {};

export const deleteChatService = async (user) => {};

export const chatHistoryService = async (user, query, param) => {
  const options = {
    loggedUser: user,
    friendId: param.friendId,
    limit: query.limit,
    page: query.page
  };

  return paginatedChatHistory(options);
};

export const chatUserListingService = async (user, query) => {
  const options = {
    loggedUser: user,
    limit: parseInt(query.limit),
    page: parseInt(query.page)
  };

  const chatResult = await paginatedChatUserListing(options);
  return chatResult;
};

export const removeChatFromMyselfService = async (user, friendId) => {
  return await removeChatFromMyself(user._id, objectId(friendId));
};