import mongoose from "mongoose";
import PostModel from "../database/models/Posts";
import UserModel from "../database/models/Users";
import Errors from "../helpers/errors";
import {notifications} from "../helpers/constants/notification";
import {parseMessage, sendMessage} from "../modules/googleFcm";
import {getUserDeviceToken} from "../repositories/userRepository";
import {findPostById, deletePostById} from "../repositories/postRepository";
import {getBaseUrl, upperCaseFirstLetter} from "../helpers/common-scripts";
import {awsConfig} from "../config";
import {createNotification} from "../repositories/notificationRepository";
import { deleteAwsS3Content, deleteFromAllVersions } from "../modules/awsService";

export const createPostService = async (payload, user) => {
    const postData = {
        postType: payload.postType,
        title: payload.title,
        description: payload.description || '',
        visibility: payload.visibility || "public",
        status: true,
        // location: {type: locationSchema},
        editedAt: null,
        totalShares: 0,
        totalComments: 0,
        totalLikes: 0,
        sharedBy: [],
        likedBy: [],
        hiddenBy: [],
        reportedBy: [],
        tags: [],
        author: user._id,
        media: [
            {
                mediaType: "image",
                mediaUrl: {
                    smallUrl: "small-url",
                    mediumUrl: "medium-url",
                    largeUrl: "large-url",
                    originalUrl: "original-url",
                }
            }
        ],
        tripStart: new Date(),
        tripCoverPhoto: {
            mediaType: "image",
            mediaUrl: {
                smallUrl: "small-url",
                mediumUrl: "medium-url",
                largeUrl: "large-url",
                originalUrl: "original-url",
            }
        },
        tripInfo: [
            {
                currentDay: "day1",
                description: "task in day 1",
                // location: {type: locationSchema},
                media: [
                    {
                        mediaType: "image",
                        mediaUrl: {
                            smallUrl: "small-url",
                            mediumUrl: "medium-url",
                            largeUrl: "large-url",
                            originalUrl: "original-url",
                        }
                    }
                ],
                tags: []
            }
        ]
    };
    const postObject = {
        ...payload,
        author: user._id
    }
    const myPost = new PostModel(postObject);
    await myPost.save();
    return myPost;
};

export const likePostService = async (postId, user) => {
    try {
        const likedPost = await PostModel.findOne(
            {
                _id: mongoose.Types.ObjectId(postId),
                likedBy: {$in: [user._id]}
            }
        );
        if (likedPost) throw Errors.customError("You have already liked this post.", 422);

        // TODO: these needs to be in repository 
        const likeAPost = await PostModel.findOneAndUpdate(
            {
                _id: mongoose.Types.ObjectId(postId)
            },
            {
                $push: {
                    likedBy: user._id
                }
            },
            {
                new: true,
                useFindAndModify: false
            }
        );
        //send notification to the followed user
        if (user._id.toString() !== likeAPost.author.toString()) {
            const messageToUserList = await getUserDeviceToken(likeAPost.author);
            if ( messageToUserList[0].fcmTokens.length > 0 ) {
                const notificationData = {
                    postId: postId,
                    userName: `${user.firstName} ${user.lastName}`,
                    userProfilePicture: `${user.profilePicture}`,
                };
                const mapObject = {
                    "#user": `${upperCaseFirstLetter(user.firstName)} ${upperCaseFirstLetter(user.lastName)}`,
                    "#title": likeAPost.title || 'n/a',
                };
                const notificationTemplate = notifications.like;
                const message = parseMessage(notificationTemplate.message, mapObject);
                const title = parseMessage(notificationTemplate.title, mapObject);
                const notificationPayload = {
                    sender: user._id,
                    receiver: likeAPost.author,
                    title,
                    data: notificationData,
                    notificationType: "like"
                };
                await createNotification(notificationPayload);

                try {
                    const fcmResponse = await sendMessage({
                        tokens: messageToUserList[0].fcmTokens,
                        badge: 1,
                        title: parseMessage(notificationTemplate.title, mapObject),
                        message: message,
                        notificationData
                    });
                    console.log("fcm response", fcmResponse);
                }
                catch (e) {
                    console.log("fcm error handled", e.message)
                }

            }
        }

        return likeAPost;
    }
    catch (e) {
        console.log("like post error", e.message)
    }
};

export const unLikePostService = async (postId, user) => {
    const post = await PostModel.findOneAndUpdate(
        {
            _id: mongoose.Types.ObjectId(postId)
        },
        {
            $pull: {
                likedBy:  user._id
            }
        },
        {
            new: true,
            useFindAndModify: false
        }
    );

    return post;
};

export const postFromUserService = async (filter, paginationOption, loggedUser) => {
    const followingIds = loggedUser.followings.map( following => following.userId );

    const stages = [];

    stages.push({
        $match: {
            author: mongoose.Types.ObjectId(filter)
        }
    });

    stages.push({
        $lookup: {
            from: "users",
            let: { "userId": "$author" },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: [ "$_id", "$$userId" ]
                        }
                    }
                },
                {
                    $project: {
                        _id: 1,
                        userName: 1,
                        profilePicture: 1,
                        fullName: { $concat: [ "$firstName", " ", "$lastName" ] },

                    }
                }
            ],
            as: "authorDetail"
        }
    });

    stages.push({
        $unwind: "$authorDetail"
    });

    stages.push({
        $project: {
            postType: 1,
            location: 1,
            title: 1,
            description: 1,
            visibility: 1,
            status: 1,
            editedAt: 1,
            totalShares: 1,
            totalComments: 1,
            totalLikes: 1,
            sharedBy: 1,
            likedBy: 1,
            hiddenBy: 1,
            reportedBy: 1,
            tags: 1,
            author: 1,
            tripCoverPhoto: 1,
            tripInfo: 1,
            createdAt: 1,
            media: {
                $map:{
                    input: "$media",
                    as: "row",
                    in: {
                        _id: "$$row._id",
                        mediaType: "$$row.mediaType",
                        mediaUrl: {
                            originalUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.originalUrl" ] },
                            smallUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.smallUrl" ] },
                            mediumUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.mediumUrl" ] },
                            largeUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.largeUrl" ] },
                        },
                    }
                }
            },
            authorDetail: 1,
            totalLike: { $size: "$likedBy" },
            hasLiked: {
                $in: [ loggedUser._id, "$likedBy" ]
            },
            hasFollowed: {
                $cond: {
                    if: { $eq: [ loggedUser._id, "$authorDetail._id" ] },
                    then: true,
                    else: {
                        $in: [ "$authorDetail._id", followingIds ]
                    }
                }
            }
        }
    });

    const aggregatedPosts = PostModel.aggregate(stages);

    return PostModel.aggregatePaginate(aggregatedPosts, paginationOption);
};

export const postDetailService = async (postId, loggedUser) => {
    const followingIds = loggedUser.followings.map( following => following.userId );
    return PostModel
        .findOne(
            { _id: postId },
            {
                _id: 1,
                postType: 1,
                location: 1,
                title: 1,
                description: 1,
                visibility: 1,
                status: 1,
                editedAt: 1,
                sharedBy: 1,
                likedBy: 1,
                hiddenBy: 1,
                reportedBy: 1,
                tags: 1,
                tripCoverPhoto: 1,
                tripInfo: 1,
                createdAt: 1,
                media: {
                    $map:{
                        input: "$media",
                        as: "row",
                        in: {
                            _id: "$$row._id",
                            mediaType: "$$row.mediaType",
                            mediaUrl: {
                                originalUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.originalUrl" ] },
                                smallUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.smallUrl" ] },
                                mediumUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.mediumUrl" ] },
                                largeUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.largeUrl" ] },
                            },
                        }
                    }
                },
                authorDetail: "$author",
                totalLike: { $size: "$likedBy" },
                hasLiked: {
                    $in: [ loggedUser._id, "$likedBy" ]
                },
                hasFollowed: {
                    $cond: {
                        if: { $eq: [ loggedUser._id, "$author" ] },
                        then: true,
                        else: {
                            $in: [ "$author", followingIds ]
                        }
                    }
                }
            }
        )
        .populate({
            path: 'authorDetail',
            model: "User",
            select: {
                _id: 1,
                userName: 1,
                profilePicture: 1,
                fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
            }
        })
};

export const likesInPostService = async (postId, paginationOption) => {
    const postDetail = await findPostById(postId);
    const stages = [];

    stages.push({
        $match: {
            $expr: {
                $in: [ "$_id", postDetail.likedBy ]
            }
        }
    });

    stages.push({
        $project: {
            _id: 1,
            fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
            profilePicture: { $concat: [ getBaseUrl("aws"), "$profilePicture" ] },
        }
    });

    const aggregatedUsers = UserModel.aggregate(stages);
    return UserModel.aggregatePaginate(aggregatedUsers, paginationOption);
};

export const deletePostService = async (postId, loggedUser) => {
    const postDetail = await findPostById(postId);
    
    if(!postDetail) throw Errors.customError("Post you are trying to delete is not found.", 404);

    if ( postDetail.author.toString() !== loggedUser._id.toString()) throw Errors.customError("Access denied.", 503);

    const deletedPost = await deletePostById(postId);

    console.log(deletedPost, postDetail);

    if (deletedPost) {
        if(Array.isArray(postDetail.media) && postDetail.media.length > 0) {
            for (const media of postDetail.media) {
                console.log("key", media.mediaUrl.originalUrl);
                // await deleteAwsS3Content(media.mediaUrl.originalUrl);
                await deleteFromAllVersions(media.mediaUrl.originalUrl);
            }
        }
    }

    return deletedPost;
};
