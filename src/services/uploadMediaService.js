import uuid from "uuid/v1";
import { generateGoogleSignedUrl } from "../modules/googleCloudService";
import { generateAwsS3SignedUrl, generateAwsS3ViewSignedUrl } from "../modules/awsService";
import { systemConfig } from "../config";

export const generateSignedUrl = async (queryData) => {
    const folder = queryData.folder;
    const contentType = queryData.contentType;
    const extension = queryData.extension;
    const key = `${folder}/${uuid()}.${extension}`;

    if (!["posts", "places", "tags", "users", "hotels", "events"].includes(folder))
        throw new Error("invalid folder provided. use [posts/ tags/ places/ users/ hotels / events]")

    switch(systemConfig.mediaService) {
        case "aws":
                return await generateAwsS3SignedUrl(key, contentType);

        case "google":
                return await generateGoogleSignedUrl('yatribhet', key, contentType);

        default:
            throw new Error("Invalid service provided.");
    }
};

export const generateSignedViewUrl = async (queryData) => {
    const key = queryData.key;

    switch(systemConfig.mediaService) {
        case "aws":
                return await generateAwsS3ViewSignedUrl(key);

        case "google":
                return await generateGoogleSignedUrl('yatribhet', key, contentType);

        default:
            throw new Error("Invalid service provided.");
    }
};
