import mongoose from "mongoose";
import CommentModel from "../database/models/Comments";
import { findCommentById, getAllComments } from "../repositories/commentRepository";
import {
    userDetailWithFcmWithSession,
    followUnfollowRemoveUser as followUnfollowRemoveUserWithSession,
    createNotification as createNotificationWithSession,
    followUserInFollowings as followUserInFollowingsWithSession,
    followUserInFollowers as followUserInFollowersWithSession
} from "../repositories/withSession/userRepository";
import {findPostById} from "../repositories/postRepository";
import Errors from "../helpers/errors";
import {
    getUserDeviceToken, 
    userOfPostWithFcm
} from "../repositories/userRepository";
import { getBaseUrl, upperCaseFirstLetter } from "../helpers/common-scripts";
import { notifications} from "../helpers/constants/notification";
import { parseMessage, sendMessage } from "../modules/googleFcm";
import emailValidator from "deep-email-validator"

export const createCommentService = async (payload, user, session) => {
    let isReply = false;
    const commentData = {
        post: payload.postId,
        comment: payload.comment,
        commentedBy: user._id,
        editedAt: null,
    };
    // check if the post exists
    const post = await findPostById(payload.postId);
    if ( !post ) throw Errors.customError("Unable to find to requested post.", 422);

    // replying to comment
    if ( payload.replyTo ) {
        isReply = true;
        const commentDetail = await findCommentById(payload.replyTo);
        commentData.replyTo = commentDetail.replyTo ? commentDetail.replyTo : payload.replyTo;
    }

    const myComment = new CommentModel(commentData);
    await myComment.save();
    //TODO: send notification when comment is created.
    const userDetailToSendNotification = await userOfPostWithFcm(post._id, [user._id]);
    const notificationTemplate = notifications.comment;
    const mapObject = {
        "#user": `${upperCaseFirstLetter(user.firstName)} ${upperCaseFirstLetter(user.lastName)}`
    };
    const title = parseMessage(notificationTemplate.title, mapObject);
    const message = "";
    const notificationData = {
        postId: payload.postId,
        userName: `${user.firstName} ${user.lastName}`,
        userProfilePicture: `${user.profilePicture}`,
    };
    const notificationPayload = {
        sender: user._id,
        receiver: user._id,
        title,
        data: notificationData,
        notificationType: "comment"
    };

    if ( userDetailToSendNotification.fcmTokens.length > 0 ) {
        try {
            const fcmResponse = await sendMessage({
                tokens: userDetailToSendNotification.fcmTokens,
                badge: 1,
                title,
                message,
                notificationData
            });
            console.info("Fcm response: ", fcmResponse);
            const notificationStatus = await createNotificationWithSession(notificationPayload, session);
            // if ( !notificationStatus ) 
            //     throw Errors.customError("Unable to create notification.", 422);
        }
        catch (exception) {
            console.log("Error sending FCM", 
                exception, 
                exception.error.errorInfo.message, 
                exception.success
            );
            // throw Errors.customError(exception.error.errorInfo.message, 500);
            // throw Errors.customError(exception.message, 500)
        }
    }

    return CommentModel
        .findOne({ _id: myComment._id})
        .populate({
            path: "commentedBy",
            select: {
                _id: 1,
                fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
                profilePicture: { $concat: [ getBaseUrl(), "$profilePicture" ] },
            }
        })
        .select({
            likedBy: 1,
            replyTo: 1,
            comment:1,
            createdAt: 1,
            hasLiked: {
                $in: [ user._id, { $ifNull: ["$likedBy", []] } ]
            },
            totalLike: {
                $ifNull: [
                    {
                        $size: "$likedBy"
                    },
                    0
                ]
            },
            repliesOnComment: []
        });
};

export const commentListingService = async (postId, options, loggedUser) => {
    const postDetail = await findPostById(postId);
    if ( !postDetail ) throw Errors.customError("Unable to find the requested post.", 422);

    return getAllComments(postDetail, options, loggedUser);
};

export const likeCommentService = async (commentId, user) => {
    // try {
    const likedComment = await CommentModel.findOne(
        {
            _id: mongoose.Types.ObjectId(commentId),
            likedBy: {$in: [user._id]}
        }
    );
    if (likedComment) throw Errors.customError("You have already liked this comment.", 412);

    const likeComment = await CommentModel.findOneAndUpdate(
        {
            _id: mongoose.Types.ObjectId(commentId)
        },
        {
            $push: {
                likedBy: user._id
            }
        },
        {
            new: true,
            useFindAndModify: false
        }
    );
    //send notification to the followed user
    /*const messageToUserList = await getUserDeviceToken(likeAPost.author);
    if ( messageToUserList[0].fcmTokens.length > 0 ) {
        const notificationData = {};
        const mapObject = {
            "#user": `${upperCaseFirstLetter(user.firstName)} ${upperCaseFirstLetter(user.lastName)}`,
            "#title": likeAPost.title || 'n/a',
        };
        const notificationTemplate = notifications.like;
        const message = parseMessage(notificationTemplate.message, mapObject);

        try {
            const fcmResponse = await sendMessage({
                tokens: messageToUserList[0].fcmTokens,
                badge: 1,
                title: parseMessage(notificationTemplate.title, mapObject),
                message: message,
                notificationData
            });
            console.log("fcm response", fcmResponse);
        }
        catch (e) {
            console.log("fcm error handled", e.message)
        }

    }*/

    return CommentModel
        .findOne({ _id: likeComment._id})
        .populate({
            path: "commentedBy",
            select: {
                _id: 1,
                fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
                profilePicture: 1,
            }
        })
        .select({
            likedBy: 1,
            replyTo: 1,
            comment:1,
            createdAt: 1,
            totalLike: {
                $ifNull: [
                    {
                        $size: "$likedBy"
                    },
                    0
                ]
            },
            repliesOnComment: [],
            hasLiked: {
                $in: [ user._id, { $ifNull: ["$likedBy", []] } ]
            },
        });
    /*}
    catch (e) {
        console.log("like post error", e.message)
    }*/
};

export const unLikeCommentService = async (commentId, user) => {
    const comment = await CommentModel.findOneAndUpdate(
        {
            _id: mongoose.Types.ObjectId(commentId)
        },
        {
            $pull: {
                likedBy:  user._id
            }
        },
        {
            new: true,
            useFindAndModify: false
        }
    );

    return CommentModel
        .findOne({ _id: comment._id})
        .populate({
            path: "commentedBy",
            select: {
                _id: 1,
                fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
                profilePicture: 1,
            }
        })
        .select({
            likedBy: 1,
            replyTo: 1,
            comment:1,
            createdAt: 1,
            hasLiked: {
                $in: [ user._id, { $ifNull: ["$likedBy", []] } ]
            },
            totalLike: {
                $ifNull: [
                    {
                        $size: "$likedBy"
                    },
                    0
                ]
            },
            repliesOnComment: []
        });
};

export const validateEmail = async (email) => {
    return emailValidator(email)
};
