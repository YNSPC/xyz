import { findUserByEmail } from "../repositories/userRepository";
import {FORGOT_PASSWORD} from "../helpers/constants/server-messages";
import Errors from "../helpers/errors";

export const forgotPasswordService = async email => {
    //check if the provided email exists in our system.
    const userDetail = await findUserByEmail(email);
    if (!userDetail) throw Errors.customError(FORGOT_PASSWORD.EMAIL_NOT_FOUND.message, FORGOT_PASSWORD.EMAIL_NOT_FOUND.httpCode);

    const template = await getTemplateBySlug("forgot-password-email"); //update later
    const emailContent = {};
    emailContent.content = template.data.content;
    emailContent.keywords = template.data.keywords;
    emailContent.onboardingName = onboarder.merchantName;
  
    const tokenizedUser = {
      user: onboarder._id,
      email: onboarder.email,
      onboardingRequestId: onboarder.onboardingRequestId,
    };
  
    const onboarderVerificationToken = await generateJWTToken(
      tokenizedUser,
      "forgotPasswordToken",
      true
    );
  
    let verificationLink = "";
  
    verificationLink = `${process.env.APP_ONBOARDING_URL}/reset-password-email/${onboarderVerificationToken}`;
    emailContent.url = verificationLink;
    const content = await parseContent(emailContent, "reset-password-email");
    const locals = {
      content,
    };
  
    const subject = "Reset Password";
    sendMail(onboarder.email, subject, locals);

    
};
