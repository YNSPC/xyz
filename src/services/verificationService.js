import {enums, system} from "../helpers/constants/database";
import {SERVER_ERROR, USER, VERIFICATION} from "../helpers/constants/server-messages";
import {findUserByEmail, findUserById, updateUserModel} from "../repositories/userRepository";
import {createToken, findByToken, findByTokenHolderAndType, removeToken} from "../repositories/tokenRepository";
import {generateJwtToken, verifyJwtToken} from "../modules/token";
import {systemConfig} from "../config";
import {sendMail} from "../modules/nodeMailer";

const __handleEmailVerification = async token => {
    const validateJwt = await verifyJwtToken(token, system.TOKEN.VERIFICATION_TOKEN.ACTION);
    if ( !validateJwt.success ) return {
        success: false,
        statusCode: VERIFICATION.TOKEN_EXPIRED.httpCode,
        message: VERIFICATION.TOKEN_EXPIRED.message
    };

    const verifyingToken = await findByToken(token);
    if ( !verifyingToken ) return {
        success: false,
        statusCode: VERIFICATION.TOKEN_INVALID.httpCode,
        message: VERIFICATION.TOKEN_INVALID.message,
        data: {}
    };

    const userData = await findUserById(verifyingToken.tokenHolder);
    if ( !userData ) return {
        success: false,
        statusCode: VERIFICATION.TOKEN_INVALID.httpCode,
        message: VERIFICATION.TOKEN_INVALID.message,
        data: {}
    };

    const verifyUser = await updateUserModel(
        { _id: userData._id },
        {
            status: enums.USER_STATUS.ACTIVE,
            emailVerifiedAt: new Date()
        }
    );

    return {
        success: true,
        statusCode: VERIFICATION.ACCOUNT.ACCOUNT_VERIFIED.httpCode,
        message: VERIFICATION.ACCOUNT.ACCOUNT_VERIFIED.message,
        data: verifyUser
    };
};

const __handleResendEmailVerification = async email => {
    const verifyingEmailUser = await findUserByEmail(email);
    if ( !verifyingEmailUser ) return {
        success: false,
        statusCode: USER.EMAIL_NOT_EXISTS.httpCode,
        message: USER.EMAIL_NOT_EXISTS.message,
        data: {}
    };

    if ( verifyingEmailUser && verifyingEmailUser.emailVerifiedAt ) return {
        success: false,
        statusCode: VERIFICATION.ACCOUNT.ACCOUNT_ALREADY_VERIFIED.httpCode,
        message: VERIFICATION.ACCOUNT.ACCOUNT_ALREADY_VERIFIED.message,
        data: {}
    };

    const userOldToken = await findByTokenHolderAndType(verifyingEmailUser._id, system.TOKEN.VERIFICATION_TOKEN.ACTION);
    if ( userOldToken ) await removeToken(userOldToken.token);

    const token = await generateJwtToken(
        {
            user: verifyingEmailUser._id,
            email: verifyingEmailUser.email
        },
        system.TOKEN.VERIFICATION_TOKEN.ACTION
    );
    const jsDate = new Date();
    await createToken({
        token,
        tokenHolder: verifyingEmailUser._id,
        tokenHolderType: "application",
        tokenType: system.TOKEN.VERIFICATION_TOKEN.ACTION,
        expiresAt: jsDate.setTime(jsDate.getTime() + (2 * 60 * 1000))
    });
    const verificationLink = `${systemConfig.apiBaseUrl}/v1/api/verification/account/${token}`;
    let emailObject = {
        from: 'no-reply@yatribhet.com',
        to: email,
        subject: 'Email verification',
        template: 'email_verification',
        emailDataObject: {
            userName: verifyingEmailUser.firstName,
            projectName: systemConfig.systemName,
            emailVerificationLink: verificationLink,//customGenerator.linkGenerate('emailVerification', randomUUID),
            attachments: []
        }
    };

    sendMail(emailObject);

    return {
        success: true,
        statusCode: 200,
        message: "New verification is sent to you in your email.",
        data: {}
    };
};

const __handleForgotPasswordVerification = async token => {
	const validateJwt = await verifyJwtToken(token, system.TOKEN.FORGOT_PASSWORD_TOKEN.ACTION);
  if (!validateJwt.success)
    return {
      success: false,
      statusCode: VERIFICATION.TOKEN_EXPIRED.httpCode,
      message: VERIFICATION.TOKEN_EXPIRED.message,
      ...validateJwt
    };

  const verifyingToken = await findByToken(token);
  if (!verifyingToken)
    return {
      success: false,
      statusCode: VERIFICATION.TOKEN_INVALID.httpCode,
      message: VERIFICATION.TOKEN_INVALID.message,
      data: {},
    };

  const userData = await findUserById(verifyingToken.tokenHolder);
  if (!userData)
    return {
      success: false,
      statusCode: VERIFICATION.TOKEN_INVALID.httpCode,
      message: VERIFICATION.TOKEN_INVALID.message,
      data: {},
    };

  return {
    success: true,
    statusCode: VERIFICATION.ACCOUNT.ACCOUNT_VERIFIED.httpCode,
    message: VERIFICATION.ACCOUNT.ACCOUNT_VERIFIED.message,
    data: userData,
  };
};

export const accountVerificationService = async (verifyingPayload, verificationOf = system.VERIFICATION.ACCOUNT) => {
    try {
        switch (verificationOf) {
          case system.VERIFICATION.ACCOUNT:
            return __handleEmailVerification(verifyingPayload);
            break;

          case system.VERIFICATION.FORGOT_PASSWORD:
            return __handleForgotPasswordVerification(verifyingPayload);
            break;

          default:
            return;
        }

    } catch (error) {
        return {
            success: false,
            statusCode: SERVER_ERROR.httpCode,
            message: SERVER_ERROR.message,
            data: error.message
        };
    }
};

export const resendVerificationService = async (resendPayload, resendOf = system.VERIFICATION.ACCOUNT) => {
    try {
        switch (resendOf) {
            case system.VERIFICATION.ACCOUNT:
                return __handleResendEmailVerification(resendPayload);
                break;

            default:
                return;
        }

    } catch (error) {
        return {
            success: false,
            statusCode: SERVER_ERROR.httpCode,
            message: SERVER_ERROR.message,
            data: error.message
        };
    }
};
