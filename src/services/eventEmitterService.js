import EventEmitter from "events";
import {handleEmailNotificationService, handleEmailNotificationServiceFP} from "./emailNotificationService";
import { handleEventCreatedNotificationService, handleEvenJoinNotificationService } from "./pushNotificationService";

class Emitter extends EventEmitter {}
const MyEmitter = new Emitter();

MyEmitter.on('FollowUserNotification', () => {
});

MyEmitter.on(
  "sendEmailVerification",
  async (userData, action = "verification") => {
    console.log(userData, action);
    try {
      await handleEmailNotificationService({ tokenHolder: userData }, action);
    } catch (e) {
      console.log("Error sending email", e);
    }
  }
);

MyEmitter.on("sendForgotPasswordEmail", async (userData) => {
  try {
    await handleEmailNotificationServiceFP({ tokenHolder: userData }, "verification");
  } catch (e) {
    console.log("Error sending email", e);
  }
});

MyEmitter.on("sendNewEventToFollowersNotification", async ({event, loggedUser}) => {
  try {
    await handleEventCreatedNotificationService({ event, loggedUser });
  } catch (e) {
    console.log("Error sending notification of new event", e);
  }
});

MyEmitter.on("sendJoinEventNotification", async ({event, loggedUser}) => {
  try {
    console.log({event, loggedUser});
    await handleEvenJoinNotificationService({ event, loggedUser });
  } catch (e) {
    console.log("Error sending notification of new event", e);
  }
});

export default MyEmitter;
