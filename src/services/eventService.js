import { getBaseUrl, objectId } from "../helpers/common-scripts";
import {
    createEvent,
    updateEvent,
    eventById,
    deleteEvent,
    listEvent,
    eventDetail,
    joinTheEvent,
    inviteToTheEvent
} from "../repositories/eventRepository";
import MyEmitter from "./eventEmitterService";

export const createEventService = async (payload, loggedUser) => {
    const eventData = {
        ...payload,
        organizer: loggedUser._id,
        status: "active",
        private: false
    };
    const event = await createEvent(eventData);

    MyEmitter.emit("sendNewEventToFollowersNotification", {
        event,
        loggedUser
    });
    
    return event;
};

export const updateEventService = async (eventId, payload, loggedUser) => {
    //donot let other user to update the event
    const eventDetail= await eventById(eventId);
    if (!eventDetail) {
        throw new Error("Event not found");
    }

    if (loggedUser._id.toString() !== eventDetail.organizer.toString()) {
        // this is un authorized access
        return;
    }

    const filter = {
        _id: eventDetail._id
    };

    return updateEvent(filter, payload);
};

export const deleteMyEventService = async (eventId, loggedUser) => {
    const filter = {
        _id: eventId
    };
    const updatePayload = {
        status: "archived"
    };

    return updateEvent(filter, updatePayload);
};

export const sendEventInvitationService = async (sender, payload) => {
    //event admin can only send the invitation
    const detail = await eventById(payload.eventId);
    if (!detail) {
        throw new Error("Event not found.");
    }

    // if (detail.organizer.toString() !== sender._id.toString()) {
    //     throw new Error("You can not send invitation for this event.");
    // }

    const totalGoing = detail.members.filter((member) => member.status === 'joined').length;

    if (totalGoing >= detail.totalTravellers) {
        throw new Error("Maximum allowed participant already joined.");
    }

    return inviteToTheEvent(detail._id, payload.userId);
};

export const joinToPublicEventService = async (loggedUser, payload) => {
    const detail = await eventById(payload.eventId);
    if (!detail) {
        throw new Error("Event not found.");
    }

    if (detail.members.find((member) => member.userId.toString() === loggedUser._id.toString())) {
        throw new Error("You have already joined to this event.");
    }
    const totalGoing = detail.members.filter((member) => member.status === 'joined').length;
    console.log(totalGoing);
    if (totalGoing >= detail.totalTravellers) {
        throw new Error("Maximum allowed participant already joined.");
    }

    MyEmitter.emit("sendJoinEventNotification", {
        event: detail,
        loggedUser
    });

    return joinTheEvent(detail._id, loggedUser);
};

export const responseToInvitationService = async (payload) => {

};

export const listEventService = async (option, loggedUser) => {
    return listEvent(option);
};

export const eventDetailService = async (loggedUser, payload) => {
    const detail = await eventDetail(payload.eventId, loggedUser);

    if (!detail) {
        throw new Error("Event not found");
    }

    // const member = detail.allMmembers.find(m => (m.userId.toString() === loggedUser._id.toString()))
    // return {
    //     ...detail,
    //     joiningStatus: !member 
    //         ? 'none'
    //         : member.status
    // };
    return detail;
};
