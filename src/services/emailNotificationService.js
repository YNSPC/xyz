import {emailConfig, systemConfig, tokenConfig} from "../config";
import {sendMail} from "../modules/nodeMailer";
import {generateJwtToken} from "../modules/token";
import {system} from "../helpers/constants/database";
import {createToken} from "../repositories/tokenRepository";
import {REGISTER, FORGOT_PASSWORD} from "../helpers/constants/server-messages";
import Errors from "../helpers/errors";
import { uniqueCodeGen } from "../utils/lodashUtil";

const emailPayloadHelper = (payload, dataObject={}) => {
    return {
        from: payload.from,
        to: payload.to,
        subject: payload.subject,
        template: payload.template,
        emailDataObject: dataObject
    };
};

const emailHeaderHelper = (payload) => {
    return {
        from: payload.from || emailConfig.from,
        to: payload.to,
        subject: payload.subject,
        template: payload.template
    }
};

export const handleEmailNotificationService = async (payload, action, tokenHolderType="application") => {
    let jwtPayload = {};
    let emailDataObject = {};
    let emailHeaderObject = {};
    let jwtAction = "";
    let tokenType = "";
    const actionObjectMapper = {
      "verification": "expiryAccountVerificationToken",
      "forgot-password": "expiryAccountVerificationToken"
    };
    const actionLinkObjectMapper = {
      verification: "emailVerificationLink",
      "forgot-password": "passwordResetLink",
    };
    const addExpiry = tokenConfig[actionObjectMapper[action]];

    if (action === "verification") {
      jwtPayload = {
        user: payload.tokenHolder._id,
        email: payload.tokenHolder.email,
      };
      jwtAction = system.TOKEN.VERIFICATION_TOKEN.ACTION;
      tokenType = system.TOKEN.VERIFICATION_TOKEN.ACTION;
      emailDataObject = {
        userName: payload.tokenHolder.firstName,
        projectName: systemConfig.systemName,
        attachments: [], 
        //customGenerator.linkGenerate('emailVerification', randomUUID),
      };
      emailHeaderObject = emailHeaderHelper({
        from: null,
        to: payload.tokenHolder.email,
        subject: "Email verification",
        template: "email_verification",
      });
    }

    if (action === "forgot-password") {
      jwtPayload = {
        user: payload.tokenHolder._id,
        email: payload.tokenHolder.email,
      };
      jwtAction = system.TOKEN.FORGOT_PASSWORD_TOKEN.ACTION;
      tokenType = system.TOKEN.FORGOT_PASSWORD_TOKEN.ACTION;
      const uniqueCode = await uniqueCodeGen(6, { type: "numeric" });
      emailDataObject = {
        userName: payload.tokenHolder.firstName,
        projectName: systemConfig.systemName,
        attachments: [],
        code: uniqueCode,
        expiry: parseInt(addExpiry)/60
      };
      emailHeaderObject = emailHeaderHelper({
        from: null,
        to: payload.tokenHolder.email,
        subject: "Forgot Password",
        template: "forgot_password",
      });
    }

    const token = await generateJwtToken( jwtPayload, jwtAction);
    emailDataObject.token = token;
    const emailObject = emailPayloadHelper(emailHeaderObject, emailDataObject);

    const actionUrlObjectMapper = {
      verification: `${systemConfig.apiBaseUrl}/fcm-test/${token}`,
      "forgot-password": `${systemConfig.adminBaseUrl}/forgot-password/${token}`,
    };
    emailDataObject[actionLinkObjectMapper[action]] = actionUrlObjectMapper[action];

    await createToken({
        token,
        tokenType,
        tokenHolderType,
        tokenHolder: payload.tokenHolder._id,
        expiresAt: new Date((Math.floor(Date.now()/1000) + parseInt(addExpiry))*1000), // convert seconds to mill
    });

    try {
        await sendMail(emailObject);
        return REGISTER.SENT_FOR_VERIFICATION.message;
    }
    catch (error) {
        return error.message || REGISTER.SENT_FOR_VERIFICATION_FAILED.message;
        throw Errors.customError(error.message || REGISTER.SENT_FOR_VERIFICATION_FAILED.message, 500);
    }
};

export const handleEmailNotificationServiceFP = async (payload, action, tokenHolderType = "application") => {
  let emailDataObject = {};
  let emailHeaderObject = {};
  let jwtPayload = {};
  let jwtAction = "";
  let tokenType = "";

  switch (action) {
    case "forgot_password":
        jwtPayload = {
          user: payload.tokenHolder._id,
          email: payload.tokenHolder.email,
        };
        jwtAction = system.TOKEN.FORGOT_PASSWORD_TOKEN.ACTION;
        tokenType = system.TOKEN.FORGOT_PASSWORD_TOKEN.ACTION;
        emailDataObject = {
          userName: `${payload.tokenHolder.firstName} ${payload.tokenHolder.lastName}`,
          projectName: systemConfig.systemName,
          code: payload.code,
          expiry: parseInt(tokenConfig.expiryForgotPasswordToken)/60,
        };
        emailHeaderObject = emailHeaderHelper({
            from: null,
            to: payload.tokenHolder.email,
            subject: "Forgot Password",
            template: "forgot_password",
        });     
        break;
  
    default:
        break;
  }

  const emailObject = emailPayloadHelper(emailHeaderObject, emailDataObject);

  try {
    await createToken({
      token,
      tokenType,
      tokenHolderType,
      tokenHolder: payload.tokenHolder._id,
      expiresAt:
        Math.floor(Date.now() / 1000) +
        parseInt(tokenConfig.expiryForgotPasswordToken),
    });
    await sendMail(emailObject);
    return FORGOT_PASSWORD.EMAIL_SENT.message;
  } catch (error) {
    return error.message || FORGOT_PASSWORD.EMAIL_NOT_FOUND.message;
    throw Errors.customError(
      error.message || FORGOT_PASSWORD.EMAIL_NOT_FOUND.message,
      500
    );
  }
};