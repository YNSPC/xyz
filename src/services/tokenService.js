import Jwt from "jsonwebtoken";
import {tokenConfig} from "../config";
import Errors from "../helpers/errors";
import {generateAccessToken, generateRefreshToken} from "../modules/token";
import {updateOrCreateUserDevices, updateUserDevices} from "../helpers/queries/database";

export const accessTokenService = async ({user, payload}) => {
    const refreshToken = await generateRefreshToken(user._id);
    const { accessToken, expiry } = await generateAccessToken(user._id);
    let userDeviceResponse = {};

    if ( typeof payload !== "undefined" && payload ) 
        userDeviceResponse = await updateOrCreateUserDevices(user._id, payload.deviceId, payload.deviceType, payload.deviceToken, accessToken, refreshToken, expiry);

    if ( !userDeviceResponse ) 
        throw Errors.customError("Unable to update|create user device.", 422);

    return {
        tokenData: {
            accessToken: userDeviceResponse.accessToken || accessToken,
            refreshToken: userDeviceResponse.refreshToken || refreshToken,
            expiry: userDeviceResponse.expiredAt || expiry
        }
    }
};

export const refreshTokenService = async refreshToken => {
    const decoded = Jwt.verify(
        refreshToken,
        tokenConfig.secretRefreshToken
    );

    if ( !decoded ) throw Errors.customError("Invalid token provided.", 401);

    const { accessToken, expiry } = await generateAccessToken(decoded.sub);

    return { accessToken, expiry };
}

export const refreshFcmTokenService = async ({ deviceId, deviceToken }) => {
    await updateUserDevices({ deviceId }, { deviceToken });
}
