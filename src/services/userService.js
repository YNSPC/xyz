import moment from "moment";
import { handleEmailNotificationService } from "./emailNotificationService";
import {
    findUserById,
    userDetailWithFcm,
    followUnfollowRemoveUser,
    saveUser,
    userDetail, 
    updateUserModel, 
    listFollowersFollowings, 
    userProfile,
    updateUserWithArrayModel,
    blockedUserList,
    updateUserWithArrayModelPull,
    loggedUserSuggestion,
    findUserByEmail,
    deleteAccount,
    updatePassword
} from "../repositories/userRepository";
import {
    createNotification, deleteNotifications
} from "../repositories/notificationRepository";
import {
    deleteReviews
} from "../repositories/reviewRepository";
import Errors from "../helpers/errors";
import {isValidObjectId, upperCaseFirstLetter, dbUsage, HashPassword} from "../helpers/common-scripts";
import {REGISTER} from "../helpers/constants/server-messages";
import {notifications} from "../helpers/constants/notification";
import {parseMessage, sendMessage} from "../modules/googleFcm";
import {enums, system} from "../helpers/constants/database";
import {accessTokenService, } from "./tokenService";
import MyEmitter from "./eventEmitterService";
import { generateJwtToken } from "../modules/token";
import { uniqueCodeGen } from "../utils/lodashUtil";
import { deleteComments } from "../repositories/commentRepository";
import { deletePosts } from "../repositories/postRepository";
import { deletePrivateMessages } from "../repositories/chatRepository";
import { deleteFavourites } from "../repositories/placesRepository";
import { deleteReports } from "../repositories/reportRepository";
import { validateEmail } from "./commentService";
import { removeToken } from "../repositories/tokenRepository";

export const userDetailService = async (userId, loggedUser) => {
    if ( !isValidObjectId(userId) ) throw Errors.customError("Please provide a valid id.", 422);
    return userProfile(userId, loggedUser);
};

export const registerService = async (registerPayload, deviceId, deviceType, deviceToken) => {
    if( registerPayload.termsAndCondition === false ) throw Errors.customError(REGISTER.TERMS_AND_CONDITION.message, 422);
    if( registerPayload.privacyPolicy === false ) throw Errors.customError(REGISTER.PRIVACY_POLICY.message, 422);

    // const emailValid = await validateEmail(registerPayload.email)
    // console.log(JSON.stringify(emailValid, null, 2));
    // if (!emailValid.valid) {
    //     if (emailValid.validators.disposable) {
    //         throw new Error(emailValid.validators.disposable.reason || "Invalid email. Try using valid email");
    //     }

    //     throw new Error(emailValid.reason || "Invalid email. Try using valid email");
    // }

    let emailIsNotVerified = true;
    let userModelObject = {
        firstName: registerPayload.firstName,
        lastName: registerPayload.lastName,
        email: registerPayload.email,
        password: registerPayload.password,
        gender: registerPayload.gender,
        // emailTokenUuid: randomUUID,
        emailVerifiedAt: '',
        profilePicture: registerPayload.profilePicture || null,
        status: 1
    };
    if( registerPayload.dateOfBirth ) userModelObject.dateOfBirth = registerPayload.dateOfBirth;

    userModelObject.termsAndConditionAcceptedAt = moment.utc();
    userModelObject.privacyPolicyAcceptedAt = moment.utc();

    const newUser = await saveUser(userModelObject);

    // if(!emailIsNotVerified) return "User has been created.";

    try {
        // MyEmitter.emit('sendEmailVerification', newUser);
        const tokenServiceResponse = await accessTokenService({ user: newUser, payload: {deviceId, deviceType, deviceToken}})

        return {
            profile: newUser,
            tokenData: tokenServiceResponse.tokenData
        }
    }
    catch (e) {
        console.log("unable to send email", e.message);
        throw e;
    }
};

export const loginService = async ({user, deviceId, deviceType, deviceToken}) => {

    const tokenServiceResponse = await accessTokenService({ user, payload: {deviceId, deviceType, deviceToken}});
    const userProfileDetail = await userDetail(user._id);

    if ( user.isFirstTimeLogin )
        await updateUserModel(
            { _id: user._id, isFirstTimeLogin: true },
            { isFirstTimeLogin: false }
        );

    return {
        profile: userProfileDetail,
        tokenData: tokenServiceResponse.tokenData
    }
};

export const adminLoginService = async ({user, deviceId, deviceType, deviceToken}) => {

    const tokenServiceResponse = await accessTokenService({ user, payload: {deviceId, deviceType, deviceToken}});

    return {
        profile: user,
        tokenData: tokenServiceResponse.tokenData
    }
};

export const favouriteTagService = async ({tags, user}) => {
    if ( tags === null || ( Array.isArray(tags) && tags.length === 0 ) )
        throw Errors.customError("You have to select at least one tag.", 490);

    return updateUserModel(
        { _id: user._id },
        { favouriteTags: tags }
    );
};

export const followUserService = async ({ loggedUser, userToBeFollowedId }) => {
    if ( !isValidObjectId(userToBeFollowedId) ) throw Errors.customError("Please provide a valid id.", 422);

    const userInfoOfOneWeWantToFollow = await userDetailWithFcm(
        { userId: userToBeFollowedId },
        {}
    );
    if(loggedUser.followings.some(following => following.userId.equals(userInfoOfOneWeWantToFollow._id) ))
        throw Errors.customError("You have already followed this user.", 412);
    //cannot follow own self
    if ( loggedUser._id.equals(userInfoOfOneWeWantToFollow._id) )
        throw Errors.customError("You don't have to follow yourselves.", 412);

    //follow the intended user and save the data in our database
    const updatedUserData = await followUnfollowRemoveUser(userInfoOfOneWeWantToFollow, loggedUser, "follow");

    const notificationTemplate = notifications.follow;
    const mapObject = {
        "#user": `${upperCaseFirstLetter(loggedUser.firstName)} ${upperCaseFirstLetter(loggedUser.lastName)}`
    };
    const title = parseMessage(notificationTemplate.title, mapObject);
    const message = "";// const message = parseMessage(notificationTemplate.message, mapObject);//currently showing only title
    const notificationData = {
        userId: loggedUser._id.toString(),
        userName: `${loggedUser.firstName} ${loggedUser.lastName}`,
        userProfilePicture: `${loggedUser.profilePicture}`,
    };
    const notificationPayload = {
        sender: loggedUser._id,
        receiver: userInfoOfOneWeWantToFollow._id,
        title,
        data: notificationData,
        notificationType: "follow"
    };
    await createNotification(notificationPayload);

    if ( userInfoOfOneWeWantToFollow.fcmTokens.length > 0 ) {
        try {
            const fcmResponse = await sendMessage({
                tokens: userInfoOfOneWeWantToFollow.fcmTokens,
                badge: 1,
                title,
                message,
                notificationData
            });
            console.log("fcm response", fcmResponse);
        }
        catch (exception) {
            console.log("unable to send notification", exception);
            // throw Errors.customError(exception.message, 422);
        }
    }

    return updatedUserData;
};

export const unFollowUserService = async ({ loggedUser, userToBeUnFollowedId }) => {
    if ( !isValidObjectId(userToBeUnFollowedId) ) throw Errors.customError("Please provide a valid id.", 422);

    const userInfoOfOneWeWantToUnFollow = await userDetailWithFcm(
        { userId: userToBeUnFollowedId },
        { showTag: true, showFcm: true }
    );

    if(!loggedUser.followings.some(following => following.userId.equals(userInfoOfOneWeWantToUnFollow._id) ))
        throw Errors.customError("You have already un-followed this user.", 412);

    if ( loggedUser._id.equals(userInfoOfOneWeWantToUnFollow._id) )
        throw Errors.customError("You don't have to un-follow yourselves.", 412);

    return followUnfollowRemoveUser(userInfoOfOneWeWantToUnFollow, loggedUser, "unfollow");
};

export const listMyFollowersService = async ({requestedParameter, loggedUser}) => {
    let userWhichWeHaveSelected = loggedUser._id;
    if ( requestedParameter.userId !== 'undefined' )
        userWhichWeHaveSelected = await findUserById(requestedParameter.userId);

    return listFollowersFollowings(userWhichWeHaveSelected, "followers");
};

export const listMyFollowingsService = async ({requestedParameter, loggedUser}) => {
    let userWhichWeHaveSelected = loggedUser._id;
    if ( requestedParameter.userId !== 'undefined' )
        userWhichWeHaveSelected = await findUserById(requestedParameter.userId);

    return listFollowersFollowings(userWhichWeHaveSelected, "followings");
};

export const facebookLinkService = async payload => {
    let userModelObject = {
        facebookAccessToken: payload.user.id,
        email: payload.user.email || null,
        firstName: payload.user.first_name,
        lastName: payload.user.last_name,
        userName: payload.user.email,
        gender: enums.GENDER.MALE,
        registeredFrom: enums.REGISTERING_DEVICES.FACEBOOK,
        termsAndConditionAcceptedAt: moment.utc(),
        profilePicture: payload.user.picture ? payload.user.picture.data.url : null
    };
    const createdUser = await saveUser(userModelObject);

    const tokenData = await accessTokenService({user: createdUser, payload: payload.body });

    return {
        profile: createdUser,
        tokenData
    };
};

export const googleLinkService = async ({ user: googleData, body }) => {
    let userModelObject = {
        firstName: googleData.given_name,
        lastName: googleData.family_name,
        email: googleData.email || null,
        userName: googleData.email,
        gender: enums.GENDER.MALE,
        googleAccessToken: googleData.sub,
        registeredFrom: enums.REGISTERING_DEVICES.GOOGLE,
        termsAndConditionAcceptedAt: moment.utc(),
        profilePicture: googleData.picture || null
    };

    const createdUser = await saveUser(userModelObject);

    const tokenData = await accessTokenService({user: createdUser, payload:{ ...body } });

    return {
        profile: createdUser,
        tokenData
    };
};

export const blockUserService = async({ userToBeBlocked, loggedUser }) => {
    const userToBeBlockedDetail = await findUserById(userToBeBlocked);
    if ( !userToBeBlockedDetail ) 
        throw Errors.customError("Unable to find the user.", 412);

    if ( userToBeBlockedDetail._id.equals(loggedUser._id) )
        throw Errors.customError("You do not have to block yourself.", 412);

    return updateUserWithArrayModel(
        {
            _id: loggedUser._id
        }, 
        {
            blockedTo: userToBeBlockedDetail._id
        }
    );
};

export const blockedUserListService = async loggedUser => {
    const response = await blockedUserList(loggedUser);
    return !response ? [] : response.blockedUserDetail;
};

export const unBlockUserService = async({ userToBeUnBlocked, loggedUser }) => {
    const userToBeUnBlockedDetail = await findUserById(userToBeUnBlocked);
    if ( !userToBeUnBlockedDetail )
        throw Errors.customError("Unable to find the user.", 412);

    if ( userToBeUnBlockedDetail._id.equals(loggedUser._id) )
        throw Errors.customError("You do not have to un block yourself.", 412);

    return updateUserWithArrayModelPull(
        {
            _id: loggedUser._id
        },
        {
            blockedTo: userToBeUnBlockedDetail._id
        }
    );
};

export const userSuggestionService = async({ search, loggedUser }) => {
    const suggestedList = await loggedUserSuggestion(loggedUser, search);
    const resentSearchList = [];

    return {
        suggestedList,
        resentSearchList
    };
};

export const forgotPasswordService = async (email) => {
  const user = await findUserByEmail(email);

  if (!user) {
    throw Errors.customError("Account you are trying to find does not exists.", 400);
  }

  MyEmitter.emit("sendEmailVerification", user, "forgot-password");
};

export const updateProfileService = async (payload) => {
    if (payload.password) {
        payload.password = bcrypt(payload.password);
    }
    return await updateUserModel()
}

export const accountDeleteService = async (loggedUser) => {
    // check for the existance of the user to be deleted
    const userProfileDetail = await userDetail(loggedUser._id);
    if (!userProfileDetail) {
        throw Errors.customError("Account you are trying to delete does not exists.", 400);
    }
    // delete all the posts
    await deletePosts(loggedUser._id);
    // delete all the comments
    await deleteComments(loggedUser._id);
    // delete all the reviews
    // await deleteReviews(loggedUser._id);
    // delete all the notifications
    await deleteNotifications(loggedUser._id);
    // delete all the chats
    await deletePrivateMessages(loggedUser._id);
    // delete all the reports
    await deleteReports(loggedUser._id);
    // delete all the favourites
    await deleteFavourites(loggedUser._id);
    // delete the account of the user completely
    await deleteAccount(loggedUser._id);
};

export const updateUserService = async (user, password, token) => {
    const hashedPassword = await HashPassword(password);
    await updatePassword(hashedPassword, user);
    await removeToken(token);
};