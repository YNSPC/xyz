import { Server } from 'socket.io';
import { upperCaseFirstLetter } from '../helpers/common-scripts';
import { notifications } from '../helpers/constants/notification';
import { parseMessage, sendMessage } from '../modules/googleFcm';
import { getMyFollowingList, getUserDeviceToken } from "../repositories/userRepository";
import { saveChatService } from "../services/chatService";
import { objectValidity } from '../utils/lodashUtil';

export default async function (serverInstance) {
  const CHAT_SOCKETS = {};

  const socketConfig = {
    cors: {
      origin: [
        "https://stage-admin.yatribhet.com",
        "http://stage-admin.yatribhet.com",
        "http://localhost:3000"
      ],
      methods: ["GET", "POST"],
      allowedHeaders: ["header-X-header"]
    }
  };

  function messageObject (payload, socket) {
    return {
      sender: {
        id: socket.uuid,
        name: socket.fullName,
        profilePicture: socket.profilePicture
      },
      receiver: payload.receiver,
      messageType: payload.messageType,
      message: payload.message,
      messageId: payload.messageId,
      hasVideo: Array.isArray(payload.videos) && payload.videos.length > 0 
        ? true
        : false,
      videos: Array.isArray(payload.videos) && payload.videos.length > 0 
        ? payload.videos
        : [],
      hasImage: Array.isArray(payload.images) && payload.images.length > 0 
        ? true
        : false,
      images: Array.isArray(payload.images) && payload.images.length > 0 
        ? payload.images
        : [],
      voice: null
    };
  }

  function messageObjectForUserList(payload, socket) {
    const dateUtc = new Date();
    return {
      _id: socket.uuid,
      fullName: socket.fullName,
      profilePicture: socket.profilePicture,
      message: {
        _id: payload.messageId,
        message: payload.message,
        updatedAt: dateUtc.toISOString()
      }
    };
  }

  function uniqueUser(uuid) {
    return !CHAT_SOCKETS.hasOwnProperty(uuid);
  }

  function createNewSet(uuid) {
    CHAT_SOCKETS[uuid] = new Set();
  }

  function addSocketToSet({ id, uuid }) {
    CHAT_SOCKETS[uuid].add(id);
  }

  function removeSocketFromSet({ id, uuid }) {
    CHAT_SOCKETS[uuid].delete(id);
    console.log(
      "update chat socket detail",
      CHAT_SOCKETS[uuid]
    );
  }

  async function handleSendingMessage(payload, socket) {
    const recipient = payload.receiver;
    const senderUser = payload.sender;
    const sender = socket.uuid;
    const recipientList = CHAT_SOCKETS[recipient] !== undefined ? CHAT_SOCKETS[recipient] : undefined;
    const senderList = CHAT_SOCKETS[sender] !== undefined ? CHAT_SOCKETS[sender] : undefined;

    console.log(recipientList);
    if (recipientList && recipientList.size > 0) {
      recipientList.forEach(recipient => {
        socket
          .broadcast
          .to(recipient)
          .emit("__BROADCAST_TO_RECIPIENTS__", messageObject(payload, socket));

        socket
          .broadcast
          .to(recipient)
          .emit("__BROADCAST_TO_RECIPIENTS_USER_LIST__", messageObjectForUserList(payload, socket));
      });
      // ack({ delivered: true });
    }
    else {
      console.log("Inactive user pn for chat.");

      const messageToUserList = await getUserDeviceToken(payload.receiver);
        if ( messageToUserList[0].fcmTokens.length > 0 ) {
            const notificationData = {
              userId: socket.uuid,
              userName: socket.fullName,
              userProfile: socket.profilePicture,
            };
            const notificationTemplate = notifications.chatMessage;
            const mapObject = {
                "#title": `${upperCaseFirstLetter(socket.fullName.split(' ')[0] || 'anonymous')} ${upperCaseFirstLetter(socket.fullName.split(' ')[1] || 'user')}`,
                "#message": payload.message.length > 20 ? `${payload.message.substring(0, 20)} ...` : payload.message
            };

            try {
                const fcmResponse = await sendMessage({
                    tokens: messageToUserList[0].fcmTokens,
                    badge: 1,
                    title: parseMessage(notificationTemplate.title, mapObject),
                    message: parseMessage(notificationTemplate.message, mapObject),
                    notificationData
                });
                console.log("fcm response", fcmResponse);
            }
            catch (e) {
                console.log("fcm error handled", e.message)
            }

        }
    }

    if (senderList) {
      senderList.forEach(sender => {
        console.log({ sender, sid: socket.id });
        if (sender !== socket.id) {
          socket.broadcast
            .to(sender)
            .emit("__BROADCAST_TO_OTHER_SENDERS__", messageObject(payload, socket))
        }
      })
    }

    await saveChatService(messageObject(payload, socket));
  }

  async function handleOnlineStateWhenConnected(socket) {
    try {
      const userInfo = await getMyFollowingList(socket.uuid);
      console.log('online state', socket.uuid);

      if (userInfo && userInfo.followings.length > 0) {
        for (const user of userInfo.followings) {
          let recipients = CHAT_SOCKETS[user.userId] != undefined ? CHAT_SOCKETS[user.userId] : undefined;

          if (recipients) {
            recipients.forEach(recipient => {
              socket.broadcast
                .to(recipient)
                .emit("__ONLINE_STATE__", String(socket.uuid));
            });
          }
          if (CHAT_SOCKETS[user.userId] && CHAT_SOCKETS[user.userId].size > 0) {
            socket
              .emit("__FRIEND_ONLINE_STATE__", String(user.userId));
          }
        }
      }
    }
    catch (exception) {
      console.log(exception);
    }
  }

  async function handleOfflineStateWhenGone(socket) {
    try {
      const userInfo = await getMyFollowingList(socket.uuid);
      console.log('offline state', socket.uuid);

      if (userInfo && userInfo.followings.length > 0) {
        for (const user of userInfo.followings) {
          let recipients = CHAT_SOCKETS[user.userId] != undefined ? CHAT_SOCKETS[user.userId] : undefined;

          if (recipients) {
            recipients.forEach(recipient => {
              socket.broadcast
                .to(recipient)
                .emit("__OFFLINE_STATE__", String(socket.uuid));
            });
          }

          if (CHAT_SOCKETS[user.userId] && CHAT_SOCKETS[user.userId].size > 0) {
            socket
              .emit("__OFFLINE_STATE_FRIEND__", String(user.userId));
          }
        }
      }
    }
    catch (exception) {
      console.log(exception);
    }
  }

  const socketIo = new Server(serverInstance, socketConfig);

  const chatIO = socketIo.of("/chat");
  chatIO.use((socket, next) => {
    const queryHandshake = socket.handshake.query;

    if (!queryHandshake.uuid || !queryHandshake.fullName) {
      console.log("Error");
      socket.broadcast.emit("__CONNECTION_ERROR_CASE__", "unable to connect!!!")
    }
    else {
      socket.uuid = queryHandshake.uuid;
      socket.fullName = queryHandshake.fullName;
      socket.profilePicture = queryHandshake.profilePicture;
      next();
    }
  });

  chatIO.on("connection", async chatSocket => {
    console.log(`Server is getting new connection ${chatSocket.uuid} => ${chatSocket.id}`);

    await handleOnlineStateWhenConnected(chatSocket);
    // await handleOnlinedUserState(chatSocket);

    if (uniqueUser(chatSocket.uuid)) {
      createNewSet(chatSocket.uuid);
      addSocketToSet(chatSocket);
    }
    else {
      addSocketToSet(chatSocket);
    }

    chatSocket.on("__PING__", (data, cb) => {
      console.log("ping received", {
        response: "pong",
        detail: {
          sid: chatSocket.id,
          userId: chatSocket.uuid
        },
        record: CHAT_SOCKETS[chatSocket.uuid]
      });
      cb({
        status: "RECEIVED"
      })
      chatSocket.broadcast.emit("__PONG__", {
        response: "pong",
        detail: {
          sid: chatSocket.id,
          userId: chatSocket.uuid
        },
        record: CHAT_SOCKETS
      });
    });

    chatSocket.emit("__WELCOME_MESSAGE__", "Welcome to chat namespace!!!");

    chatSocket.on("__SEND_DIRECT_MESSAGE__", (payload) => {
      try {
        handleSendingMessage(payload, chatSocket);
      }
      catch (exception) {
        console.log("Unable to complete the task", exception);
      }
    });

    // // 3. Game play
    // chatSocket.timeout(5000).emit("__TIME_LIMITED_OFFER__", (err) => {
    //     if (err) {
    //         //TODO: the other side did not acknowledge the event in the given delay
    //     }
    // });


    //  build in events
    chatSocket.on("connect_error", reason => {
      console.log(
        "CE",
        reason,
        {
          sid: chatSocket.id,
          userId: chatSocket.uuid
        }
      );
    });

    chatSocket.on("disconnecting", reason => {
      console.log(
        "DCG",
        reason,
        {
          sid: chatSocket.id,
          userId: chatSocket.uuid,
          fullName: chatSocket.fullName,
          userDetail: CHAT_SOCKETS[chatSocket.uuid]
        }
      );
    });

    chatSocket.on("disconnect", async reason => {
      console.log(
        "DC before",
        reason,
        {
          sid: chatSocket.id,
          userId: chatSocket.uuid,
          fullName: chatSocket.fullName,
          userDetail: CHAT_SOCKETS[chatSocket.uuid]
        }
      );
      await handleOfflineStateWhenGone(chatSocket);
      removeSocketFromSet(chatSocket);
    });
  });
}

/**
 * Notes:
 * 1. emit                  ----> to all of the acitves
 * 2. broadcast.emit        ----> all except the sender
 * 3. local.emit            ----> clients only in the local server
 * 4. rooms                 ----> intro to rooms to emit a bunch of sockets
 *      i. to(""|[]).emit   ----> to all in rooms except sender
 *          a. roomName ------> for room
 *          b. socketId ------> for particular socket ie client (..DM..)
 *      ii. in().emit       ----> to all clients
 *      iii. except()       ----> name suggests
 *      iv. of("")          ----> select the particular namespace
 */

//socket related task

// let CHAT_SYS_LIST = {};
// // const ex = {
// //   "uuid": {
// //     "name": "",
// //     "id": "uuid",
// //     ...others,
// //     sids: [],
// //   }
// // }



//   socket.on("disconnect", async function () {
//     console.log("some one got disconnected", {
//       list: CHAT_SYS_LIST[user.userId],
//       user,
//       sid: socket.id
//     });
//     try {
//       users = await sendOfflineStateToMyCircle(socket);
//       if (users.length > 0) {
//         for (const user of users) {
//           let recipients = CHAT_SYS_LIST[user.userId] != undefined ? CHAT_SYS_LIST[user.userId] : undefined;
//           console.log({recipients, j: user.userId});
//           if (recipients) {
//             recipients.forEach(recipient => {
//               socket
//                 .broadcast
//                 .to(recipient)
//                 .emit("__OFFLINE_STATE__", {
//                   "friend": socket.uuid
//                 })
//             })
//           }
//         }
//       }
//     }
//     catch(e) {
//       console.log({e});
//     }
//     removeSocketFromSet(socket);
//   });

// });
