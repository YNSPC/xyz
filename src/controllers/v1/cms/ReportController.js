import {getValidPage, paginateProvider, undefinedCase} from "../../../helpers/common-scripts";
import {GENERAL } from "../../../helpers/constants/server-messages";
import {
    listReportService,
    reportDetailService
} from "../../../services/reportService";

const listReport = async (  request, response, next ) => {
    try {
        const payload = request.query;
        const reportResponse = await listReportService(payload);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: reportResponse
            });
    }
    catch(e) {
        return next(e);
    }
};

const reportDetail = async (  request, response, next ) => {
    try {
        const payload = request.query;
        const reportResponse = await reportDetailService(payload);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: reportResponse
            });
    }
    catch(e) {
        return next(e);
    }
};

export default {
    listReport,
    reportDetail
};
