import { PLACE, GENERAL } from "../../../helpers/constants/server-messages";
import {
  getValidPage,
  isValidObjectId,
  paginateProvider,
} from "../../../helpers/common-scripts";
import {
  createPage,
  adminPageList
} from "../../../repositories/pageRepository";

export const list = async (request, response, next) => {
  try {
    const page = await getValidPage(request.query.page);
    const { sortBy, sortDesc, limit, search } = request.query;

    const options = {
      limit,
      page,
      search,
      sorting:
        sortBy && sortDesc
          ? {
              [sortBy]: sortDesc,
            }
          : { name: "asc" },
      logger: request.user ? request.user._id : null,
    };

    const { docs, ...pagination } = await adminPageList(options);

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: GENERAL.SUCCESS.message,
      data: {
        docs,
        pagination,
      },
    });
  } catch (e) {
    return next(e);
  }
};

export const create = async (request, response, next) => {
  try {
    const payload = request.body;

    const createdHotel = await createPage(payload);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data: createdHotel
      });
  } catch (e) {
    return next(e);
  }
};
