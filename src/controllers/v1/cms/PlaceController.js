import { PLACE, GENERAL } from "../../../helpers/constants/server-messages";
import {
  getValidPage,
  isValidObjectId,
  paginateProvider,
} from "../../../helpers/common-scripts";
import placesRepository, {
  adminPlaceListForDropdown,
  adminPlaceCreate,
  adminPlaceUpdate,
  deletePlaceWithId
} from "../../../repositories/placesRepository";
import { adminPlaceList } from "../../../repositories/placesRepository";

export const list = async (request, response, next) => {
  try {
    const page = await getValidPage(request.query.page);
    const { sortBy, sortDesc, limit, search } = request.query;

    const options = {
      limit,
      page,
      search,
      sorting:
        sortBy && sortDesc
          ? {
              [sortBy]: sortDesc,
            }
          : { name: "asc" },
      logger: request.user ? request.user._id : null,
    };

    const { docs, ...pagination } = await adminPlaceList(options);

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: GENERAL.SUCCESS.message,
      data: {
        docs,
        pagination,
      },
    });
  } catch (e) {
    return next(e);
  }
};

export const listForDropdown = async (request, response, next) => {
  try {
    const listData = await adminPlaceListForDropdown();

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: GENERAL.SUCCESS.message,
      data: listData,
    });
  } catch (e) {
    return next(e);
  }
};

export const createPlace = async (request, response, next) => {
  try {
    const placeBody = request.body;
    const newPlace = await adminPlaceCreate(placeBody);

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: GENERAL.SUCCESS.message,
      data: newPlace
    });
  } catch (e) {
    return next(e);
  }
};

export const updatePlace = async (request, response, next) => {
  try {
    const placeBody = request.body;
    const { placeId } = request.params;
    const updatedPlace = await adminPlaceUpdate(placeId, placeBody);

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: GENERAL.SUCCESS.message,
      data: updatedPlace
    });
  } catch (e) {
    return next(e);
  }
};

export const deletePlace = async (request, response, next) => {
  try {
    const { placeId } = request.params;
    const updatedPlace = await deletePlaceWithId(placeId);

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: GENERAL.SUCCESS.message,
      data: {}
    });
  } catch (e) {
    return next(e);
  }
};
