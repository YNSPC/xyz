import fileUpload from "../../../modules/fileUpload";
import { uploadConfig } from "../../../config";
import { copyFileToGCS, uploadFilesToGCS } from "../../../modules/googleCloudService";
import { PLACE } from "../../../helpers/constants/server-messages";

export const uploadSingleFile = async (request, response, next) => {
    try {
        const fileUploader = new fileUpload('csv/tags', true, true);
        const uploadedResponse = await fileUploader.multipleUpload('payload', request, response, uploadConfig.image);
        const requestFile = request.file;
        console.log(requestFile, request.files)
        let googleCloudResponse = await copyFileToGCS(requestFile);

        return response
            .status(PLACE.IMAGE_UPLOAD.SUCCESS.httpCode)
            .json({
                message: PLACE.IMAGE_UPLOAD.SUCCESS.message,
                data: googleCloudResponse
            });
    }
    catch (e) {
        return next(e);
    }
};

export const uploadMultipleFiles = async (request, response, next) => {
    try {
        const fileUploader = new fileUpload('csv/tags', true, true);
        const uploadedResponse = await fileUploader.multipleUpload('payload', request, response, uploadConfig.image);
        const requestFile = request.files;
        const requestBody = request.body;
        const options = {
            folder: requestBody.folder
        };
        // TODO: handle in precise mannar
        if (requestFile.length === 0) {
            return response
                .status(400)
                .json({
                    message: "Bad request",
                    data: []
                });
        }
        let googleCloudResponse = await uploadFilesToGCS(requestFile, options);

        return response
            .status(PLACE.IMAGE_UPLOAD.SUCCESS.httpCode)
            .json({
                message: PLACE.IMAGE_UPLOAD.SUCCESS.message,
                data: googleCloudResponse
            });
    }
    catch (e) {
        return next(e);
    }
};
