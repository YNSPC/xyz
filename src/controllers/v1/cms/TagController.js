import { PLACE, GENERAL } from "../../../helpers/constants/server-messages";
import {
  getValidPage,
  isValidObjectId,
  paginateProvider,
} from "../../../helpers/common-scripts";
import {
  adminTagList,
  createTag,
  adminTagListForDropDown,
  getTagById,
  updateTagViaApi,
  deleteTagRepo
} from "../../../repositories/tagsRepository";

export const list = async (request, response, next) => {
  try {
    const page = await getValidPage(request.query.page);
    const { sortBy, sortDesc, limit, search } = request.query;

    const options = {
      limit,
      page,
      search,
      sorting:
        sortBy && sortDesc
          ? {
              [sortBy]: sortDesc,
            }
          : { name: "asc" },
      logger: request.user ? request.user._id : null,
    };

    const { docs, ...pagination } = await adminTagList(options);

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: GENERAL.SUCCESS.message,
      data: {
        docs,
        pagination,
      },
    });
  } catch (e) {
    return next(e);
  }
};

export const listForDropdown = async (request, response, next) => {
  try {
    const dataList = await adminTagListForDropDown();

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: GENERAL.SUCCESS.message,
      data: dataList,
    });
  } catch (e) {
    return next(e);
  }
};

export const detail =  async (request, response, next) => {
  try {
    const tagId = request.params.tagId;
    const tag = await getTagById(tagId);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data: tag
      });
  }
  catch (e) {
    return next(e);
  }
};

export const updateIcon = async (request, response, next) => {
  try {
    const tagId = request.params.tagId;
    const payload = request.body;
    const tag = await updateTagViaApi(tagId, payload);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data: tag
      });
  }
  catch (e) {
    return next(e);
  }
};

export const updateTag = async (request, response, next) => {
  try {
    const tagId = request.params.tagId;
    const payload = request.body;
    const tag = await updateTagViaApi(tagId, payload);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data: tag
      });
  }
  catch (e) {
    return next(e);
  }
};

export const create = async (request, response, next) => {
  try {
    const payload = request.body;

    const createdTag = await createTag(payload);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data: createdTag
      });
  } catch (e) {
    return next(e);
  }
};

export const deleteTag = async (request, response, next) => {
  try {
    const tagId = request.params.tagId;
    const tag = await deleteTagRepo(tagId);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data: tag
      });
  }
  catch (e) {
    return next(e);
  }
};
