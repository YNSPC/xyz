import {
    REGISTER,
    GENERAL,
    LOGIN,
    USER
} from '../../../helpers/constants/server-messages';
import { logoutDevices } from '../../../helpers/queries/database';
import {
    adminLoginService,
    userDetailService,
  
} from '../../../services/userService';
import Errors from "../../../helpers/errors";
import {
    updateUserProfile,
} from "../../../repositories/userRepository"
import {refreshTokenService} from "../../../services/tokenService";

export const login = async ( request, response, next ) => {
    const { deviceType, deviceId, deviceToken } = request.body;
    const user = request.user;

    try {
        const loginServiceResponse = await adminLoginService({user});

        response.set('accessToken', loginServiceResponse.tokenData.accessToken);
        response.set('refreshToken', loginServiceResponse.tokenData.refreshToken);
        response.set('expiresIn', loginServiceResponse.tokenData.expiry);

        return response
            .status(LOGIN.SUCCESS.httpCode)
            .json({
                message: LOGIN.SUCCESS.message,
                data: {
                    userProfile: loginServiceResponse.profile,
                    tokenData: loginServiceResponse.tokenData
                }
            });
    } catch (e) {
        return next(e);
    }
};

export const logout = async ( request, response, next ) => {
    try{
        const { deviceId } = request.params;

        if ( !deviceId ) return next(Errors.notFound());

        await logoutDevices({ accessToken: request.headers.authorization });
        await request.logout();

        return response
            .status(200)
            .json({
                message: LOGIN.SUCCESS.message,
                data: {}
            })

    } catch (e) {
        next(e);
    }
};

export const getUserProfile = async ( request, response, next ) => {
    try{
        const loggedUser = request.user;
        const profile = await userDetailService(loggedUser._id);

        return response
            .status(USER.PROFILE.httpCode)
            .json({
                message: USER.PROFILE.message,
                data: profile
            })
    } catch (e) {
        next(e);
    }
};

export const refreshAccessToken = async (request, response, next) => {
    try {
        const { refreshToken } = request.query;

        const refreshTokenServiceResponse = await refreshTokenService(refreshToken);

        response.set('accessToken', refreshTokenServiceResponse.accessToken);
        response.set('expiresIn', refreshTokenServiceResponse.expiry);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: {
                    accessToken: refreshTokenServiceResponse.accessToken,
                    expiry: refreshTokenServiceResponse.expiry
                }
            });
    }
    catch (error) {
        next(error);
    }
};
