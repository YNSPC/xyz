import { PLACE, GENERAL, HOTEL } from '../../../helpers/constants/server-messages';
import {getValidPage, paginateProvider, isValidObjectId} from "../../../helpers/common-scripts";
import HotelModel from "../../../database/models/Hotels";
import hotelRepository from "../../../repositories/hotelRepository";
import placeRepository from "../../../repositories/placesRepository";
import Errors from "../../../helpers/errors"

const listInGivenPlace = async ( request, response, next ) => {
    try {
        const page = await getValidPage(request.query.page);
        const placeId = request.params.placeId;
        const { rating, minimumOfPriceRange, maximumOfPriceRange, search } = request.query;

        const options = {
            limit: parseInt(process.env.LIMIT || 10),
            page,
            rating,
            minimumOfPriceRange,
            maximumOfPriceRange,
            search
        };

        const placeDetail = await placeRepository.placeById(placeId);
        if ( !placeDetail ) {
            return response
                .status(PLACE.NOT_FOUND.httpCode)
                .json({
                    message: PLACE.NOT_FOUND.message,
                    data: {}
                });
        }

        const aggregateQuery = hotelRepository.getHotelsOfGivePlace(placeDetail, options);
        const hotels = await HotelModel.aggregatePaginate(aggregateQuery, options);
        const getPagination = await paginateProvider(hotels);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                ...getPagination
            });
    }
    catch (e) {
        return next(e);
    }
};

const hotelDetailById = async ( request, response, next ) => {
    try {
        const { hotelId } = request.params;
        if(!isValidObjectId(hotelId)) throw Errors.customError("Invalid request.", 412);

        const hotelDetail = await hotelRepository.hotelById(hotelId);
        if ( !hotelDetail ) {
            return response
                .status(HOTEL.NOT_FOUND.httpCode)
                .json({
                    message: HOTEL.NOT_FOUND.message,
                    data: {}
                });
        }

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: hotelDetail
            });
    }
    catch (e) {
        return next(e);
    }
};

const listAllHotelMenu = async ( request, response, next ) => {
    try {
        const search  = "search" in request.query ? request.query.search : "";
        const rating  = "rating" in request.query ? request.query.rating : "";
        const minimum  = "minimum" in request.query ? request.query.minimum : "";
        const maximum  = "maximum" in request.query ? request.query.maximum : "";
        const filters = {
            rating,
            amount: {
                minimum,
                maximum
            }
        };
        const options = {
            page: request.query.page || 1,
            limit:  request.query.limit || 10,
            sorting: { name: 1 }
        };

        const { docs: data, ...pagination } = await hotelRepository.listHotels(search, filters, options);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data,
                pagination
            });
    }
    catch (e) {
        return next(e);
    }
};

export default {
    listAllHotelMenu,
    listInGivenPlace,
    hotelDetailById
};
