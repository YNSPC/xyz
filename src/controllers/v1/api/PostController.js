import {getValidPage, paginateProvider, undefinedCase} from "../../../helpers/common-scripts";
import {GENERAL } from "../../../helpers/constants/server-messages";
import { getAllPost } from "../../../repositories/postRepository";
import {
    createPostService,
    postDetailService,
    likePostService,
    unLikePostService,
    postFromUserService,
    likesInPostService,
    deletePostService
} from "../../../services/postService";

const listPosts = async ( request, response, next ) => {
    try {
        let guestLogin = !("user" in request && "_id" in request.user && request.user._id.toString().length > 0);
        const loggedUser = request.user || null;
        const page = await getValidPage(request.query.page);

        const options = {
            logger: undefinedCase(request, ["user", "_id"], null),
            limit: request.query.limit || parseInt(process.env.LIMIT || 10),
            page,
        };

/*        const posts = await PostModel.aggregatePaginate(aggregateQuery, options);
        const getPagination = await paginateProvider(posts);*/

        const { docs: data, ...pagination } = await getAllPost(options, loggedUser, guestLogin);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data,
                pagination,
            });
    }
    catch (e) {
        return next(e);
    }
};

const createPost = async ( request, response, next ) => {
    try {
        const user = request.user;
        const payload = request.body;
        const post = await createPostService(payload, user);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: post
            });
    }
    catch (e) {
        return next(e);
    }
};

const likeAPost = async ( request, response, next ) => {
    try {
        const user = request.user;
        const { postId } = request.params;

        const postData = await likePostService(postId, user);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: postData
            });
    }
    catch (e) {
        return next(e);
    }
};

const UnLikeAPost = async ( request, response, next ) => {
    try {
        const user = request.user;
        const { postId } = request.params;

        const postData = await unLikePostService(postId, user);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: postData
            });
    }
    catch (e) {
        return next(e);
    }
};

const postFromUser = async ( request, response, next ) => {
    try {
        const loggedUser = request.user;
        const { page, limit } = request.query;
        const paginationOption = {
            page: page || 1,
            limit: limit || 10,
            sort: {
                createdAt: -1
            }
        };
        const filter = "userId" in request.params
            ?
            (
                (
                    typeof request.params.userId === "undefined" ||
                    request.params.userId === "undefined"
                )
                    ? loggedUser._id.toString()
                    : request.params.userId
            )
            : loggedUser._id.toString();

        const { docs: data, ...pagination } = await postFromUserService(filter, paginationOption, loggedUser);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data,
                pagination
            });
    }
    catch (e) {
        return next(e);
    }
};

const postDetail = async ( request, response, next ) => {
    try {
        const loggedUser = request.user;
        const { postId } = request.params;

        const postData = await postDetailService(postId, loggedUser);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: postData
            });
    }
    catch (e) {
        return next(e);
    }
};

const likesInPost = async ( request, response, next ) => {
    try {
        const { page, limit } = request.query;
        const { postId } = request.params;
        const paginationOption = {
            page: page || 1,
            limit: limit || 10,
            sort: {
                createdAt: -1
            }
        };

        const {docs: data, ...pagination} = await likesInPostService(postId, paginationOption);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data,
                pagination
            });
    }
    catch (e) {
        return next(e);
    }
};

const postDelete = async ( request, response, next ) => {
    try {
        const { postId } = request.params;
        const loggedUser = request.user;

        const postDeleteResponse = await deletePostService(postId, loggedUser);


        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: postDeleteResponse
            });
    }
    catch (e) {
        return next(e);
    }
};

export default {
    listPosts,
    createPost,
    likeAPost,
    UnLikeAPost,
    postFromUser,
    postDetail,
    likesInPost,
    postDelete,
};
