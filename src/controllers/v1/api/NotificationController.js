import {
    listNotifications
} from "../../../repositories/notificationRepository";
import {GENERAL} from "../../../helpers/constants/server-messages";

export const listAllNotifications = async (request, response, next) => {
    try {
        const loggedUser = request.user;
        const options = {
            page: request.query.page || 1,
            limit: request.query.limit || 10,
        };
        const { docs: data, ...pagination } = await listNotifications(options, loggedUser);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data,
                pagination,
            });
    }
    catch (e) {
        next(e);
    }
};
