import { getValidPage } from '../../../helpers/common-scripts';
import { GENERAL } from '../../../helpers/constants/server-messages';
import { 
  createEventService,
  updateEventService,
  sendEventInvitationService,
  responseToInvitationService,
  joinToPublicEventService,
  deleteMyEventService,
  listEventService,
  eventDetailService
} from "../../../services/eventService";

export const createEvent = async (request, response, next) => {
    try {
      const loggedUser = request.user;
      const data = request.body;
      
      const newEvent = await createEventService(data, loggedUser);
  
      return response
        .status(GENERAL.SUCCESS.httpCode)
        .json({
            message: GENERAL.SUCCESS.message,
            data: newEvent
        });
    } catch (e) {
      return next(e);
    }
};

export const updateEvent = async (request, response, next) => {
  try {
    const loggedUser = request.user;
    const data = request.body;
    const { eventId } = request.params;

    console.log("data", data, eventId);
    const updatedEvent = await updateEventService(eventId, data, loggedUser);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
          message: GENERAL.SUCCESS.message,
          data: updatedEvent
      });
  } catch (e) {
    return next(e);
  }
};

export const deleteEvent = async (request, response, next) => {
  try {
    const loggedUser = request.user;
    const { eventId } = request.params;

    await deleteMyEventService(eventId, loggedUser);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
          message: GENERAL.SUCCESS.message,
          data: {}
      });
  } 
  catch (e) {
    return next(e);
  }
};

export const sendInvitation = async (request, response, next) => {
  try {
    const sender = request.user;
    const paramData = request.params;
    const invitationResponse = await sendEventInvitationService(sender, paramData);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
          message: GENERAL.SUCCESS.message,
          data: invitationResponse
      });
  } 
  catch (e) {
    return next(e);
  }
};

export const responseToEvent = async (request, response, next) => {
  try {
  } catch (e) {
    return next(e);
  }
};

export const listEvent = async (request, response, next) => {
  try {
    const loggedUser = request.user;
    // const data = request.body;
    const page = await getValidPage(request.query.page);

    const options = {
        limit: request.query.limit || parseInt(process.env.LIMIT || 10),
        page,
    };

    
    const { docs: data, ...pagination } = await listEventService(options, loggedUser);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data,
        pagination,
      });
  } catch (e) {
    return next(e);
  }
};

export const eventHistory = async (request, response, next) => {
  try {
  } catch (e) {
    return next(e);
  }
};

export const joinValidEvent = async (request, response, next) => {
  try {
    const loggedUser = request.user;
    const paramData = request.params;
    const joinResponse = await joinToPublicEventService(loggedUser, paramData);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
          message: GENERAL.SUCCESS.message,
          data: {}
      });
  } 
  catch (e) {
    return next(e);
  }
};

export const detailOfEvent = async (request, response, next) => {
  try {
    const loggedUser = request.user;
    const paramData = request.params;
    const eventDetail = await eventDetailService(loggedUser, paramData);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
          message: GENERAL.SUCCESS.message,
          data: eventDetail
      });
  } 
  catch (e) {
    return next(e);
  }
};