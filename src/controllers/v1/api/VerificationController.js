import { VERIFICATION } from "../../../helpers/constants/server-messages";
import {accountVerificationService, resendVerificationService} from "../../../services/verificationService";
import {system} from "../../../helpers/constants/database";

export const verifyAccount = async ( request, response, next ) => {
    try {
        const { verificationToken } = request.params;
        const responseFromService = await accountVerificationService(verificationToken, system.VERIFICATION.ACCOUNT);

        if ( !responseFromService.success )
            return response
                .status(responseFromService.statusCode)
                .json({
                    message: responseFromService.message,
                    data: {}
                });

        return response
            .status(responseFromService.statusCode)
            .json({
                message: responseFromService.message,
                data: responseFromService.data
            });
    }
    catch (e) {
        return next(e);
    }
};

export const resendAccountVerification = async ( request, response, next ) => {
    try {
        const { email } = request.params;
        const responseFromService = await resendVerificationService(email);

        if ( !responseFromService.success )
            return response
                .status(responseFromService.statusCode)
                .json({
                    message: responseFromService.message,
                    data: {}
                });

        return response
            .status(responseFromService.statusCode)
            .json({
                message: responseFromService.message,
                data: responseFromService.data
            });
    }
    catch (e) {
        return next(e);
    }
};

export const commitMessageTesting = () => {
    console.log("This is a test message.");
}

export const commitMessageTestingUpdated = () => {
    console.log("This is a test message updated.");
}