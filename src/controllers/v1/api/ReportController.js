import {getValidPage, paginateProvider, undefinedCase} from "../../../helpers/common-scripts";
import {GENERAL } from "../../../helpers/constants/server-messages";
import {
    addReportService
} from "../../../services/reportService";

const makeAReport = async (  request, response, next ) => {
    try {
        const loggedUser = request.user;
        const payload = request.body;
        const reportResponse = await addReportService(payload, loggedUser);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: reportResponse
            }); 
    }
    catch(e) {
        return next(e);
    }
};

export default {
    makeAReport
};
