import {GENERAL } from "../../../helpers/constants/server-messages";
import {
  getRemoteConfigService
} from "../../../services/settingService";

const remoteConfig = async (  request, response, next ) => {
    try {
        const result = await getRemoteConfigService();

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: result
            }); 
    }
    catch(e) {
        return next(e);
    }
};

export default {
  remoteConfig
};
