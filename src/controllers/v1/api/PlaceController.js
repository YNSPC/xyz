import { PLACE, GENERAL } from "../../../helpers/constants/server-messages";
import {
  getValidPage,
  isValidObjectId,
  paginateProvider,
} from "../../../helpers/common-scripts";
import fileUpload from "../../../modules/fileUpload";
import { uploadConfig } from "../../../config";
import placesRepository from "../../../repositories/placesRepository";
import reviewRepository from "../../../repositories/reviewRepository";

const uploadImages = async (request, response, next) => {
  try {
    const fileUploader = new fileUpload("places", false);
    const uploadedResponse = await fileUploader.multipleUpload(
      "place_images",
      request,
      response,
      uploadConfig.image
    );
    let { identifier, type } = request.body;
    type = parseInt(type);
    const uploadedResult = await placesRepository.uploadImages(
      identifier,
      type,
      uploadedResponse
    );

    return response.status(PLACE.IMAGE_UPLOAD.SUCCESS.httpCode).json({
      message: PLACE.IMAGE_UPLOAD.SUCCESS.message,
      data: uploadedResult,
    });
  } catch (e) {
    return next(e);
  }
};

const myPlaces = async (request, response, next) => {
  try {
    const page = await getValidPage(request.query.page);
    const { sortBy, search, sortDesc, itemsPerPage, limit } = request.query;

    const options = {
      limit,
      page,
      search,
      sortBy,
      sortDesc,
      logger: request.user._id,
    };

    const { docs: data, ...pagination } = await placesRepository.getPlaces(options);

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: GENERAL.SUCCESS.message,
      data,
      pagination,
    });
  } catch (e) {
    return next(e);
  }
};

const placeDetail = async (request, response, next) => {
  try {
    const { placeId } = request.params;

    let placeDetail = await placesRepository.placeDetail({
      placeId,
      logger: request.user._id,
    });

    if (!placeDetail) {
      return response.status(GENERAL.NOT_FOUND.httpCode).json({
        message: GENERAL.NOT_FOUND.message,
        data: {},
      });
    }

    const recommendedPlaces = await placesRepository.listRecommendedPlaces({
      tags: placeDetail.tags,
      placeId: placeDetail._id
    });

    placeDetail = {
      ...placeDetail,
      recommendedPlaces: recommendedPlaces,
    };

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: GENERAL.SUCCESS.message,
      data: placeDetail,
    });
  } catch (e) {
    return next(e);
  }
};

const makeFavouriteToggle = async (request, response, next) => {
  try {
    const { placeId } = request.body;

    let placeDetail = await placesRepository.placeDetail({
      placeId,
      logger: request.user._id,
    });

    if (!placeDetail) {
      return response.status(GENERAL.NOT_FOUND.httpCode).json({
        message: GENERAL.NOT_FOUND.message,
        data: {},
      });
    }

    const toggleAction = await placesRepository.toggleFavouritePlace(
      request.user._id,
      placeId
    );

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: toggleAction ? "You have favourite." : "You have un-favourite.",
      data: {
        isFavourite: toggleAction,
      },
    });
  } catch (e) {
    return next(e);
  }
};

/*move to users controller and route*/
const listMyFavouritePlaces = async (request, response, next) => {
  try {
    const listFavouritePlace = await placesRepository.listFavouritePlace(
      request.user._id
    );

    return response
        .status(GENERAL.SUCCESS.httpCode)
        .json({
          message: GENERAL.SUCCESS.message,
          data: listFavouritePlace,
        });
  } catch (e) {
    return next(e);
  }
};

const listPlacesByTag = async (request, response, next) => {
  try {
    const { tagId } = request.params;
    const listPlaceByTag = await placesRepository.listPlaceByTag(tagId);

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: GENERAL.SUCCESS.message,
      data: listPlaceByTag,
    });
  } catch (e) {
    return next(e);
  }
};

const createReview = async (request, response, next) => {
  try {
    const reviewPayload = request.body;
    const loggedUser = request.user;
    const hasAlreadyBeenReviewedDetail =
      await reviewRepository.hasThisPlaceBeenReviewed(
        reviewPayload.placeId,
        loggedUser
      );
    /*if ( !reviewPayload.placeId ) {
            return response
                .status(GENERAL.NOT_FOUND.httpCode)
                .json({
                    message: GENERAL.NOT_FOUND.message,
                    data: {}
                });
        }*/

    if (hasAlreadyBeenReviewedDetail)
      await reviewRepository.updateReview(hasAlreadyBeenReviewedDetail._id, {
        rating: reviewPayload.rating,
        review: reviewPayload.review,
      });
    else await reviewRepository.createReview(reviewPayload, loggedUser);

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: !hasAlreadyBeenReviewedDetail
        ? "Reviewed with success."
        : "Updated your review.",
      data: {},
    });
  } catch (e) {
    return next(e);
  }
};

const updateReview = async (request, response, next) => {
  try {
    const reviewPayload = request.body;
    const { reviewId } = request.params;
    /*if ( !reviewPayload.placeId ) {
            return response
                .status(GENERAL.NOT_FOUND.httpCode)
                .json({
                    message: GENERAL.NOT_FOUND.message,
                    data: {}
                });
        }*/

    await reviewRepository.updateReview(reviewId, reviewPayload);

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: "Success while updating review.",
      data: {},
    });
  } catch (e) {
    return next(e);
  }
};

const deleteReview = async (request, response, next) => {
  try {
    const { reviewId } = request.params;

    await reviewRepository.deleteReview(reviewId);

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: "Success while deleted review.",
      data: {},
    });
  } catch (e) {
    return next(e);
  }
};

const listReviewOfPlace = async (request, response, next) => {
  try {
    const { placeId } = request.params;
    const listReviewOfPlace = await reviewRepository.listReviewsOfPlace(
      placeId
    );

    return response.status(GENERAL.SUCCESS.httpCode).json({
      message: GENERAL.SUCCESS.message,
      data: listReviewOfPlace,
    });
  } catch (e) {
    return next(e);
  }
};

export default {
  myPlaces,
  uploadImages,
  placeDetail,
  listPlacesByTag,
  makeFavouriteToggle,
  listMyFavouritePlaces,

  createReview,
  updateReview,
  deleteReview,
  listReviewOfPlace,
};
