import mongoose from 'mongoose';
import {
    REGISTER,
    GENERAL,
    LOGIN,
    USER
} from '../../../helpers/constants/server-messages';
import { logoutDevices } from '../../../helpers/queries/database';
import {
  loginService,
  registerService,
  followUserService,
  googleLinkService,
  userDetailService,
  unFollowUserService,
  favouriteTagService,
  facebookLinkService,
  listMyFollowersService,
  listMyFollowingsService,
  blockUserService,
  blockedUserListService,
  unBlockUserService,
  userSuggestionService,
  forgotPasswordService,
  accountDeleteService,
  updateUserService
} from "../../../services/userService";
import Errors from "../../../helpers/errors";
import {
    updateUserProfile,
} from "../../../repositories/userRepository"
import {refreshTokenService, refreshFcmTokenService} from "../../../services/tokenService";
import { system } from '../../../helpers/constants/database';
import { accountVerificationService } from '../../../services/verificationService';
import { objectId } from '../../../helpers/common-scripts';
import { placeAndUserSearchService } from '../../../services/searchService';

export const register = async ( request, response, next ) => {
    try {
        const registerData = request.body;
        const deviceId = registerData.deviceId; 
        const deviceType = registerData.deviceType; 
        const deviceToken = registerData.deviceToken;
        const registeredUser = await registerService(registerData, deviceId, deviceType, deviceToken);

        response.set('accessToken', registeredUser.tokenData.accessToken);
        response.set('refreshToken', registeredUser.tokenData.refreshToken);
        response.set('expiresIn', registeredUser.tokenData.expiry);

        return response
            .status(REGISTER.SUCCESS.httpCode)
            .json({
                message: "User has been successfully registered.",
                data:  {
                    userProfile: registeredUser.profile,
                    tokenData: registeredUser.tokenData
                }
            });
    }catch (e) {
        return next(e);
    }
};

export const login = async ( request, response, next ) => {
    const { deviceType, deviceId, deviceToken } = request.body;
    const user = request.user;

    try {
        const loginServiceResponse = await loginService({user, deviceId, deviceToken, deviceType});

        response.set('accessToken', loginServiceResponse.tokenData.accessToken);
        response.set('refreshToken', loginServiceResponse.tokenData.refreshToken);
        response.set('expiresIn', loginServiceResponse.tokenData.expiry);

        return response
            .status(LOGIN.SUCCESS.httpCode)
            .json({
                message: LOGIN.SUCCESS.message,
                data: {
                    userProfile: loginServiceResponse.profile,
                    tokenData: loginServiceResponse.tokenData
                }
            });
    } catch (e) {
        return next(e);
    }
};

export const logout = async ( request, response, next ) => {
    try{
        const { deviceId } = request.params;

        if ( !deviceId ) return next(Errors.notFound());

        await logoutDevices({ deviceId });
        await request.logout();

        return response
            .status(200)
            .json({
                message: LOGIN.SUCCESS.message,
                data: {}
            })

    } catch (e) {
        next(e);
    }
};

export const getUserProfile = async ( request, response, next ) => {
    try{
        const loggedUser = request.user;
        const userId = request.query.userId;
        const userToSearch = userId ? objectId(userId) : loggedUser._id;
        const profile = await userDetailService(userToSearch, loggedUser);

        if (!profile) return next(Errors.notFound(USER));

        return response
            .status(USER.PROFILE.httpCode)
            .json({
                message: USER.PROFILE.message,
                data: profile
            })
    } catch (e) {
        next(e);
    }
};

export const refreshAccessToken = async (request, response, next) => {
    try {
        const { refreshToken } = request.query;

        const refreshTokenServiceResponse = await refreshTokenService(refreshToken);

        response.set('accessToken', refreshTokenServiceResponse.accessToken);
        response.set('expiresIn', refreshTokenServiceResponse.expiry);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: {
                    accessToken: refreshTokenServiceResponse.accessToken,
                    expiry: refreshTokenServiceResponse.expiry
                }
            });
    }
    catch (error) {
        next(error);
    }
};

export const storeFavouriteTags = async (request, response, next) => {
    try {
        const { tags } = request.body;
        const loggedUser = request.user;
        const favouriteTagServiceResponse = await favouriteTagService({tags, user: loggedUser});

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: favouriteTagServiceResponse
            });
    }
    catch (e) {
        return next(e);
    }
};

export const updateProfile = async (request, response, next) => {
    try {
        const requestBody = request.body;
        const loggedUser = request.user;

        await updateUserProfile(requestBody, loggedUser);
        const profile = await userDetailService(loggedUser._id, loggedUser);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: profile
            })
    }
    catch (e) {
        return next(e);
    }
};

export const followARandomPerson = async (request, response, next) => {
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
        const loggedUser = request.user;
        let { userId: userToBeFollowedId } = request.params;
        // const { user: loggedUser, params: dataParams} = request;

        const userResponse = await followUserService({ loggedUser, userToBeFollowedId, session });
        await session.commitTransaction();

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: userResponse
            });
    }
    catch (e) {
        await session.abortTransaction();
        return next(e);
    }
    finally {
        session.endSession();
    }
};

export const unFollowTheOneWhoYouHate = async (request, response, next) => {
    try {
        let { userId: userToBeUnFollowedId } = request.params;
        const loggedUser = request.user;
        console.log("called");
        const userResponse = await unFollowUserService({ loggedUser, userToBeUnFollowedId });

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: userResponse
            })
    }
    catch (e) {
        return next(e);
    }
};

// TODO: this is unwanted feature now.
/*const removeUnwantedFollower = async (request, response, next) => {
    try {
        let { userId } = request.params;
        const loggedUser = request.user;

        const userInfoOfOneWeWantToUnFollow = await UserModel.details(userId);

        if ( loggedUser._id === userInfoOfOneWeWantToUnFollow._id)
            return response
                .status(GENERAL.BAD_REQUEST.httpCode)
                .json({
                    message: GENERAL.BAD_REQUEST.message
                });

        await followUnfollowRemoveUser(userInfoOfOneWeWantToUnFollow, loggedUser, "remove_follower");

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message
            })
    }
    catch (e) {
        return next(e);
    }
};*/

export const checkUserNameAvailability = async ( request, response, next ) => {
    /*try{
        const { userId } = request.params;//nullable field
        const { userName } = request.body;
        let user, httpCode, httpMessage;

        if ( typeof userId === 'string' && isValidObjectId(userId) )
            user = await UserModel.findOne({ userName: userName, _id: { $ne: userId } });
        else
            user = await UserModel.findOne({ userName: userName });

        if ( user ){
            httpCode = USER.USER_NAME_EXISTS.httpCode;
            httpMessage = USER.USER_NAME_EXISTS.message
        }
        else {
            httpCode = GENERAL.SUCCESS.httpCode
            httpMessage = GENERAL.SUCCESS.message
        }

        return response
            .status(httpCode)
            .json({
                message: httpMessage
            })
    }
    catch (e) {
        next(e);
    }*/
};

export const listYourFollowers = async (request, response, next) => {
    try {
        let requestedParameter = request.params;
        const loggedUser = request.user;
        const listData = await listMyFollowersService({requestedParameter, loggedUser});

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: listData
            })
    }
    catch (e) {
        return next(e);
    }
};

export const listYourFollowings = async (request, response, next) => {
    try {
        let requestedParameter = request.params;
        const loggedUser = request.user;
        const listData = await listMyFollowingsService({requestedParameter, loggedUser});

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: listData
            })
    }
    catch (e) {
        return next(e);
    }
};

export const socialLinkFacebook = async (request, response, next) => {
    try {
        const serviceResponse = await facebookLinkService(request);

        response.set('accessToken', serviceResponse.tokenData.accessToken);
        response.set('refreshToken', serviceResponse.tokenData.refreshToken);
        response.set('expiry', serviceResponse.tokenData.expiry);

        return response
            .status(LOGIN.SUCCESS.httpCode)
            .json({
                message: LOGIN.SUCCESS.message,
                data: {
                    userProfile: serviceResponse.profile,
                    tokenData: serviceResponse.tokenData
                }
            });
    }
    catch (e) {
        return next(e);
    }
};

export const socialLinkGoogle = async (request, response, next) => {
    try {
        const serviceResponse = await googleLinkService(request);

        response.set("accessToken", serviceResponse.tokenData.accessToken);
        response.set("refreshToken", serviceResponse.tokenData.refreshToken);
        response.set("expiry", serviceResponse.tokenData.expiry);

        return response
            .status(LOGIN.SUCCESS.httpCode)
            .json({
                message: LOGIN.SUCCESS.message,
                data: {
                    userProfile: serviceResponse.profile,
                    ...serviceResponse.tokenData
                }
        });
    } catch (e) {
        return next(e);
    }
};

export const showYourReview = async (request, response, next) => {
    try {
        const { userId } = request.params;
        const loggedUser = request.user;
        const reviewListing = await favouriteTagService({tags, loggedUser});

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: reviewListing
            });
    }
    catch (e) {
        return next(e);
    }
};

export const blockUser = async (request, response, next) => {
    try {
        const { userId: userToBeBlocked } = request.params;
        const loggedUser = request.user;
        const blockUserResponse = await blockUserService({userToBeBlocked, loggedUser});

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: blockUserResponse
            });
    }
    catch (e) {
        return next(e);
    }
};

export const blockedUserList = async (request, response, next) => {
    try {
        const loggedUser = request.user;
        const blockedUserListResponse = await blockedUserListService(loggedUser);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: blockedUserListResponse
            });
    }
    catch (e) {
        return next(e);
    }
};

export const unBlockUser = async (request, response, next) => {
    try {
        const { userId: userToBeUnBlocked } = request.params;
        const loggedUser = request.user;
        const unBlockedUserResponse = await unBlockUserService({userToBeUnBlocked, loggedUser});

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: unBlockedUserResponse
            });
    }
    catch (e) {
        return next(e);
    }
};

export const userSuggestion = async (request, response, next) => {
    try {
        const { search } = request.query;
        const loggedUser = request.user;
        const userSuggestionResponse = await userSuggestionService({search, loggedUser});

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: userSuggestionResponse
            });
    }
    catch (e) {
        return next(e);
    }
};

export const forgotPassword = async (request, response, next) => {
    try {
        const { email } = request.params;
        await forgotPasswordService(email);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: {}
            });
    }
    catch (e) {
        return next(e);
    }
};

export const verifyToken = async (request, response, next) => {
  try {
    const { token } = request.params;
    const responseFromService = await accountVerificationService(
      token,
      system.VERIFICATION.FORGOT_PASSWORD
    );

    if (!responseFromService.success)
      return response.status(responseFromService.statusCode).json({
        message: responseFromService.message,
        data: {},
      });

    return response.status(responseFromService.statusCode).json({
      message: responseFromService.message,
      data: responseFromService.data._id,
    });
  } catch (e) {
    return next(e);
  }
};

export const resetForgotPassword = async (request, response, next) => {
  try {
    const { password, token } = request.body;
    const userInfo = await accountVerificationService(
      token,
      system.VERIFICATION.FORGOT_PASSWORD
    );

    if (userInfo.success && userInfo.data && userInfo.data && userInfo.data._id) {
        await updateUserService(userInfo.data._id, password, token);
        return response
        .status(200)
        .json({
            message: "Password has been updated.",
            data: {},
        }); 
    } else {
        throw new Error("Invalid or expired link.");
    }  
  } catch (e) {
    return next(e);
  }
};

export const userAccountDelete = async (request, response, next) => {
    try {
        const loggedUser = request.user;
        await accountDeleteService(loggedUser);
        return response
            .status(200)
            .json({
                message: "User account deleted successfully.",
                data: {},
            });
      } catch (e) {
        return next(e);
      }
};

export const loginSession = async () => {
    try {
        console.log("Logging with session.");   
    }
    catch(e) {
        return next(e);
    }
}

export const search = async (request, response, next) => {
    try {
        const loggedUser = request.user;
        const searchQuery = request.query;
        const searchedResponse = await placeAndUserSearchService(searchQuery, loggedUser);
        
        return response
            .status(200)
            .json({
                message: "User searched successfully.",
                data: searchedResponse
            });
      } catch (e) {
        return next(e);
      }
};

export const refreshFcmToken = async (request, response, next) => {
    try {
        const { deviceId, deviceToken } = request.body;

        await refreshFcmTokenService({ deviceId, deviceToken });

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                data: {}
            });
    }
    catch (error) {
        next(error);
    }
};