import { GENERAL } from '../../../helpers/constants/server-messages';
import { 
  listEventCategoryService,
} from "../../../services/eventCategoryService";

export const listCategories = async (request, response, next) => {
    try {
      const payload = request.query;
      const list = await listEventCategoryService(payload);
  
      return response
        .status(GENERAL.SUCCESS.httpCode)
        .json({
            message: GENERAL.SUCCESS.message,
            data: list
        });
    } catch (e) {
      return next(e);
    }
};
