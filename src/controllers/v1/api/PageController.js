import PageModel from '../../../database/models/Pages';
import { PAGE } from '../../../helpers/constants/server-messages';
import Errors from '../../../helpers/errors';
import { enumerations } from '../../../helpers/enumerations';
import { decodeHTMLEntities } from '../../../helpers/common-scripts';

const getPageBySlug = async ( request, response, next ) => {
    try {
        const {slug} = request.params;
        if (!enumerations.PageSlug.includes(slug))
            return next(Errors.invalidRequest(PAGE));

        const pageData = await PageModel.findBySlug(slug);

        if ( !pageData )
            return next(Errors.notFound(PAGE));

        return response
            .status(PAGE.SUCCESS.httpCode)
            .json({
                message: PAGE.SUCCESS.message,
                data: pageData
            });
        // return response.render('page', { pageContent: decodeHTMLEntities(pageData.content) });
    }
    catch (e) {
        return next(e);
    }
};

const createPage = async (request, response, next) => {
    try {
        const payload = request.body;
        const pageObject = new PageModel(payload);
        await pageObject.save();

        return response
            .status(PAGE.SUCCESS.httpCode)
            .json({
                message: PAGE.SUCCESS.message,
                data: pageObject
            });
    } catch (e) {
        return next(e);
    }
};

const updatePage = async (request, response, next) => {
    try {
        const body = request.body;
        const pageId = request.params.pageId;

        const updatedPage = await PageModel.findOneAndUpdate(
            { _id: pageId  },
            { $set: body },
            { useFindAndModify: false, new: true }
        );

        return response
            .status(PAGE.SUCCESS.httpCode)
            .json({
                message: PAGE.SUCCESS.message,
                data: updatedPage
            });
    } catch (e) {
        return next(e);
    }
};

const deletePage = async (request, response, next) => {
    try {
        const pageId = request.params.pageId;
        await PageModel.findByIdAndDelete(pageId);

        return response
            .status(PAGE.SUCCESS.httpCode)
            .json({
                message: PAGE.SUCCESS.message,
                data: {}
            });
    } catch (e) {
        return next(e);
    }
};

export default {
    createPage,
    updatePage,
    deletePage,
    getPageBySlug
};
