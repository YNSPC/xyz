import {
  chatHistoryService,
  chatUserListingService,
  removeChatFromMyselfService
} from "../../../services/chatService";
import { GENERAL } from "../../../helpers/constants/server-messages";

export const chatHistory = async (request, response, next) => {
  const user = request.user;
  const requestQuery = request.query;
  const requestParam = request.params;

  try {
    const history = await chatHistoryService(user, requestQuery, requestParam);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data: history
      });
  }
  catch (e) {
    return next(e);
  }
};

export const chatUserList = async (request, response, next) => {
  const user = request.user;
  const requestQuery = request.query;
  console.log(requestQuery);

  try {
    const userListing = await chatUserListingService(user, requestQuery);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data: userListing
      });
  }
  catch (e) {
    return next(e);
  }
};

export const removeChatFromMyself = async (request, response, next) => {
  const user = request.user;
  const { friendId } = request.params;

  try {
    await removeChatFromMyselfService(user, friendId);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data: {}
      });
  }
  catch (e) {
    return next(e);
  }
};