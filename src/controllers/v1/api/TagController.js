import {GENERAL, PLACE} from '../../../helpers/constants/server-messages';

import {getValidPage, paginateProvider} from "../../../helpers/common-scripts";
import TagModel from "../../../database/models/Tags";
import fileUpload from "../../../modules/fileUpload";
import {uploadConfig} from "../../../config";
import tagsRepository from "../../../repositories/tagsRepository";

const list = async ( request, response, next ) => {
    try {
        const page = await getValidPage(request.query.page);
        const { sortBy, search, sortDesc, itemsPerPage } = request.query;

        const options = {
            limit: 100, //itemsPerPage,//design specific
            page,
            search,
            sortBy,
            sortDesc
        };

        const aggregateQuery = TagModel.getTags(options);
        const tags = await TagModel.aggregatePaginate(aggregateQuery, options);
        const getPagination = await paginateProvider(tags);

        return response
            .status(GENERAL.SUCCESS.httpCode)
            .json({
                message: GENERAL.SUCCESS.message,
                ...getPagination
            });
    }
    catch (e) {
        return next(e);
    }
};

const uploadImages = async ( request, response, next ) => {
    try {
        const fileUploader = new fileUpload('tags', false);
        const uploadedResponse = await fileUploader.multipleUpload('tag_icon', request, response, uploadConfig.image);
        let { identifier } = request.body;
        const uploadedResult = await tagsRepository.uploadImages(identifier, uploadedResponse);

        return response
            .status(PLACE.IMAGE_UPLOAD.SUCCESS.httpCode)
            .json({
                message: PLACE.IMAGE_UPLOAD.SUCCESS.message,
                data: uploadedResult
            });
    }
    catch (e) {
        return next(e);
    }
};

export default {
    list,
    uploadImages
};
