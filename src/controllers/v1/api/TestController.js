import mongoose from "mongoose";
import tagModel from "../../../database/models/Tags";
import { 
    storeTagViaApi, 
    updateTagViaApi 
} from "../../../repositories/tagsRepository";

export const updateTest = async (request, response, next) => {
    try {
        console.log("testing update");
        const payload = {
            // name: "tag1",
            // icon: "ICON1",
            color: "RED",
            // isPremium: false,
        };

        // const result = await storeTagViaApi(payload);
        const result = await updateTagViaApi("62578fe02928552454f82fcc", payload);
        if (!result) {
            return response
                .status(400)//not able to complete task
                .json({
                    message: "Unable to complete the update.",
                    data: {}
                })
        }

        // const cloned = { ...result }
        // console.log("direct", result);
        // console.log("direct fetch", result.name);
        // console.log("cloned", cloned);
        // console.log("cloned fetch", cloned.name);

        // 62579148aaf158188034c537
        // 625790ec4c194e36c438ddf3
        // 62578fe02928552454f82fcc

        return response
            .status(200)
            .json({
                message: "test completed",
                data: result
            });
    }
    catch (e) {
        console.log("error", e);
        next(e);
    }
};

export const transactionTest = async (request, response, next) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
        console.log("am i called first");
        const payload = {
            name: "transaction",
            color: "red",
            icon: "url for icon",
            isPremium: true
        }
        //add the record
        // const tag = await tagModel.create([payload], {session});
        // console.log(tag);

        const tagObject = new tagModel(payload);
        const tag = await tagObject.save({session});
        console.log('created', tag);

        //update the same record 
        const updatedTag = await tagModel.updateOne(
            {
                _id: tag._id
            },
            {
                $set: {
                    name: "transaction3"
                }
            },
            {
                session
            }
        )
        console.log("updated", updatedTag, updatedTag.nModified);

        //delete the same record
        const deletedTag = await tagModel.deleteOne({ color: "green" }, {session});
        console.log('delete', deletedTag, deletedTag.deletedCount);

        await session.commitTransaction();
        
        return response
            .status(200)
            .json({
                message: "test transaction completed",
                data: {}
        });
    }
    catch(e) {
        console.log("am i called second");
        await session.abortTransaction();
        return next(e);
    }
    finally {
        console.log("always finally here");
        session.endSession();
    }
};

export const checkEnvironment = async (request, response, next) => {
    try {
        const environment = process.env.NODE_ENV;
        return response
            .status(200)
            .json({
                message: "test completed",
                data: environment ? environment : "N/A"
            });
    }
    catch (e) {
        console.log("error", e);
        next(e);
    }
};
