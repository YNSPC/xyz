const rewire = require("rewire")
const ReportController = rewire("../ReportController")
const reportToPost = ReportController.__get__("reportToPost")
// @ponicode
describe("reportToPost", () => {
    test("0", async () => {
        await reportToPost({ user: "Anas", body: "role" }, 429, () => true)
    })

    test("1", async () => {
        await reportToPost({ user: "Jean-Philippe", body: "key" }, 200, () => false)
    })

    test("2", async () => {
        await reportToPost({ user: "Jean-Philippe", body: "location" }, 429, () => true)
    })

    test("3", async () => {
        await reportToPost({ user: "Edmond", body: "key" }, 429, () => true)
    })

    test("4", async () => {
        await reportToPost({ user: "George", body: "effect" }, 429, () => false)
    })

    test("5", async () => {
        await reportToPost({ user: "", body: "" }, Infinity, () => false)
    })
})
