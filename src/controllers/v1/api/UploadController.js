import {GENERAL, PLACE} from "../../../helpers/constants/server-messages";
import fileUpload from "../../../modules/fileUpload";
import {uploadConfig} from "../../../config";
import sharp from "sharp";
import path from "path";
import fs from "fs";
import {fullURL} from "../../../helpers/common-scripts";
import { copyFileToGCS } from "../../../modules/googleCloudService";
import { generateSignedUrl, generateSignedViewUrl } from "../../../services/uploadMediaService";

const upload = async ( request, response, next ) => {
    try {
        const { folderName, mediaType, uploadTo } = request.query;
        const fileUploader = new fileUpload(folderName, false);
        const uploadedResponse = await fileUploader.singleUpload('fileData', request, response, uploadConfig.image);
        const requestFile = request.file;
        let googleCloudResponse;

        switch (uploadTo) {
            case "google":
                googleCloudResponse = await copyFileToGCS(requestFile);
                break;
        
            default:
                break;
        }
        /*f
        if ( mediaType !== 'image' )
            return response
                .status(GENERAL.BAD_REQUEST.httpCode)
                .json({
                    message: GENERAL.BAD_REQUEST.message,
                    data: {}
                });

        let urls = [];
        for ( let data of uploadedResponse) {
            await sharp(data.path)
                .webp()
                .toFile(path.resolve(`${data.destination}`, 'sharp/', `${data.filename.split(".")[0]}.webp`));

            urls.push(fullURL(folderName, data.filename));
            fs.unlinkSync(data.path);
        }*/

        return response
            .status(PLACE.IMAGE_UPLOAD.SUCCESS.httpCode)
            .json({
                message: PLACE.IMAGE_UPLOAD.SUCCESS.message,
                data: googleCloudResponse
            });
    }
    catch (e) {
        return next(e);
    }
};

const preSignedUrl = async ( request, response, next ) => {
    try {
        const queryData = request.query;

        const signedUrlResponse = await generateSignedUrl(queryData);
        return response
            .status(200)
            .json({
                message: "Signed url generated successfully.",
                data: signedUrlResponse
            });
    }
    catch (error) {
        next(error);
    }
};

const preSignedViewUrl = async ( request, response, next ) => {
    try {
        const queryData = request.query;

        const signedUrlResponse = await generateSignedViewUrl(queryData);
        return response
            .status(200)
            .json({
                message: "Signed url generated successfully.",
                data: signedUrlResponse
            });
    }
    catch (error) {
        next(error);
    }
};

export default {
    upload,
    preSignedUrl,
    preSignedViewUrl
};
