import mongoose from "mongoose";
import {
  createCommentService,
  commentListingService,
  likeCommentService,
  unLikeCommentService
} from "../../../services/commentService";
import { GENERAL } from "../../../helpers/constants/server-messages";

export const postComment = async (request, response, next) => {
  const user = request.user;
  const payload = request.body;
  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    const comment = await createCommentService(payload, user, session);
    await session.commitTransaction();

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data: comment
      });
  }
  catch (e) {
    await session.abortTransaction();
    return next(e);
  }
  finally {
    session.endSession();
  }
};

export const editComment = async () => { };

export const deleteComment = async () => { };

export const listComment = async (request, response, next) => {
  try {
    const loggedUser = request.user;
    const { page, limit } = request.query;
    const { postId } = request.params;
    const options = {
      page: page || 1,
      limit: limit || 10,
      sort: {
        createdAt: 1
      }
    }
    const { docs: data, ...pagination } = await commentListingService(postId, options, loggedUser);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data,
        pagination,
      });

  } catch (e) {
    return next(e);
  }
};

export const likeComment = async (request, response, next) => {
  try {
    const user = request.user;
    const { commentId } = request.params;

    const commentData = await likeCommentService(commentId, user);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data: commentData
      });
  }
  catch (e) {
    return next(e);
  }
};

export const unLikeComment = async (request, response, next) => {
  try {
    const user = request.user;
    const { commentId } = request.params;

    const commentData = await unLikeCommentService(commentId, user);

    return response
      .status(GENERAL.SUCCESS.httpCode)
      .json({
        message: GENERAL.SUCCESS.message,
        data: commentData
      });
  }
  catch (e) {
    return next(e);
  }
};
