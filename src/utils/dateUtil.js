const currentDateTimeConsole = () => {
    const date = new Date();

    return date.toLocaleString('en-US', {
        year: 'numeric',
        month: 'long',
        weekday: 'long',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
    });
};

module.exports = {
    currentDateTimeConsole: currentDateTimeConsole()
};
