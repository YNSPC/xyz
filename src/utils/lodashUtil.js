import lodash from "lodash";

export const objectValidity = (object, path, def) => {
  return lodash.get(object, path, def);
};

export const uniqueCodeGen = (size, option) => {
  let letters = "";
  let code = "";
  switch (option.type) {
    case "numeric":
      letters = "1234567890";
      break;

    case "alpha-numeric":
      letters = "1234567890abcdefghijklmnopqrstuvwxyz";
      break;
    
    case "alpha-numeric-symbol":
      letters = "1234567890abcdefghijklmnopqrstuvwxyz!$&*()_-+=";
      break;

    case "alpha":
      letters = "abcdefghijklmnopqrstuvwxyz";
      break;

    case "alpha-symbol":
      letters = "abcdefghijklmnopqrstuvwxyz!$&*()_-+=";
      break;
    default:
      letters = "";
      break;
  }

  while (size !== 0) {
    code += letters.charAt(Math.random()*letters.length);
    size--;
  }
  
  return code;
};
