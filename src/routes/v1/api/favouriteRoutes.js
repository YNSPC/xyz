import expressRouter from 'express-promise-router';
import { passportVerification } from '../../../middlewares/strategies/appJwt';
import placeController from "../../../controllers/v1/api/PlaceController";

const router = expressRouter();

/**
 * @typedef placeFavouriteModel
 * @property {string} placeId.required
 */
/**
 * Toggle favourite the place.
 * @route POST /favourite/toggle
 * @group Favourite - Favourite
 * @operationId toggle favourite place
 * @param {placeFavouriteModel.model} Place.body.required - (placeId is required)
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/toggle')
    .post(
        passportVerification,
        placeController.makeFavouriteToggle
    );

/**
 * List favourite places.
 * @route GET /favourite/place
 * @group Favourite - Favourite
 * @operationId List favourite place
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/place')
    .get(
        passportVerification,
        placeController.listMyFavouritePlaces
    );


export default router;
