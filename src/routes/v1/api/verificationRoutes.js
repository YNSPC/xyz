import expressRouter from 'express-promise-router';
// import { requireJsonContent, validateBody } from '../../../middlewares/routes';
// import { validationSchemas } from '../../../request/userSchemaValidation';
// import { passportVerification } from '../../../middlewares/strategies/appJwt';
import { verifyAccount, resendAccountVerification } from "../../../controllers/v1/api/VerificationController";

const router = expressRouter();

/**
 * Resend Account Verification
 * @route POST /verification/resend-for-account/{email}
 * @param {string} email.path
 * @group Verification
 * @operationId resendVerificationToken
 * @produces application/json
 * @consumes application/json
 * @returns {registrationResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 */
router
    .route('/resend-for-account/:email')
    .post(resendAccountVerification);

/**
 * Verification of Account
 * @route GET /verification/account/{verificationToken}
 * @param {string} verificationToken.path
 * @group Verification - Verify account
 * @operationId verifyToken
 * @produces application/json
 * @consumes application/json
 * @returns {registrationResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 */
router
    .route('/account/:verificationToken')
    .get(verifyAccount);

export default router;
