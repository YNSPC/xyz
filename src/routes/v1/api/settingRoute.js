import expressRouter from 'express-promise-router';
import settingController from "../../../controllers/v1/api/SettingController";

const router = expressRouter();

router
  .route('/remote-config')
  .get(
    settingController.remoteConfig
  );

export default router;
