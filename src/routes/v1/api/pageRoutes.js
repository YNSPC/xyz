import expressRouter from 'express-promise-router';
import pageController from "../../../controllers/v1/api/PageController";
import { validateBody } from '../../../middlewares/routes';
import { validationSchemas } from '../../../request/pageSchemaValidation';

const router = expressRouter();

/**
 * Privacy policy of yatribhet
 * @route GET /page/{slug}
 * @group Page
 * @operationId getPageBasedOnSlug
 * @param {string} slug.path.required - slug - eg:privacy-policy
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 */

router
    .route('/:slug')
    .get(
        pageController.getPageBySlug
    );

/**
 * Create pages for yatribhet
 * @route POST /page
 * @group Page
 * @operationId createPage
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 */

router
    .route('/')
    .post(
        validateBody(validationSchemas.create, { abortEarly: true }),
        pageController.createPage
    );

/**
 * Update your page
 * @route PATCH /page/{pageId}
 * @group Page
 * @operationId updatePage
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 */

router
    .route('/:pageId')
    .patch(
        validateBody(validationSchemas.update, { abortEarly: true }),
        pageController.updatePage
    );

/**
 * Delete your page
 * @route DELETE /page/{pageId}
 * @group Page
 * @operationId deletePage
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 */

router
    .route('/:pageId')
    .delete(
        pageController.deletePage
    );

export default router;
