import expressRouter from 'express-promise-router';
import { passportVerification } from '../../../middlewares/strategies/appJwt';
import { listCategories } from "../../../controllers/v1/api/EventCategoryController";

const router = expressRouter();

/**
 * Retrieve all the places.
 * @route GET /event-category
 * @group EventCateogry - Category listing
 * @operationId retrieve all categories
 * @param {string} search.query - Optional
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/event-category')
    .get(
        passportVerification,
        listCategories
    );

export default router;
