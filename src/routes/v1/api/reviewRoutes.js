import expressRouter from 'express-promise-router';
import { passportVerification } from '../../../middlewares/strategies/appJwt';
import placeController from "../../../controllers/v1/api/PlaceController";
import {requireJsonContent, validateBody} from "../../../middlewares/routes";
import {validationSchemas} from "../../../request/reviewSchemaValidation";

const router = expressRouter();

/**
 * @typedef createReviewPlaceModel
 * @property {string} placeId.required
 * @property {string} review.required
 * @property {number} rating.required
 */
/**
 * Create review.
 * @route POST /review
 * @group Review - Create review
 * @operationId create
 * @param {createReviewPlaceModel.model} Review.body.required - (placeId, review, rating are required)
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
    .route('/')
    .post(
        passportVerification,
        requireJsonContent,
        validateBody( validationSchemas.create, {abortEarly:true} ),
        placeController.createReview
    );

/**
 * @typedef editReviewPlaceModel
 * @property {string} review.required
 * @property {number} rating.required
 */
/**
 * Edit review.
 * @route PATCH /review/{reviewId}
 * @group Review
 * @operationId update
 * @param {editReviewPlaceModel.model} Review.body.required - (review, rating are required)
 * @param {string} reviewId.path - required
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/:reviewId')
    .patch(
        passportVerification,
        placeController.updateReview
    );

/**
 * Delete review.
 * @route DELETE /review/{reviewId}
 * @group Review
 * @operationId delete review
 * @param {string} reviewId.path - required
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/:reviewId')
    .delete(
        passportVerification,
        placeController.deleteReview
    );

/**
 * List reviews of place
 * @route GET /review/{placeId}
 * @group Review
 * @operationId List reviews of place
 * @param {string} placeId.path - required
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/:placeId')
    .get(
        passportVerification,
        placeController.listReviewOfPlace
    );

export default router;
