import expressRouter from 'express-promise-router';
import {
    updateTest,
    transactionTest,
    checkEnvironment
} from "../../../controllers/v1/api/TestController";
import { requireJsonContent, validateBody } from '../../../middlewares/routes';
import { validationSchemas } from '../../../request/userSchemaValidation';
const router = expressRouter();

router
    .route('/update')
    .get(updateTest);

router
    .route('/transaction')
    .get(transactionTest);

/**
 * Show environment.
 * @route GET /test/check-environment
 * @group Test - check environment
 * @operationId environmentTest
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 */
router
    .route('/check-environment')
    .get(checkEnvironment);

export default router;
