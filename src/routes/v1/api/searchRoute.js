import expressRouter from 'express-promise-router';
import { passportVerification } from '../../../middlewares/strategies/appJwt';
import { search } from "../../../controllers/v1/api/UserController";

const router = expressRouter();

/**
 * Search place and user.
 * @route GET /
 * @group search - search
 * @operationId retrieve all searchs
 * @param {string} search.query - Optional
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/')
    .get(
        passportVerification,
        search
    );

export default router;
