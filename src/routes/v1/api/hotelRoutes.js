import expressRouter from 'express-promise-router';
import hotelController from "../../../controllers/v1/api/HotelController";
import {passportVerification} from "../../../middlewares/strategies/appJwt";
import { requireJsonContent, validateBody, validateParams } from '../../../middlewares/routes';
import { validationSchemas } from '../../../request/hotelSchemaValidation';

const router = expressRouter();

/**
 * Hotels detail information.
 * @route GET /hotel/{hotelId}
 * @group Hotel - Hotel
 * @operationId hotelDetail
 * @param {string} hotelId.path - Optional
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
    .route('/:hotelId')
    .get(
        passportVerification,
        validateParams(validationSchemas.detail),
        hotelController.hotelDetailById
    );

/**
 * All hotels listing.
 * @route GET /hotel
 * @group Hotel - Hotel listing
 * @operationId hotelListing
 * @param {string} search.query - Optional - eg: placename|hotelname
 * @param {number} rating.query - Optional
 * @param {number} minimum.query - Optional
 * @param {number} maximum.query - Optional
 * @param {number} page.query - Optional
 * @param {number} limit.query - Optional
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
    .route('/')
    .get(
        passportVerification,
        hotelController.listAllHotelMenu
    );

export default router;
