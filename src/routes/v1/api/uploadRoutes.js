import expressRouter from "express-promise-router";
import uploadController from "../../../controllers/v1/api/UploadController";
import { passportVerification } from "../../../middlewares/strategies/appJwt";
import { awsConfig } from "../../../config";	

const router = expressRouter();

/**
 * @typedef uploadMediaModel
 * @property {string} title.required
 * @property {enum} postType.required
 * /
 /**
 * Upload of media.
 * @route POST /media
 * @group Upload - Upload
 * @operationId uploadMedia
 * @param {file} fileData.formData.required - your image
 * @param {enum} folderName.query - folderName - eg:posts,tags,places
 * @param {enum} mediaType.query - mediaType - eg:image,video
 * @param {enum} uploadTo.query - uploadTo - eg:local,s3,cloudinary,google
 * @consumes multipart/form-data
 * @produces application/json
 * @returns { validationResponse.model } 200 - Success
 */
router.route("/").post(uploadController.upload);

 /**
 * Generate signed url.
 * @route GET /media/pre-signed-url
 * @group Upload - GenerateSignedUrl
 * @operationId signedUrl
 * @param {enum} folder.query - folder - eg:posts,tags,places
 * @param {enum} contentType.query - contentType - eg:image,video
 * @param {enum} extension.query - extension - eg:png,jpg,jpeg,gif
 * @param {enum} service.query - uploadTo - eg:s3,cloudinary,google
 * @consumes multipart/form-data
 * @produces application/json
 * @returns { validationResponse.model } 200 - Success
 */
router.route("/pre-signed-url").get(uploadController.preSignedUrl);

router.route("/pre-signed-view-url").get(uploadController.preSignedViewUrl);

export default router;
