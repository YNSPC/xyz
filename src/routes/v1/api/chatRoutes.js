import expressRouter from 'express-promise-router';
import { chatHistory, chatUserList, removeChatFromMyself } from "../../../controllers/v1/api/ChatController";
import { validateQuery } from "../../../middlewares/routes";
import { validationSchemas } from "../../../request/common/paginationSchemaValidation";
import { passportVerification } from '../../../middlewares/strategies/appJwt';

const router = expressRouter();

router
  .route('/user-list')
  .get(
    passportVerification,
    validateQuery(validationSchemas.paginate, { abortEarly: true }),
    chatUserList
  );

router
  .route('/:friendId')
  .get(
    passportVerification,
    validateQuery(validationSchemas.paginate, { abortEarly: true }),
    chatHistory
  );


router
  .route('/:friendId')
  .delete(
    passportVerification,
    removeChatFromMyself
  );
export default router;
