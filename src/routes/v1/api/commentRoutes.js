import expressRouter from 'express-promise-router';
import {passportVerification} from "../../../middlewares/strategies/appJwt";
import {
    deleteComment,
    editComment,
    listComment,
    postComment,
    likeComment,
    unLikeComment
} from "../../../controllers/v1/api/CommentController";
import {validateBody} from "../../../middlewares/routes";
import {validationSchemas} from "../../../request/commentSchemaValidation";

const router = expressRouter();

/**
 * @typedef commentModel
 * @property {string} data.required
 * @property {string} dataType.required -   - eg: text|image|video|link
 */

/**
 * @typedef createCommentModel
 * @property {string} postId.required - post where you are commenting
 * @property {Array.<commentModel>} comment - your comment
 * @property {string} replyTo - post-comment to which you are replying
 */

/**
 * Create new comment
 * @route POST /comment
 * @group Comment - Create new comment
 * @param {createCommentModel.model} Comment.body.required - (FirstName, password, email and userName are required)
 * @operationId postComment
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/')
    .post(
        passportVerification,
        validateBody(validationSchemas.create, { abortEarly: true }),
        postComment
    );

/**
 * Edit your comment.
 * @route PATCH /comment/{commentId}
 * @group Comment - Edit comment
 * @operationId editComment
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/:commentId')
    .patch(
        passportVerification,
        editComment
    );

/**
 * Delete your comment.
 * @route DELETE /comment/{commentId}
 * @group Comment - Delete comment
 * @operationId deleteComment
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/:commentId')
    .delete(
        passportVerification,
        deleteComment
    );


/**
 * List comments in post.
 * @route GET /comment/post/{postId}
 * @group Comment - Edit comment
 * @operationId listComments
 * @param {string} postId.path - postId - eg: postId
 * @param {string} page.query - page - eg: 1
 * @param {string} limit.query - limit - eg: 10
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/post/:postId')
    .get(
        passportVerification,
        listComment
    );

/**
 * Like a comment.
 * @route PATCH /comment/{commentId}/like
 * @group Comment - Like
 * @operationId likeAComment
 * @param {string} commentId.path - commentId
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
    .route('/:commentId/like')
    .patch(
        passportVerification,
        likeComment
    );

/**
 * UnLike a comment.
 * @route DELETE /comment/{commentId}/un-like
 * @group Comment - Un Like
 * @operationId unLikeAComment
 * @param {string} commentId.path - commentId
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
    .route('/:commentId/un-like')
    .delete(
        passportVerification,
        unLikeComment
    );

export default router;
