import expressRouter from 'express-promise-router';
import reportController from "../../../controllers/v1/api/ReportController";
import {requireJsonContent, validateBody} from "../../../middlewares/routes";
import {validationSchemas} from "../../../request/reportSchemaValidation";
import { passportVerification } from '../../../middlewares/strategies/appJwt';

const router = expressRouter();

/**
 * @typedef reportCreateModel
 * @property {string} comment.required
 * @property {enum} reportedOn.required - reportedOn - eg:post,user,comment
 * @property {string} reportedBy.required
 * @property {string} identity.required
 * @property {boolean} silentNotification.required - silentNotification - eg:false
 * @property {boolean} silentUser.required - silentUser - eg:false
 */

/**
 * Make a report.
 * @route POST /report
 * @group Report - Report
 * @operationId createAReport
 * @param {reportCreateModel.model} Report.body.required
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/')
    .post(
        passportVerification,
        validateBody(validationSchemas.create, { abortEarly: true }),
        reportController.makeAReport
    );

export default router;
