import expressRouter from 'express-promise-router';
import csvController from "../../../controllers/v1/api/CsvController";

const router = expressRouter();

router
    .route('/import-tags')
    .post(
        csvController.importTags
    );

router
    .route('/import-places')
    .post(
        csvController.importPlaces
    );

router
    .route('/import-routes')
    .post(
        csvController.importRoutes
    );

router
    .route('/import-sub-routes')
    .post(
        csvController.importSubRoutes
    );

export default router;
