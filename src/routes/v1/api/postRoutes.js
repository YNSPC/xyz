import expressRouter from 'express-promise-router';
import { requireJsonContent, validateBody } from '../../../middlewares/routes';
import { validationSchemas } from '../../../request/postSchemaValidation';
import postController from "../../../controllers/v1/api/PostController";
import {passportVerification} from "../../../middlewares/strategies/appJwt";
import {headerToVerifyGuestLogin} from "../../../middlewares/strategies/guestLogin";

const router = expressRouter();

/**
 * Listing of posts.
 * @route GET /post
 * @group Post - Post
 * @operationId listPosts
 * @param {number} page.query - Optional
 * @param {number} limit.query - Optional
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/')
    .get(
        headerToVerifyGuestLogin,
        postController.listPosts
    );

/**
 * @typedef mediaUrl
 * @property {string} smallUrl - smallUrl
 * @property {string} mediumUrl - mediumUrl
 * @property {string} largeUrl - largeUrl
 * @property {string} originalUrl - originalUrl
 */

/**
 * @typedef mediaFile
 * @property {enum} mediaType - mediaType - eg:image,video
 * @property {mediaUrl.model} mediaUrl - mediaUrl
 */

/**
 * @typedef tripInfo
 * @property {string} currentDay.required - currentDay
 * @property {string} description.required - description
 * @property {Array.<mediaFile>} media - media files details
 * @property {string} location.required
 * @property {Array.<string>} tags - tags
 */

/**
 * @typedef postCreateModel
 * @property {string} title.required
 * @property {enum} postType.required - postType - eg:trip,iv
 * @property {enum} visibility.required - postType - eg:public,private
 * @property {string} description.required
 * @property {string} location.required
 * @property {Array.<string>} tags
 * @property {Array.<mediaFile>} media - media files details
 * @property {string} tripStart - start date of trip
 * @property {mediaFile.model} tripCoverPhoto - tripCoverPhoto
 * @property {Array.<tripInfo>} tripInfo - about trip information
 *
 */

/**
 * Create new post.
 * @route POST /post
 * @group Post - Post
 * @operationId createPosts
 * @param {postCreateModel.model} Post.body.required
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
    .route('/')
    .post(
        passportVerification,
        validateBody(validationSchemas.create, { abortEarly: true }),
        postController.createPost
    );

/**
 * Like a post.
 * @route PATCH /post/{postId}/like
 * @group Post - Like
 * @operationId likeAPosts
 * @param {string} postId.path - postId
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
    .route('/:postId/like')
    .patch(
        passportVerification,
        postController.likeAPost
    );

/**
 * UnLike a post.
 * @route DELETE /post/{postId}/un-like
 * @group Post - Un Like
 * @operationId unLikeAPosts
 * @param {string} postId.path - postId
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
    .route('/:postId/un-like')
    .delete(
        passportVerification,
        postController.UnLikeAPost
    );

/**
 * Get post from userId.
 * @route GET /post/user/{userId}
 * @group Post - postFromUser
 * @operationId postFromUser
 * @param {string} userId.path - userId - eg:123456
 * @param {string} page.query - page - eg: 1
 * @param {string} limit.query - limit - eg: 10
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
    .route('/user/:userId?')
    .get(
        passportVerification,
        postController.postFromUser
    );

/**
 * Get post detail page.
 * @route GET /post/{postId}
 * @group Post - postDetail
 * @operationId postDetail
 * @param {string} postId.path - postId
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
    .route('/:postId')
    .get(
        passportVerification,
        postController.postDetail
    );


/**
 * Get likes on post.
 * @route GET /post/{postId}/like
 * @group Post - likeInPost
 * @operationId likeInPost
 * @param {string} postId.path - postId
 * @param {string} page.query - postId
 * @param {string} limit.query - postId
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
    .route('/:postId/like')
    .get(
        passportVerification,
        postController.likesInPost
    );

/**
 * Delete my post.
 * @route DELETE /post/{postId}
 * @group Post - delete post
 * @operationId postDelete
 * @param {string} postId.path - postId
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
.route('/:postId')
.delete(
    passportVerification,
    postController.postDelete
);

export default router;
