import expressRouter from 'express-promise-router';
import placeController from "../../../controllers/v1/api/PlaceController";
import hotelController from "../../../controllers/v1/api/HotelController";
import { passportVerification } from '../../../middlewares/strategies/appJwt';
import { validateParams, validateBody, requireJsonContent } from '../../../middlewares/routes';
import { validationSchemas } from '../../../request/placeSchemaValidation';

const router = expressRouter();

/**
 * Retrieve all the places.
 * @route GET /place/my-places
 * @group Place - Place
 * @operationId retrieveAllPlaces
 * @param {number} page.query - Optional
 * @param {string} search.query - Optional
 * @param {number} limit.query - Optional
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
    .route('/my-places')
    .get(
        passportVerification,
        placeController.myPlaces
    );

/**
 * Detail of the selected place.
 * @route GET /place/{placeId}
 * @group Place - Place
 * @operationId detailOfPlace
 * @param {string} placeId.path
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/:placeId')
    .get(
        passportVerification,
        validateParams(validationSchemas.detail),
        placeController.placeDetail
    );// TODO: recommended places based upon the logged user tags

/**
 * Retrieve hotels withing given place.
 * @route GET /place/{placeId}/hotels
 * @group Place - Place
 * @operationId hotelsRelatedToPlace
 * @param {string} placeId.path - Optional
 * @param {number} page.query - Optional
 * @param {string} search.query - Optional
 * @param {number} minimumOfPriceRange.query - Optional
 * @param {number} maximumOfPriceRange.query - Optional
 * @param {number} rating.query - Optional
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router
    .route('/:placeId/hotels')
    .get(
        passportVerification,
        hotelController.listInGivenPlace
    );

/*cms route*/
router
    .route('/upload-images')
    .patch(
        placeController.uploadImages
    );

export default router;
