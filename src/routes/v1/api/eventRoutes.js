import expressRouter from 'express-promise-router';
import {
    createEvent,
    updateEvent,
    sendInvitation,
    responseToEvent,
    deleteEvent,
    listEvent,
    eventHistory,
    detailOfEvent,
    joinValidEvent
} from "../../../controllers/v1/api/EventController";
import { requireJsonContent, validateBody, validateParams } from '../../../middlewares/routes';
import { validationSchemas } from '../../../request/eventSchemaValidation';
import { passportVerification } from '../../../middlewares/strategies/appJwt';

const router = expressRouter();

/**
 * @typedef memberInvitation
 * @property {string} userId.required - userId
 * @property {string} invitedOn.required - invitedOn
 * @property {string} updatedAt.required - updatedAt
 * @property {enum} status.required - status - eg:joined
 */

/**
 * @typedef eventModel
 * @property {string} name.required - name - eg:Event name
 * @property {string} to.required
 * @property {string} from.required
 * @property {string} eventStart.required
 * @property {string} eventEnd.required
 * @property {enum} status.required - status - eg:public,private
 * @property {string} location.required
 * @property {boolean} private.required - private - eg:false
 * @property {Array.<memberInvitation>} members - about members to invite
 */

 
 /**
 * @typedef inviteResponseModel
 * @property {string} eventId.required - name - eg:Event id
 * @property {string} status.required - name - eg:status
 */

/**
 * @typedef eventResponseModel
 * @property {string} message:Success
 */

/**
 * Create an event
 * @route POST /event
 * @param {eventModel.model} Event.body.required
 * @group Event - Event
 * @operationId createEvent
 * @produces application/json
 * @consumes application/json
 * @returns {eventResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 * @security JWT
 */

router
    .route('/')
    .post(
        passportVerification,
        // requireJsonContent,
        validateBody( validationSchemas.create, {abortEarly:true} ),
        createEvent
    );

/**
 * Update an event
 * @route PATCH /event/{eventId}
 * @param {string} eventId.path - required
 * @param {eventModel.model} Event.body.required
 * @group Event - Event
 * @operationId updateEvent
 * @produces application/json
 * @consumes application/json
 * @returns {eventResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 * @security JWT
 */

router
    .route('/:eventId')
    .patch(
        passportVerification,
        requireJsonContent,
        validateParams( validationSchemas.mongoId ),
        validateBody( validationSchemas.update, {abortEarly:true} ),
        updateEvent
    );

/**
 * Invitation to members
 * @route PATCH /event/{eventId}/invite/{userId}
 * @param {string} eventId.path - required
 * @param {string} userId.path - required
 * @group Event - Event
 * @operationId userInvitation
 * @produces application/json
 * @consumes application/json
 * @returns {eventResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 * @security JWT
 */

 router
    .route('/:eventId/invite/:userId')
    .patch(
        passportVerification,
        validateParams( validationSchemas.invitationToEvent ),
        sendInvitation
    );

 /**
 * Response from invited members
 * @route PATCH /event/response-to-invitation
 * @param {string} eventId.path - required
 * @param {inviteResponseModel.model} EventResponse.body.required
 * @group Event - Event
 * @operationId responseToInvitation
 * @produces application/json
 * @consumes application/json
 * @returns {eventResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 * @security JWT
 */

router
    .route('/:eventId/response-to-invitation')
    .post(
        passportVerification,
        requireJsonContent,
        validateBody( validationSchemas.responseToInvitation, {abortEarly:true} ),
        responseToEvent
    );

/**
 * Delete event by admin of event
 * @route DELETE /event/{eventId}
 * @param {string} eventId.path - required
 * @group Event - Event
 * @operationId deleteEvent
 * @produces application/json
 * @consumes application/json
 * @returns {eventResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 * @security JWT
 */

router
    .route('/:eventId')
    .delete(
        passportVerification,
        validateParams( validationSchemas.mongoId ),
        deleteEvent
    );

/**
 * List all event
 * @route GET /event
 * @group Event - Event
 * @operationId listEvents
 * @param {number} page.query - Optional
 * @param {number} limit.query - Optional
 * @produces application/json
 * @consumes application/json
 * @returns {eventResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 * @security JWT
 */

 router
 .route('/')
 .get(
     passportVerification,
    //  requireJsonContent,
     listEvent
 );

 /**
 * Event detail with chat history
 * @route GET /event/{eventId}
 * @group Event - Event
 * @operationId eventDetail
 * @param {number} page.query - Optional
 * @param {number} limit.query - Optional
 * @produces application/json
 * @consumes application/json
 * @returns {eventResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 * @security JWT
 */

  router
  .route('/:eventId')
  .post(
      passportVerification,
      requireJsonContent,
      eventHistory
  );
  
 /**
 * Event detail with chat history
 * @route GET /event/{eventId}
 * @group Event - Event
 * @operationId eventDetail
 * @param {string} eventId.path - required
 * @produces application/json
 * @consumes application/json
 * @returns {eventResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 * @security JWT
 */
 router
 .route('/:eventId')
 .get(
     passportVerification,
     detailOfEvent
 );

 /**
 * Join an event
 * @route PATCH /event/{eventId}/join
 * @param {string} eventId.path - required
 * @group Event - Event
 * @operationId joinEvent
 * @produces application/json
 * @consumes application/json
 * @returns {eventResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 * @security JWT
 */

router
.route('/:eventId/join')
.patch(
    passportVerification,
    validateParams( validationSchemas.mongoId ),
    joinValidEvent
);

export default router;
