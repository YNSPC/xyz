import { listAllNotifications } from "../../../controllers/v1/api/NotificationController";
import { passportVerification } from '../../../middlewares/strategies/appJwt';

export default function notificationRoutes(prefix, app) {
    /**
     * Retrieve all the notifications.
     * @route GET /notification
     * @group Notification - Notification
     * @operationId retrieveAllNotification
     * @param {number} page.query - Optional
     * @param {number} limit.query - Optional
     * @produces application/json
     * @consumes application/json
     * @returns { validationResponse.model } 200 - Success
     * @security JWT
     */
    app.get(`${prefix}`,
        passportVerification,
        listAllNotifications
    );

};
