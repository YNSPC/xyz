import expressRouter from 'express-promise-router';
// import { requireJsonContent, validateBody } from '../../../middlewares/routes';
// import { validationSchemas } from '../../../request/userSchemaValidation';
// import { passportVerification } from '../../../middlewares/strategies/appJwt';
import { forgotPassword, verifyToken, resetForgotPassword } from "../../../controllers/v1/api/UserController";

const router = expressRouter();

/**
 * Initiate Forgot Password
 * @route POST /forgot-password/{email}
 * @param {string} email.path
 * @group Password
 * @operationId passwordRecoveryStep
 * @produces application/json
 * @consumes application/json
 * @returns {registrationResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 */
 router.route("/forgot-password/:email").post(forgotPassword);
 router.route("/verify-token/:token").get(verifyToken);
 router.route("/update-password").patch(resetForgotPassword);

 export default router;
 