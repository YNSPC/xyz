import expressRouter from 'express-promise-router';
import tagController from "../../../controllers/v1/api/TagController";
import { passportVerification } from '../../../middlewares/strategies/appJwt';
import placeController from "../../../controllers/v1/api/PlaceController";

const router = expressRouter();

/**
 * Retrieve all the places.
 * @route GET /tag
 * @group Tag - Tag
 * @operationId retrieve all tags
 * @param {number} page.query - Optional
 * @param {string} search.query - Optional
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/')
    .get(
        passportVerification,
        tagController.list
    );

/**
 * List place by appropriate tag.
 * @route GET /tag/{tagId}/place
 * @group Tag - Get places
 * @operationId place by tag
 * @param {string} tagId.path - required
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/:tagId/place')
    .get(
        passportVerification,
        placeController.listPlacesByTag
    );

/*cms route*/
router
    .route('/upload-images')
    .patch(
        tagController.uploadImages
    );

export default router;
