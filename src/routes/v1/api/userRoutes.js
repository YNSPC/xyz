import expressRouter from 'express-promise-router';
import {
    blockedUserList,
    blockUser,
    followARandomPerson,
    getUserProfile,
    listYourFollowers,
    listYourFollowings,
    storeFavouriteTags,
    unBlockUser,
    unFollowTheOneWhoYouHate,
    updateProfile,
    userSuggestion,
    userAccountDelete
} from "../../../controllers/v1/api/UserController";
import { requireJsonContent, validateBody } from '../../../middlewares/routes';
import { passportVerification } from '../../../middlewares/strategies/appJwt';
import {validationSchemas} from "../../../request/userSchemaValidation";

const router = expressRouter();

/*-----------user profile------------*/
/**
 * @typedef profileResponseModel
 * @property {string} message:Success
 */

/**
 * Logged user profile
 * @route GET /user/profile
 * @group User - Operations in user
 * @operationId userProfile
 * @produces application/json
 * @consumes application/json
 * @returns { profileResponseModel.model } 200 - Success
 * @security JWT
 */

router
    .route('/profile')
    .get(
        passportVerification,
        getUserProfile
    );

/**
 * @typedef favouriteTagModel
 * @property {Array.<string>} tags.required - tag ids - eg:12345678
 */

/**
 * Record favourite tags of the user for getting recommendation places.
 * @route PATCH /user/favourite-tags
 * @param {favouriteTagModel.model} Tags.body.required - (tags is required)
 * @group User - Operations in user
 * @operationId record all favourite places
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/favourite-tags')
    .patch(
        requireJsonContent,
        validateBody( validationSchemas.favouriteTagSchema, {abortEarly:true} ),
        passportVerification,
        storeFavouriteTags
    );

/**
 * List all my reviews
 * @route GET /user/{userId}/review
 * @group User
 * @operationId List all my review
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/:userId/review')
    .get(
        passportVerification,
        storeFavouriteTags
    );

/**
 * Follow any random user.
 * @route PATCH /user/follow/{userId}
 * @group User - Follow any random user
 * @operationId followUser
 * @param {string} userId.path - required
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/follow/:userId')
    .patch(
        passportVerification,
        followARandomPerson
    );

/**
 * Un follow from followers list.
 * @route DELETE /user/unfollow/{userId}
 * @group User - UnFollow user
 * @operationId unFollowUser
 * @param {string} userId.path - required
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/unfollow/:userId')
    .delete(
        passportVerification,
        unFollowTheOneWhoYouHate
    );

/**
 * Remove user who are following you.
 * @route DELETE /user/remove-follower/{userId}
 * @group User - Remove Follower
 * @operationId removeFollowUser
 * @param {string} userId.path - required
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/remove-follower/:userId')
    .delete(
        passportVerification,
        unFollowTheOneWhoYouHate
    );

/**
 * List users who are following you [ie. your followers].
 * @route GET /user/followers/{userId}
 * @group User - List Follower
 * @operationId listFollowUser
 * @param {string} userId.path
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/followers/:userId?')
    .get(
        passportVerification,
        listYourFollowers
    );

/**
 * List users to whom you are following to [ie. those to whom you follow].
 * @route GET /user/followings/{userId}
 * @group User - List Followings
 * @operationId listFollowingUser
 * @param {string} userId.path
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/followings/:userId?')
    .get(
        passportVerification,
        listYourFollowings
    );

/**
 * @typedef userProfileModel
 * @property {string} firstName firstName - first name - eg:12345678
 * @property {string} lastName lastName - last name - eg:12345678
 * @property {string} gender gender - gender - eg:12345678
 * @property {string} profilePicture profilePicture - profile picture - eg:12345678
 */

/**
 * Update profile.
 * @route PATCH /user/update-profile
 * @param {userProfileModel.model} profile.body.required - (profile is required)
 * @group User - update profile
 * @operationId updateUserProfile
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
    .route('/update-profile')
    .patch(
        passportVerification,
        updateProfile
    );

/**
 * Block user.
 * @route PATCH /user/block/{userId}
 * @param {string} userId.path
 * @group Block - block user
 * @operationId blockUser
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
router
.route('/block/:userId')
.patch(
    passportVerification,
    blockUser
);

/**
 * Blocked user list.
 * @route GET /user/block
 * @group Block - blocked user list
 * @operationId blockedUserList
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
 router
 .route('/block')
 .get(
    passportVerification,
    blockedUserList
 );

/**
 * Un block user.
 * @route PATCH /user/un-block/{userId}
 * @param {string} userId.path
 * @group Block - un block user
 * @operationId unBlockUser
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
 router
 .route('/un-block/:userId')
 .patch(
     passportVerification,
     unBlockUser
 );

 /**
 * User search - suggestion.
 * @route GET /user/search
 * @param {string} search.query
 * @group Chat - user suggestion
 * @operationId userSuggestion
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
  router
  .route('/search')
  .get(
      passportVerification,
      userSuggestion
  );
  
 /**
 * User delete as delete account.
 * @route DELETE /user/delete-account
 * @group USER - delete account
 * @operationId deleteAccount
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */
 router
 .route('/delete-account')
 .delete(
     passportVerification,
     userAccountDelete
 );

export default router;
