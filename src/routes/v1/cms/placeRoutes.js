import expressRouter from "express-promise-router";
import {
  list,
  listForDropdown,
  createPlace,
  updatePlace,
  deletePlace
} from "../../../controllers/v1/cms/PlaceController";
import {passportVerification} from "../../../middlewares/strategies/appJwt";
const router = expressRouter();

/**
 * Retrieve all the places.
 * @route GET /admin/places
 * @group Place - Place
 * @operationId placesListForAdmin
 * @param {number} page.query - Optional
 * @param {string} search.query - Optional
 * @param {number} limit.query - Optional
 * @param {string} sortBy.query - Optional
 * @param {string} sortDesc.query - Optional
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router.route("/").get(list);//passportVerification

/**
 * Places for dropdown.
 * @route GET /admin/places/dropdown
 * @group Place - Place
 * @operationId placesListForAdminDropdown
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router.route("/dropdown").get(listForDropdown);//1 if same method
router.route("/")
  .post(createPlace);
router.route("/:placeId")
  .patch(
    // token validation,
    // request body validation
    updatePlace
  );//2 if same method
  router.route("/:placeId")
  .delete(
    // token validation,
    deletePlace
  );
  
export default router;
