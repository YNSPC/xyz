import expressRouter from 'express-promise-router';
import {
    login,
    refreshAccessToken,
    logout
} from "../../../controllers/v1/cms/AuthController";
import { requireJsonContent, validateBody } from '../../../middlewares/routes';
import { validationSchemas } from '../../../request/userSchemaValidation';
import { passportAdminLoginAuthenticate } from '../../../middlewares/strategies/admin/appAdminLogin';
import { passportAdminAuthenticate } from '../../../middlewares/strategies/admin/appAdmin';

const   router = expressRouter();

/*----------admin-login-------------*/

/**
 * @typedef loginModel
 * @property {string} userName.required - valid userName - eg:s@getnada.com
 * @property {string} password.required - valid password  - eg:11111111
 * @property {integer} deviceType.required - 0-ios/1-android/2-web - eg:1
 * @property {string} deviceId.required - valid device id - eg:123456
 * @property {string} deviceToken.required - valid device token - eg:1234
 */

/**
 * @typedef loginResponseModel
 * @property {string} message:Success
 */

/**
 * Login in the valid user.
 * @route POST /auth/login
 * @param {loginModel.model} User.body.required - (userName, password are required)
 * @group Authentication - Operations in authentication
 * @operationId login
 * @produces application/json
 * @consumes application/json
 * @returns {loginResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 */

router
    .route('/login')
    .post(
        requireJsonContent,
        validateBody(validationSchemas.loginAdminSchema),
        passportAdminLoginAuthenticate,
        login
    );

/*-----------refresh token---------------*/
/**
 * @typedef refreshAccessTokenModel
 * @property {string} refreshToken.required - the token obtained during login.
**/

/**
 * @typedef refreshAccessTokenResponseModel
 * @property {string} message:Success
 * @property {string} data
 */

/**
 * Refresh access token.
 * @route GET /auth/refreshToken
 * @param { refreshAccessTokenModel.model } refreshToken.query.required - Required (refreshToken)
 * @group Authentication - Operations in authentication
 * @operationId refreshAccessToken
 * @produces application/json
 * @consumes application/json
 * @returns {refreshAccessTokenResponseModel.model} 200 - Success
 */

router
    .route('/refreshToken')
    .get( refreshAccessToken );

/**
 *
 * Logout the valid user.
 * @route DELETE /auth/logout/{deviceId}
 * @group Authentication - Logout authenticated user
 * @operationId logout
 * @param {string} deviceId.path.required - deviceId - eg:deviceId
 * @produces application/json
 * @consumes application/json
 * @returns {loginResponseModel.model} 200 - Success
 * @returns {validationResponse.model} 422 - Data validation message
 * @returns {validationResponse.model} 409 - Already exists(Email or userName or contactNumber or access_token)
 * @returns {validationResponse.model} 302 - Already exists(But Not verified)
 * @returns {validationResponse.model} 301 - Link with facebook
 * @returns {validationResponse.model} 403 - Disabled by admin
 * @security JWT
 */
router.route('/logout/:deviceId')
    .delete(
        passportAdminAuthenticate,
        logout
    )

export default router;
