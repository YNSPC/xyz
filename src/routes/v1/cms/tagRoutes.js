import expressRouter from "express-promise-router";
import {
  create,
  list,
  listForDropdown,
  detail,
  updateIcon,
  updateTag,
  deleteTag
} from "../../../controllers/v1/cms/TagController";
const router = expressRouter();

/**
 * @typedef tagModel
 * @property {string} name.required
 * @property {string} icon.required
 * @property {string} color.required
 */

/**
 * Retrieve all the tags.
 * @route GET /admin/tags
 * @group Tag - Tag
 * @operationId tagsListForAdmin
 * @param {number} page.query - Optional
 * @param {string} search.query - Optional
 * @param {number} limit.query - Optional
 * @param {string} sortBy.query - Optional
 * @param {string} sortDesc.query - Optional
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router.route("/").get(list);

/**
 * Retrieve tags for dropdown.
 * @route GET /admin/tags/dropdown
 * @group Tag - Tag
 * @operationId tagsListForAdminDropdown
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router.route("/dropdown").get(listForDropdown);

router.route("/").post(create);

/**
 * Tag detail.
 * @route GET /admin/tags/{tagId}
 * @group Tag - Tag
 * @operationId tagDetailForAdmin
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router.route("/:tagId").get(detail);


/**
 * Update tag without images.
 * @route PATCH /admin/tags/icon/{tagId}
 * @group Tag - Tag
 * @operationId tagIconUpdateByAdmin
 * @param {number} tagId.params
 * @param {tagModel.model} Tag.body.required
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

 router.route("/icon/:tagId").patch(updateIcon);

 router.route("/:tagId").patch(updateTag);

 router.route("/:tagId").delete(deleteTag);

export default router;
