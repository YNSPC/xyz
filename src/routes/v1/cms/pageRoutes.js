import expressRouter from "express-promise-router";
import {
  list,
  create
} from "../../../controllers/v1/cms/PageController";
const router = expressRouter();

router.route("/").get(list);

router.route("/").post(create);

export default router;
