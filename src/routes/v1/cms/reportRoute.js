import expressRouter from "express-promise-router";
import reportController from "../../../controllers/v1/cms/ReportController";
const router = expressRouter();

router
    .route('/')
    .get(
        reportController.listReport
    );

router
    .route('/detail')
    .get(
        reportController.reportDetail
    );

export default router;
