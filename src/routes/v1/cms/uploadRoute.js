import expressRouter from "express-promise-router";
import { uploadMultipleFiles } from "../../../controllers/v1/cms/UploadController";
import uploadController from "../../../controllers/v1/api/UploadController";
import {passportVerification} from "../../../middlewares/strategies/appJwt";
import {validateBody} from "../../../middlewares/routes";
import reportController from "../../../controllers/v1/api/ReportController";
const router = expressRouter();

router
    .route("/")
    .post(uploadMultipleFiles);

router.route("/pre-signed-url").get(uploadController.preSignedUrl);

export default router;
