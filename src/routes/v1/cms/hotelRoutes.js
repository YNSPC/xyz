import expressRouter from "express-promise-router";
import {
  list,
  create
} from "../../../controllers/v1/cms/HotelController";
import {passportVerification} from "../../../middlewares/strategies/appJwt";
const router = expressRouter();

router.route("/").post(create);

/**
 * Retrieve all the tags.
 * @route GET /admin/hotels
 * @group Hotel - adminHotelList
 * @operationId hotelListForAdmin
 * @param {number} page.query - Optional
 * @param {string} search.query - Optional
 * @param {number} limit.query - Optional
 * @param {string} sortBy.query - Optional
 * @param {string} sortDesc.query - Optional
 * @produces application/json
 * @consumes application/json
 * @returns { validationResponse.model } 200 - Success
 * @security JWT
 */

router.route("/").get(list);

export default router;
