import session from "express-session";
import MongoStore from "connect-mongo";

import { dbConfig } from "../../config";
import placeRoutes from "./cms/placeRoutes";
import tagRoutes from "./cms/tagRoutes";
import hotelRoutes from "./cms/hotelRoutes";
import pageRoutes from "./cms/pageRoutes";
import authRoutes from "./cms/authRoutes";
import uploadRoute from "./cms/uploadRoute";
import reportRoute from "./cms/reportRoute";
import passport from "passport";

/**
 * @typedef validationResponse
 * @property {string} message
 */

export default function cmsRoutes(app) {
  app.use(
    session({
      secret: "my ynspc",
      resave: false,
      saveUninitialized: true,
      store: new MongoStore({
        mongoUrl: dbConfig.URI,
        collection: "sessions",
      }),
      cookie: {
        maxAge: 1000 * 60 * 60 * 24,
      },
    })
  );
  app.use(passport.session());

  app.use("/v1/api/admin", authRoutes);
  app.use("/v1/api/admin/places", placeRoutes);
  app.use("/v1/api/admin/tags", tagRoutes);
  app.use("/v1/api/admin/hotels", hotelRoutes);
  app.use("/v1/api/admin/pages", pageRoutes);
  app.use("/v1/api/admin/upload", uploadRoute);
  app.use("/v1/api/admin/reports", reportRoute);
  // app.use("/v1/api/admin/users", hotelRoutes);
}
