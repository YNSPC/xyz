/**
 * @typedef validationResponse
 * @property {string} message
 */

import authRoutes from './api/authRoutes';
import userRoutes from './api/userRoutes';
import pageRoutes from './api/pageRoutes';
import csvRoutes from './api/csvRoutes';
import placeRoutes from './api/placeRoutes';
import exploreRoutes from './api/exploreRoutes';
import tagRoutes from './api/tagRoutes';
import favouriteRoutes from "./api/favouriteRoutes";
import reviewRoutes from "./api/reviewRoutes";
import hotelRoutes from "./api/hotelRoutes";
import postRoutes from "./api/postRoutes";
import uploadRoutes from "./api/uploadRoutes";
import verificationRoutes from "./api/verificationRoutes";
import commentRoutes from "./api/commentRoutes";
import notificationRoutes from "./api/notificationRoutes";
import passwordRoutes from "./api/passwordRoutes";
import eventRoutes from "./api/eventRoutes";
import testRoutes from "./api/testRoutes";
import reportRoutes from "./api/reportRoutes";
import chatRoutes from "./api/chatRoutes";
import settingRoute from "./api/settingRoute";
import eventCategoryRoutes from "./api/eventCategoryRoute";
import searchRoutes from "./api/searchRoute";
// import unspecificRoutes from './api/unspecificRoutes';

export default function apiRoutes(app) {
    app.use('/v1/api/auth', authRoutes);
    app.use('/v1/api/user', userRoutes);
    app.use('/v1/api/page', pageRoutes);
    app.use('/v1/api/csv', csvRoutes);
    app.use('/v1/api/place', placeRoutes);
    app.use('/v1/api/explore', exploreRoutes);
    app.use('/v1/api/tag', tagRoutes);
    app.use('/v1/api/favourite', favouriteRoutes);
    app.use('/v1/api/review', reviewRoutes);
    app.use('/v1/api/hotel', hotelRoutes);
    app.use('/v1/api/post', postRoutes);
    app.use('/v1/api/media', uploadRoutes);
    app.use('/v1/api/verification', verificationRoutes);
    app.use('/v1/api/comment', commentRoutes);
    app.use('/v1/api/event', eventRoutes);
    app.use('/v1/api/test', testRoutes);
    app.use('/v1/api/report', reportRoutes);
    app.use('/v1/api/chat', chatRoutes);
    app.use('/v1/api/setting', settingRoute);
    app.use('/v1/api', passwordRoutes);
    app.use('/v1/api', eventCategoryRoutes);
    // app.use('/v1/api/notification', notificationRoutes);
    notificationRoutes('/v1/api/notification', app);
    app.use('/v1/api/search', searchRoutes);
    // app.use('/v1/api', unspecificRoutes);
};
