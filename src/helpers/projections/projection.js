import { awsConfig } from "../../config"
import { getBaseUrl } from "../common-scripts";
const projections = {
    USER: {
        PROFILE: {
            email: 1,
            userName: 1,
            firstName: 1,
            middleName: 1,
            lastName: 1,
            gender: 1,
            coverPhoto: 1,
            profilePicture: {
                $cond: {
                    if: { 
                        $regexMatch: { input: "$profilePicture", regex: /http/ } 
                    },
                    then: "$profilePicture",
                    else: {
                        $concat: [ getBaseUrl("aws"), "$profilePicture" ]
                    }
                }
            },
            address: 1,
            contactNumber: 1,
            registeredFrom: 1,
            status: 1,
            hasProfileSetupCompleted: 1,
            notificationBadge: 1,
            locale: 1,
            createdAt: 1,
            updatedAt: 1,
            isFirstTimeLogin: 1,
            favouriteTags: 1,
            favouriteTagsObject: 1,
            accountType: 1,
            totalFollowers: {
                $cond: {
                    if: { $isArray: "$followers" },
                    then: { $size: "$followers" },
                    else: 0
                }
            },
            totalFollowings: {
                $cond: {
                    if: { $isArray: "$followings" },
                    then: { $size: "$followings" },
                    else: 0
                }
            },
            followers: 1,
            followings: 1
        },
        PROFILE_PICTURE: {
            profilePicture: {
                $cond: {
                    if: { $regexMatch: { input: "$profilePicture", regex: /http/ } },
                    then: "$profilePicture",
                    else: {
                        $concat: [ getBaseUrl("aws"), "$profilePicture" ]
                    }
                }
            },
        }
    },
    TAG: {
        ICON: {
            icon: {
                $cond: {
                    if: { $regexMatch: { input: "$icon", regex: /http/ } },
                    then: "$icon",
                    else: {
                        $cond: {
                            if: { 
                                $gt: [
                                    { $strLenCP: "$icon" },
                                    1
                                ] 
                            },
                            then: {
                                $concat: [ getBaseUrl("aws"), "$icon" ]
                            },
                            else: ''
                        }
                    }
                }
            },
        }
    }
};

export {
    projections
};
