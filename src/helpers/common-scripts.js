/*
* Creator: Yathartha Shrestha
* About: Common functions to be used throughout the project where needed.
*
* Included: bCrypt library helper functions
*
* */
import mongoose from 'mongoose';
import dotEnv from 'dotenv';
dotEnv.config();
import { genSalt, hash, compare } from 'bcrypt';
import moment from "moment";
import { googleConfig, environment, awsConfig, systemConfig } from "../config";

export const HashPassword = async (plainText) => {
    try {
        const salt = await genSalt(10, '');
        return await hash(plainText, salt);
    }
    catch (error) {
        console.log("common-scripts error encountered-", error);
        throw new Error(error.message);
        // return error;
    }
};

export const CompareHashWith = async ( plainText, ourStoredHash ) => {
    try {

        return await compare(plainText, ourStoredHash);
    }
    catch (error) {
        console.log("common-scripts error encountered-", error);
        throw new Error(error.message);
    }
};

export const MakeDomainLowerCaseOfEmail = email => {
    if( !email )
        return email;

    try {
        email = email.split('@');
        email[1] = email[1].toLowerCase();
        return email.join('@');
    } catch (e) {
        return email;
    }
};

export const isValidObjectId = objectId => {
    return mongoose.Types.ObjectId.isValid(objectId)
};

export const objectId = stringId => {
    /*this is used in case of aggregation*/
    return mongoose.Types.ObjectId(stringId);
};

export const getValidPage = async page => {
    page = parseInt(page);
    if (isNaN(page) || typeof page === 'undefined')
        return 1;

    return page <= 0 ? 1 : page
};

export const paginateProvider = async (dataObj) => {
    let pageData;
    let records = dataObj.docs;
    if (records.length > 0) {

        let pagination = {
            totalRecords: dataObj.totalDocs,
            limit: dataObj.limit,
            hasPrevPage: dataObj.hasPrevPage,
            hasNextPage: dataObj.hasNextPage,
            currentPage: dataObj.page,
            totalPage: dataObj.totalPages,
            pagingCounter: dataObj.pagingCounter,
            prevPage: dataObj.prevPage,
            nextPage: dataObj.nextPage
        };
        pageData = {
            data: records,
            pagination: pagination
        };

        return pageData;


    }
    else {
        pageData = {
            data: records,
            pagination: {
                totalRecords: 0,
                limit: 0,
                hasPrevPage: false,
                hasNextPage: false,
                currentPage: 1,
                totalPage: 1,
                pagingCounter: 1,
                prevPage: null,
                nextPage: null
            }
        }
    }
    return pageData;
};

export const requestFilter = (requestBody) => {
    for ( const attribute in requestBody ) {
        console.log(attribute)
    }
};

export const isValidPlaceRow = place => {
    return (
        place.country !== '' &&
        place.name !== '' &&
        place.description !== '' &&
        place.popularName !== '' &&
        place.latitude !== '' &&
        place.longitude !== '' &&
        // place.displayImage !== '' &&
        place.placeType !== '' &&
        place.state !== '' &&
        place.zone !== '' &&
        place.district !== '' &&
        place.myUniqueCode !== '' &&
        place.tags !== '' &&
        place.famousRating !== ''
    );
};

export const fullURL = (folder, filename) => {
    const scheme = process.env.UPLOAD_IMAGE ? "https" : "http";
    const url = process.env.UPLOAD_IMAGE ? process.env.UPLOAD_SERVER_URL : process.env.SERVER_URL;
    return `${scheme}://${url}/${folder}/sharp/${filename.split(".")[0]}.webp`;
}

export const getCurrentUnixTimeStamp = () => moment.utc().unix();

export const upperCaseFirstLetter = word => word.trim().charAt(0).toUpperCase() + word.slice(1).trim();

export const decodeHTMLEntities = (text) => {
    const entities = [
        ["amp", "&"],
        ["apos", "'"],
        ["#x27", "'"],
        ["#x2F", "/"],
        ["#39", "'"],
        ["#47", "/"],
        ["lt", "<"],
        ["gt", ">"],
        ["nbsp", " "],
        ["quot", '"'],
    ];

    for (let i = 0, max = entities.length; i < max; ++i)
        text = text.replace(new RegExp(`&${entities[i][0]};`, "g"), entities[i][1]);

    return text;
};

export const undefinedCase = (payload, fields, def) => {
    const result = fields.reduce((o, k) => {
        return o && typeof o === 'object' && k in o ? o[k] : false
    }, payload)

    return result;
};

export const deepCopy = jsonPayload => JSON.parse(JSON.stringify(jsonPayload));
export const shallowCopy = jsonPayload => ({...jsonPayload});

export const dbUsage = async() => {
    console.log(mongoose);
};

// just for google url
const getBucketName = (service="google") => {
    switch (service) {
        case "google":
            return environment === 'live' 
            ? googleConfig.bucketLive.name
            : googleConfig.bucket.name

        case "aws":
            return environment === 'live' 
            ? awsConfig.s3BucketLive.name
            : awsConfig.s3Bucket.name
    
        default:
            break;
    }
};

export const getBaseUrl = (service="google") => {
    switch (systemConfig.mediaService) {
        case "google":
            return `${googleConfig.bucket.publicUrl}${getBucketName()}/`

        case "aws":
            return `${awsConfig.s3Bucket.urlPrefix}`
    
        default:
            break;
    }
};
