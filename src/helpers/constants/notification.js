export const notifications = {
  follow: {
    title: "#user has followed you.",
    message: ""
  },
  like: {
    title: "#user has liked your post.",
    message: "#title"
  },
  happyNewYear: {
    title: `Happy New Year 2079 #user. 
    FROM: yatribhet team`,
    message: "#title"
  },
  comment: {
    title: "#user has commented on your post.",
    message: "#title"
  },
  chatMessage: {
    title: "#title",
    message: "#message"
  },
  newEventCreated: {
    title: "New travel event",
    message: `#event  -#organizer`
  },
  eventInvitation: {
    title: "New travel #title",
    message: "#message"
  },
  eventRespond: {
    title: "New travel #title",
    message: "#message"
  },
  joinEvent: {
    title: "Joined the event",
    message: "#member has joined to #event"
  }
};
