import { serverConfig } from '../../config/index';
const enums = {
    USER_STATUS: {
        INACTIVE: 0,
        ACTIVE: 1,
        DISABLED: 2,
    },
    USER_ROLES: {
        ADMIN: 1,
        SHOPKEEPER: 2,
        WAITER: 3
    },
    BALANCE_TYPE: {
        CREDIT: 'credit',
        DEBIT: 'debit'
    },
    DEVICE: {
        android: 1,
        ios: 2,
        web: 3
    },
    REGISTERING_DEVICES: {
        FACEBOOK: 'facebook',
        GOOGLE: 'google',
        APP: 'normal'
    },
    PARTIAL_ROUTE_FOR_URL_GENERATION: {
        EMAIL_VERIFICATION: `${serverConfig.URL}/vi/api/user/verify-email`
    },
    GENDER: {
        MALE: 'male',
        FEMALE: 'female',
        OTHER: 'other',
    },
    PAGE_SLUG: {
        PRIVACY_POLICY: "privacy-policy",
        TERMS_AND_CONDITION: "terms-condition",
        ABOUT_US: "about-us",
    },
    UPLOAD_IMAGE_TYPE: {
        DISPLAY_IMAGE: 1,
        IMAGES: 2
    },
    ACCOUNT_TYPE: {
        PRIVATE: "private",
        PUBLIC: "public"
    },
    VISIBILITY: {
        PRIVATE: "private",
        PUBLIC: "public"
    },
    POST_TYPE: {
        TRIP: "trip",
        IMAGE_VIDEO: "iv"
    },
    MEDIA_TYPE: {
        IMAGE: "image",
        VIDEO: "video"
    }
};

const system = {
  LOGGER: {
    DAILY: "daily",
    SINGLE: "single",
  },
  VERIFICATION: {
    ACCOUNT: "account",
    CONTACT: "contact",
    FORGOT_PASSWORD: "forgot_password",
  },
  TOKEN: {
    ACCESS_TOKEN: "access_token",
    REFRESH_TOKEN: "refresh_token",
    PASSWORD_RESET_TOKEN: "password_reset_token",
    FORGOT_PASSWORD_TOKEN: {
      TEMPLATE: "forgot_password",
      SUBJECT: "Forgot password",
      ACTION: "forgot_password_token",
    },
    VERIFICATION_TOKEN: {
      TEMPLATE: "email_verification",
      SUBJECT: "Email verification",
      ACTION: "verification_token",
    },
  },
};

export {
    enums,
    system
};
