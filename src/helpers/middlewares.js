import AdminPermissionModel from "../database/models/AdminPermission";

export const checkPermission = async (request, response, next) => {
    try {
        if (!request.module || !request.action) return next(_accessDenied());
        
        const loggedUser = request.user;
        const moduleDetail = await AdminPermissionModel.findById(loggedUser.role);
        const selectedModuleData = moduleDetail.permits.find(permission => permission.module === request.module);

        if ( !selectedModuleData ) return next(_accessDenied());
        return selectedModuleData.actions.includes(request.action) 
            ? next() 
            : next(_accessDenied());
    }
    catch(error) {
        return next(_accessDenied());
    }
};

export const setModuleAction = (module, action) => {
    return (request, response, next) => {
        request.module = module;
        request.action = action;

        return next();
    };
};

const _accessDenied = () => {
    const error = new Error("Access Denied.");
    error.code = error.status = 403;
    error.title = "Access Denied";

    return error;
}