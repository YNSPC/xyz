import { commonUserWithFcm } from "../../repositories/userRepository";
import {notifications} from "../constants/notification";
import {isValidObjectId, upperCaseFirstLetter} from "../common-scripts";
import {parseMessage, sendMessage} from "../../modules/googleFcm";

export const sendNotification = async () => {
    const allAppUsers = await commonUserWithFcm();

    for (let user of allAppUsers) {
        const notificationTemplate = notifications.happyNewYear;
        const mapObject = {
            "#user": `${upperCaseFirstLetter(user.firstName)} ${upperCaseFirstLetter(user.lastName)}`
        };
        const title = parseMessage(notificationTemplate.title, mapObject);
        const message = "";// const message = parseMessage(notificationTemplate.message, mapObject);//currently showing only title
        const notificationData = {};
        const notificationPayload = {
            title,
            data: notificationData,
        };

        if ( user.fcmTokens.length > 0 ) {
            try {
                const fcmResponse = await sendMessage({
                    tokens: user.fcmTokens,
                    badge: 1,
                    title,
                    message,
                    notificationData
                });
                console.log("fcm response", fcmResponse);
            }
            catch (exception) {
                console.log("error sending notification", exception);
            }
        }        
    }
}
