import UserDevices from '../../database/models/UserDevices'

export const updateOrCreateUserDevices = async ( userId, deviceId, deviceType, deviceToken, accessToken, refreshToken, expiry ) => {
    const deviceData = await UserDevices.findOneAndUpdate(
        {
            deviceId: deviceId,
        },
        {
            $set: {
                user: userId,
                deviceType: deviceType,
                deviceToken: deviceToken,
                accessToken: accessToken,
                refreshToken: refreshToken,
                expiredAt: expiry
            }
        },
        {
            new: true,
            upsert: true,
            useFindAndModify: false
        }
    );

    return deviceData;
};

export const logoutDevices = async filterOption => {
    return UserDevices.findOneAndDelete(filterOption);
};


export const updateUserDevices = async (filter, update) => {
    return await UserDevices.findOneAndUpdate(
        filter,
        update,
        {
            new: true,
            upsert: true,
            useFindAndModify: false
        }
    );
};
