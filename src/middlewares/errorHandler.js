import _ from "lodash";
import requestIp from "request-ip";

const requestTracker = function (request, response, next) {
    console.log("================REQUEST CALLED=========================");
    console.log("Ip: ", requestIp.getClientIp(request) );
    console.log("End point: ", request.originalUrl );
    // console.log("base URL: ", request.baseUrl );
    console.log("http Verb: ", request.method );
    console.log("path: ", request.path );
    console.log("headers: ", request.headers );
    console.log("request data[BODY]: ", request.body );
    console.log("request data[QUERY]: ", request.query );
    console.log("request data[PARAMS]: ", request.params );
    // console.log("socket Events: ", request._events );
    console.log("=======================================================");
    next();
};

const pageNotFound = function (request, response, next) {
    let error = new Error("Page Not Found.");
    error.status = 404;
    next(error);
};

const errorHandling = function (error, request, response, next) {
    console.log("error: ", error.message );
    const verboseError = true;
    if ( verboseError ) {
        console.log("verbose: ", error);
        console.log("==============================================Error End===========================================================");
    }
    if (request.xhr && request.accepts("json")) {
        return response
            .json({
                message: error.message || "Internal Server Error.",
                data: {},
            });
    }
    else {
        const list = [..._.range(400, 499, 1), ..._.range(500, 599, 1)];
        const errorStatus =
            list.includes(error.status) || list.includes(error.code)
                ? error.status || error.code
                : 500;
        return response
            .status(errorStatus)
            .json({
                message: error.message || "Internal Server Error.",
                errorProvider: error.name || "Unknown.",
                data: {},
            });
    }
};

module.exports = {
    pageNotFound,
    errorHandling,
    requestTracker
};
