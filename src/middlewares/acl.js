import AdminPermissionModel from "../database/models/AdminPermission";

export const setModuleAction = (module, action) => {
    return (req, res, next) => {
        req.module = module;
        req.action = action;

        return next();
    }
};

//TODO: need to re fetch the new set of permission for removing the menu list.
const accessDenied = () => {
    const Err = new Error("Access Denied");
    Err.code = Err.status = 403;
    Err.title = "Access Denied.";
    return Err;
};

//TODO: model related code need to be in the repository level
export const checkPermission = async(request, response, next) => {
    const loggedUser = request.user;
    const moduleDetail = await AdminPermissionModel.findOne( { role: loggedUser.role } );
    const selectedModuleData = moduleDetail.permits.find(permission => permission.module === request.module);
    if (selectedModuleData === undefined) return next(accessDenied())

    return selectedModuleData.actions.includes(request.action) ? next() : next(accessDenied());
};
