// import { fileDetection } from "../../helpers/uploadHelper";
const SIZE_THRESHOLD = 5;
const SIZE_THRESHOLD_IN_BYTES = SIZE_THRESHOLD * 1024 * 1024;

export const fileSizeLimiter = (request, response, next) => {
    if ( request.file ) {};

    if ( request.files ) {
        const files = request.files;
        const filesOverLimit = [];

        Object.keys(files).forEach(key => {
            if ( files[key].size > SIZE_THRESHOLD_IN_BYTES ) {
                filesOverLimit.push(files[key].name);
            }
        });

        if ( filesOverLimit.length ) {
            const message = `Upload failed! ${fileErrorMessage(filesOverLimit)} over the file size limit of ${SIZE_THRESHOLD} MB.`

            return response
                .status(413)
                .json({
                    status: "error",
                    message
                })
        }

        next();
    }
};
