export const fileExistsInPayload = (request, response, next) => {
    if ( !request.files || !request.file ) {
        return response
            .status(400)
            .json({
                status: "error",
                message: "Missing files."
            });
    }

    next();
};
