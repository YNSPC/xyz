import Passport from "passport";
import userModel from "../../database/models/Users";
import { LOGIN } from "../../helpers/constants/server-messages";
import { oauthConfig } from "../../config/index";
import { OAuth2Client } from "google-auth-library";
import passportCustom from "passport-custom";
const CustomStrategy = passportCustom.Strategy;

Passport.use(
  "google",
  new CustomStrategy(async function (req, done) {
    try {
      let Err;
      const client = new OAuth2Client({
        clientId: oauthConfig.GOOGLE.APP_ID,
      });

      const { accessToken } = req.body;
      const response = await client.verifyIdToken({ idToken: accessToken });
      const googlePayload = response.getPayload();
      //   const aa = {
      //     iss: "accounts.google.com",
      //     azp: "394829292852-36aa4cbkhfat2eiho8p42kmbjjgtuj6j.apps.googleusercontent.com",
      //     aud: "394829292852-36aa4cbkhfat2eiho8p42kmbjjgtuj6j.apps.googleusercontent.com",
      //     sub: "107354793519632892618",
      //     email: "yshrestha007@gmail.com",
      //     email_verified: true,
      //     at_hash: "DYr6xUitOqXa9YpJtf97MQ",
      //     name: "Yathartha Shrestha",
      //     picture:
      //       "https://lh3.googleusercontent.com/a-/AOh14GgwOBdzlKGiukJM6rjxuogc3cYisFVAH_79iw89TQ=s96-c",
      //     given_name: "Yathartha",
      //     family_name: "Shrestha",
      //     locale: "en",
      //     iat: 1631114779,
      //     exp: 1631118379,
      //     jti: "1e86d3a213ed332f57c43029c7d46792769c221f",
      //   };
      const googleUser = await userModel.findOne({
        googleAccessToken: googlePayload.sub,
      });

      if (!googleUser) {
        if (googlePayload.hasOwnProperty("email")) {
          if (
            typeof googlePayload.email === "string" &&
            googlePayload.email.length > 0
          ) {
            let localUser = await userModel.findOne({
              email: googlePayload.email,
            });
          }
        }
        // redirect to registration
        Err = new Error(
          "User is not registered in our system. Signup this new user and try again."
        );
        Err.code = Err.status = 441;
        return done(Err);
      }

      if (googleUser.status === 2) {
        let Err = new Error(
          "This user is disabled. Contact the administration."
        );
        Err.code = Err.status = 401;
        return done(Err);
      }

      done(null, googleUser);
    } catch (error) {
      console.log("exception caught while signin with google.", error);
      done(error);
    }
  })
);

Passport.use(
  "googleLinkAndSingIn",
  new CustomStrategy(async (request, done) => {
    try {
      const client = new OAuth2Client({
        clientId: oauthConfig.GOOGLE.APP_ID,
      });

      const { accessToken } = request.body;
      const response = await client.verifyIdToken({ idToken: accessToken });
      const googlePayload = response.getPayload();

      done(null, googlePayload);
    } catch (error) {
      console.log("exception caught while signin with google.", error);
      done(error);
    }
  })
);

export const passportGoogleAuthLogin = async (req, res, next) => {
  await Passport.authenticate(
    "google",
    { session: false },
    function (error, user, info) {
      try {
        if (error) return next(error);

        if (info && info.hasOwnProperty("message")) {
          let error = new Error(info.message);
          error.code = 400;
          return next(error);
        }

        if (!user) {
          let error = new Error(LOGIN.INVALID_CREDENTIALS.message);
          error.code = error.status = LOGIN.INVALID_CREDENTIALS.httpCode;

          return next(error);
        }

        req.user = user;
        return next();
      } catch (exception) {
        console.log("caught", exception);
        next(exception);
      }
    }
  )(req, res, next);
};

export const passportLinkWithGoogle = async (request, response, next) => {
  await Passport.authenticate(
    "googleLinkAndSingIn",
    { session: false },
    function (error, user, info) {
      try {
        if (error) return next(error);

        if (info && info.hasOwnProperty("message")) {
          let error = new Error(info.message);
          error.code = 400;
          return next(error);
        }

        if (!user) {
          let error = new Error(LOGIN.INVALID_CREDENTIALS.message);
          error.code = error.status = LOGIN.INVALID_CREDENTIALS.httpCode;

          return next(error);
        }

        request.user = user;
        return next();
      } catch (exception) {
        console.log("caught", exception);
        next(exception);
      }
    }
  )(request, response, next);
};
