import Passport from "passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { tokenConfig } from "../../../config";
import adminModel from "../../../database/models/Admin";
// import { LOGIN } from '../../helpers/constants/server-messages';

Passport.use('app-admin', new Strategy(
    {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken,
        secretOrKey: tokenConfig.secretAccessToken,
        passReqToCallback: true
    },
    async (request, jwtPayload, done) => {
        try {
            let accessToken = request.headers.Authorization || '';
            const user = await adminModel.findById(jwtPayload.sub);
            return done(null, user);
        } 
        catch (error) {
            return done(error, false);
        }
    }
));

export const passportAdminAuthenticate = async (request, response, next) => {
    await Passport.authenticate(
        'app-admin',
        { session: false },
        function (error, user, info) {
            if (error) return next(error);

            if( info )
            {
                if (info && info.name === 'Error') {
                    let Err = new Error('Token not found.');
                    Err.code = Err.status = 401;
                    return next(Err);
                }
                if (info && info.name === 'TokenExpiredError') {
                    let Err = new Error(`Token has expired @ ${info.expiredAt}`);
                    Err.code = Err.status = 401;
                    return next(Err);
                }
                if (info && info.name === 'JsonWebTokenError') {
                    let Err = new Error(info.message);
                    Err.code = Err.status = 401;
                    return next(Err);
                }
            }

            if ( !user ) {
                let Err = new Error('User not found.');
                Err.code = Err.status = 401;
                return next(Err);
            }
            request.user = user;

            next();
        }
    )(request, response, next);
};
