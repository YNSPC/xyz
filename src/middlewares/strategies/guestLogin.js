import {passportVerification} from "./appJwt";

export const headerToVerifyGuestLogin = (request, response, next) => {
    const headers = request.headers;
    if (headers.guestlogin === 'true') return next();

    return passportVerification(request, response, next);
};

export const headerToVerifyWithAppKeyId = (request, response, next) => {
    const headers = request.headers;
    if (headers.appkeyid === 'eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ') 
        return next();

    let error = new Error('Unauthorized.');
    error.code = error.status = 401;
    return next(error);
};
