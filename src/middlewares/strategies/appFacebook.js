import Passport from "passport";
import FacebookTokenStrategy from "passport-facebook-token";
import userModel from "../../database/models/Users";
import { LOGIN } from "../../helpers/constants/server-messages";
import { oauthConfig } from "../../config/index";

Passport.use(
  "facebookSignup",
  new FacebookTokenStrategy(
    {
      clientID: oauthConfig.FACEBOOK.APP_ID,
      clientSecret: oauthConfig.FACEBOOK.APP_SECRET,
      enableProof: true,
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        const facebookProfile = profile._json || {};

        return done(null, facebookProfile);
      } catch (error) {
        console.log("exception caught while signin with facebook.", error);
        return done(error);
      }
    }
  )
);

Passport.use(
  "facebookLogin",
  new FacebookTokenStrategy(
    {
      clientID: oauthConfig.FACEBOOK.APP_ID,
      clientSecret: oauthConfig.FACEBOOK.APP_SECRET,
      enableProof: true,
      passReqToCallback: true,
    },
    async (request, accessToken, refreshToken, profile, done) => {
      try {
        request.body.access_token = accessToken;
        let Err;
        const facebookProfile = profile._json || {};
        // console.log("passport profile", profile);

        /*check if user has already logged in with facebook before*/
        const facebookUser = await userModel.findOne({
          facebookAccessToken: facebookProfile.id,
        });
        // console.log("passport", facebookUser);

        if (!facebookUser) {
          if (facebookProfile.hasOwnProperty("email")) {
            if (
              typeof facebookProfile.email === "string" &&
              facebookProfile.email.length > 0
            ) {
              let localUser = await userModel.findOne({
                email: facebookProfile.email,
              });

              // if (localUser && parseInt(localUser.status) === 2) {
              //     Err = new Error(LOGIN.DISABLED_BY_ADMIN.message);
              //     Err.code = Err.status = LOGIN.DISABLED_BY_ADMIN.httpCode;
              //     return done(Err);
              // } else if (localUser) {
              //     Err = new Error(LOGIN.FB_UNLINKED_EMAIL_EXISTS.message);
              //     Err.code = Err.status = LOGIN.FB_UNLINKED_EMAIL_EXISTS.httpCode;
              //     return done(Err);
              // } else {
              //     //
              // }
            }
          }
          // redirect to registration
          Err = new Error(
            "User is not registered in our system. Signup this new user and try again."
          );
          Err.code = Err.status = 441;
          return done(Err);
        }

        if (facebookUser.status === 2) {
          let Err = new Error(
            "This user is disabled. Contact the administration."
          );
          Err.code = Err.status = 401;
          return done(Err);
        }

        done(null, facebookUser);
      } catch (error) {
        console.log("exception caught while signin with facebook.", error);
        done(error);
      }
    }
  )
);

Passport.use(
  "facebookLinkAndSingIn",
  new FacebookTokenStrategy(
    {
      clientID: oauthConfig.FACEBOOK.APP_ID,
      clientSecret: oauthConfig.FACEBOOK.APP_SECRET,
      enableProof: true,
      passReqToCallback: true,
      profileFields: ['id', 'displayName', 'name', 'emails', 'picture.width(400)']
    },
    async (request, accessToken, refreshToken, profile, done) => {
      try {
        request.body.access_token = accessToken;
        let Err;
        const facebookProfile = profile._json || {};

        //check if fb does not provide information

        done(null, facebookProfile);
      } catch (error) {
        console.log("exception caught while signin with facebook.", error);
        done(error);
      }
    }
  )
);

export const passportFacebookSignup = async (request, response, next) => {
  request.body.access_token = request.body.facebookAccessToken;

  Passport.authenticate("facebookSignup", function (error, user, info) {
    try {
      if (error) {
        console.log("debug", error);
        if (error.oauthError) {
          const code = error.oauthError.statusCode;
          const oauthError = JSON.parse(error.oauthError.data);
          let err = new Error(oauthError.error.message);
          err.status = code;
          return next(err);
        }
        return next(error);
      } else {
        request.facebookObject = user;
        return next();
      }
    } catch (exception) {
      console.log("caught", exception);
      return next(exception);
    }
  })(request, response, next);
};

export const passportFacebookLogin = async (request, response, next) => {
  await Passport.authenticate(
    "facebookLogin",
    { session: false },
    function (error, user, info) {
      try {
        // console.log("error", error);
        // console.log("user", user);
        // console.log("info", info);
        if (error) return next(error);

        if (info && info.hasOwnProperty("message")) {
          let error = new Error(info.message);
          error.code = 400;
          return next(error);
        }

        if (!user) {
          let error = new Error(LOGIN.INVALID_CREDENTIALS.message);
          error.code = error.status = LOGIN.INVALID_CREDENTIALS.httpCode;

          return next(error);
        }

        request.user = user;
        return next();
      } catch (exception) {
        console.log("caught", exception);
        next(exception);
      }
    }
  )(request, response, next);
};

export const passportLinkWithFacebook = async (request, response, next) => {
  await Passport.authenticate(
    "facebookLinkAndSingIn",
    { session: false },
    function (error, user, info) {
      try {
        if (error) return next(error);

        if (info && info.hasOwnProperty("message")) {
          let error = new Error(info.message);
          error.code = 400;
          return next(error);
        }

        if (!user) {
          let error = new Error(LOGIN.INVALID_CREDENTIALS.message);
          error.code = error.status = LOGIN.INVALID_CREDENTIALS.httpCode;

          return next(error);
        }

        request.user = user;
        return next();
      } catch (exception) {
        console.log("caught", exception);
        next(exception);
      }
    }
  )(request, response, next);
};
