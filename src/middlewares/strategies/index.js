import { passportAppAuthenticate } from './appLocal';
import { passportVerification } from './appJwt';
import { passportFacebookLogin } from './appFacebook';
import { passportAdminAuthenticate } from './admin/appAdmin';
import { passportAdminLoginAuthenticate } from './admin/appAdminLogin';
import { headerToVerifyGuestLogin, headerToVerifyWithAppKeyId } from './guestLogin';

export default {
    passportAppAuthenticate,
    passportFacebookLogin,
    passportVerification,
    passportAdminAuthenticate,
    passportAdminLoginAuthenticate,
    headerToVerifyGuestLogin,
    headerToVerifyWithAppKeyId
};
