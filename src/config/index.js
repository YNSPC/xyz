import dotEnv from "dotenv";
dotEnv.config();

const deployment = process.env.DEPLOYMENT || "development:local";
const deploymentSplit = deployment.split(":");
const environment = process.env.NODE_ENV;
console.log({environment});
let uri = "";

switch (deploymentSplit[0]) {
  case "development":
    if (deploymentSplit[1] === "local")
      uri = `${process.env.DB_DRIVER}://${
        process.env.DB_USER_NAME
      }:${encodeURIComponent(process.env.DB_PASSWORD)}@${process.env.DB_HOST}:${
        process.env.DB_PORT
      }/${process.env.DB_NAME}`;
    if (deploymentSplit[1] === "mongodb")
      uri = `${process.env.DB_DRIVER}+srv://${
        process.env.DB_NAME
      }:${encodeURIComponent(process.env.DB_PASSWORD)}@${process.env.DB_HOST}/${
        process.env.DB_NAME
      }?retryWrites=true&w=majority&ssl=true`;
    if (deploymentSplit[1] === "mlab")
      uri = `${process.env.DB_DRIVER}://${
        process.env.DB_NAME
      }:${encodeURIComponent(process.env.DB_PASSWORD)}@${process.env.DB_HOST}:${
        process.env.DB_PORT
      }/${process.env.DB_NAME}`;
    break;

  case "production":
    if (deploymentSplit[1] === "local") uri = "";
    if (deploymentSplit[1] === "mongodb") uri = "";
    if (deploymentSplit[1] === "mlab") uri = "";
    break;
}

const serverConfig = {
  PORT: environment !== 'live' ? Number(process.env.SERVER_PORT) : 5005,
  URL: process.env.SERVER_URL || "localhost",
};

const dbConfig = {
  URI: environment !== 'live'
    ? uri
    : `${process.env.DB_DRIVER_LIVE}://${process.env.DB_HOST_LIVE}:${process.env.DB_PORT_LIVE}/${process.env.DB_NAME_LIVE}`,
  OPTIONS: {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  },
};

const swaggerConfig = {
  DISABLE: process.env.DISABLE_SWAGGER || false,
  INFO: {
    description: process.env.SWG_DESCRIPTION || "Give the description",
    title: process.env.SWG_TITLE || "Give the title",
    version: "1.0.0",
  },
  HOST: process.env.SERVER_URL || "localhost:5000",
  BASE_PATH: "/v1/api",
  PRODUCES: ["application/json", "application/xml"],
  SCHEMES: ["http", "https"],
  SECURITY: {
    JWT: {
      type: "apiKey",
      in: "header",
      name: "Authorization",
      description: "",
    },
  },
};

const oauthConfig = {
  FACEBOOK: {
    APP_ID: process.env.FB_APP_ID,
    APP_SECRET: process.env.FB_APP_SECRET,
    REDIRECT_URL: process.env.FB_REDIRECT_URL,
  },
  GOOGLE: {
    APP_ID: process.env.G_ID,
    APP_SECRET: process.env.G_SECRET_KEY,
    REDIRECT_URL: process.env.FB_REDIRECT_URL,
  },
};

const systemConfig = {
  facebookLink: process.env.FACEBOOK_LINK,
  systemName: process.env.SYSTEM_NAME,
  systemContactNumber: process.env.SYSTEM_CONTACT_NUMBER,
  systemEmail: process.env.SYSTEM_EMAIL || "no-reply@yatribhet.com",
  appBaseUrl: process.env.APP_BASE_URL || "localhost:3000",
  adminBaseUrl: process.env.ADMIN_BASE_URL || "localhost:4000",
  apiBaseUrl: process.env.SERVER_URL || "localhost:5000",
  isLoggable: process.env.LOGGABLE === 'yes',
  protocol: process.env.SERVER_PROTOCOL === 'secure' ? 'https' : "http",
  mediaService: process.env.MEDIA_SERVICE,
};

const emailConfig = {
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  authUserName: process.env.MAIL_USER,
  authPassword: process.env.MAIL_PASSWORD,
  from: process.env.MAIL_FROM,
};

const tokenConfig = {
  expiryAccessToken: process.env.ACCESS_TOKEN_EXPIRY_IN_SECONDS,
  expiryRefreshToken: process.env.REFRESH_TOKEN_EXPIRY_IN_SECONDS,
  expiryPasswordResetToken: process.env.PASSWORD_RESET_EXPIRY_IN_SECONDS,
  expiryForgotPasswordToken: process.env.FORGOT_PASSWORD_EXPIRY_IN_SECONDS,
  expiryAccountVerificationToken:
    process.env.ACCOUNT_VERIFICATION_EXPIRY_IN_SECONDS,

  secretAccessToken: process.env.SECRET_ACCESS_TOKEN,
  secretRefreshToken: process.env.SECRET_REFRESH_TOKEN,
  secretPasswordResetToken: process.env.SECRET_PASSWORD_RESET_TOKEN,
  secretForgotPasswordToken: process.env.SECRET_FORGOT_PASSWORD_TOKEN,
  secretAccountVerificationToken: process.env.SECRET_ACCOUNT_VERIFICATION_TOKEN,
};

const parserConfig = {
  csv: {
    separator: {
      separator: ",",
    },
    deleteFile: true,
  },
};

const uploadConfig = {
  csv: {
    allowedExtension: {
      extensions: [".csv"],
      message: "Only .csv format allowed!", //from-server-message
      httpCode: 400, //from-server-message
    },
  },

  image: {
    allowedExtension: {
      extensions: [".jpg", ".png", ".jpeg", ".gif"],
      message: "Only image file extensions allowed!", //from-server-message
      httpCode: 422, //from-server-message
    },
  },
};

const awsConfig = {
  s3Bucket: {
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
    urlPrefix: process.env.S3_URL_PREFIX,
    name: process.env.S3_BUCKET,
    version: process.env.S3_VERSION,
    region: process.env.S3_REGION,
  },
  s3BucketLive: {
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
    urlPrefix: process.env.S3_URL_PREFIX,
    name: process.env.S3_BUCKET,
    version: process.env.S3_VERSION,
    region: process.env.S3_REGION,
  }
};

const firebaseConfig = {
  dynamicLink: {
    dynamicLinkBaseUrl: process.env.DYNAMIC_LINK_BASE_URL,
    androidPackageName: process.env.ANDROID_PACKAGE_NAME,
    iosBundleId: process.env.IOS_BUNDLE_ID,
    firebaseApiKey: process.env.FIREBASE_API_KEY,
    shorteningUrl: process.env.FIREBASE_SHORTENING_URL,
    protocol: "https"
  }
};

const googleConfig = {
  bucket: {
    publicUrl: process.env.GOOGLE_CLOUD_PUBLIC_URL,
    projectId: process.env.GOOGLE_CLOUD_PROJECT_ID,
    keyFile: process.env.GOOGLE_CLOUD_KEY_FILE,
    name: process.env.GOOGLE_BUCKET
  },
  bucketLive: {
    publicUrl: process.env.GOOGLE_CLOUD_PUBLIC_URL,
    projectId: process.env.GOOGLE_CLOUD_PROJECT_ID,
    keyFile: process.env.GOOGLE_CLOUD_KEY_FILE,
    name: process.env.GOOGLE_BUCKET_LIVE
  }
};

export {
  firebaseConfig,
  swaggerConfig,
  systemConfig,
  parserConfig,
  uploadConfig,
  serverConfig,
  tokenConfig,
  oauthConfig,
  emailConfig,
  dbConfig,
  awsConfig,
  environment,
  googleConfig
};
