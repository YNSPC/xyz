"use strict";

import expressHBS from "express-handlebars";
import Express from "express";
import bodyParser from "body-parser";
import passport from "passport";
import cors from "cors";
import path from "path";
import * as Sentry from "@sentry/node";
import { Server as mySocketServer } from 'socket.io';

import { errorHandling, pageNotFound, requestTracker } from "./middlewares/errorHandler";
import { swaggerConfig, serverConfig } from "./config";
import { logging } from "./services/loggableService";
import apiRoutes from "./routes/v1/apiRoutes";
import cmsRoutes from "./routes/v1/cmsRoutes";
import Logger from "./utils/Logger";
import Database from "./database";
import { getMyFollowingList } from "./repositories/userRepository";

const app = new Express();
const logger = new Logger();
global.bp = process.cwd();
global.TEMP_FOLDER = `${process.cwd()}/storage/system/temp`;
global.SYSTEM_FOLDER = `${process.cwd()}/storage/system`;
global.MY_DEBUGGER = (payload) => logging(payload);
global.MY_LOGGER = (payload) => logger[payload.action](payload.data);
/*absolute path / relative path*/
import http from 'http';
const httpServer = http.Server(app);

Sentry.init({
  dsn: 'https://ced1025fa873490cbd203967af7e669d@o834079.ingest.sentry.io/5812994',
  debug: false,
  environment: process.env.NODE_ENV,
  // sampleRate: 1.0, //0.0 to 1.0 in percentage
  serverName: 'yatribhet-server',
  //beforeSend: () => {},//hook
  // integrations: [
  //     // enable HTTP calls tracing
  //     new Sentry.Integrations.Http({ tracing: true }),
  //     // enable Express.js middleware tracing
  //     new Tracing.Integrations.Express({
  //       // to trace all requests to the default router
  //       app,
  //       // alternatively, you can specify the routes you want to trace:
  //       // router: someRouter,
  //     }),
  // ],
  tracesSampleRate: 0.1,
});

app.use(Sentry.Handlers.requestHandler());

new Database().connect();

if(!!+swaggerConfig.DISABLE)  app.use('/api-docs', pageNotFound);
const expressSwagger = require("express-swagger-generator")(app);
let options = {
  swaggerDefinition: {
    info: swaggerConfig.INFO,
    host: swaggerConfig.HOST,
    basePath: swaggerConfig.BASE_PATH,
    produces: swaggerConfig.PRODUCES,
    schemes: swaggerConfig.SCHEMES,
    securityDefinitions: swaggerConfig.SECURITY,
  },
  basedir: __dirname, //app absolute path
  files: ["./routes/**/*.js"], //Path to the API handle folder
};
expressSwagger(options);

/*activating middleware*/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(passport.initialize());
// app.use(Express.static(path.join(__dirname, "../storage/system")));
app.use(Express.static(path.join(__dirname, "../views")));
/*view and view engine*/

const hbs = expressHBS.create({
    extname: 'hbs',
    defaultLayout: 'page',
    layoutsDir: __dirname + './../views'
    // helpers: require('./helpers/handlebars')
});
app.engine('hbs', hbs.engine);
app.set("views", path.join(__dirname, "./../views"));
app.set('view engine', 'hbs');

app.use(requestTracker);
/*routes enabled*/
apiRoutes(app);
cmsRoutes(app);

/*to be removed*/
import { sendMessage } from "./modules/googleFcm";
import { sendNotification } from "./helpers/commons/allNotification";
import user from "./database/models/Users";
  app.get("/fcm-test", (req, res, next) => {
    // forgotPasswordService("s3@getnada.com")
    //   .then(suc => {
    //     console.log("done");
    //   })
    //   .catch(err => {
    //     console.log(err);
    //   })
    // return res.status(200).json({
    //   data: uniqueCodeGen(6, {type:"alpha-numeric"})});
    sendMessage({
      tokens: [
        "eGMdOJJFSAyTwd1osghdht:APA91bEPcvQKuQqZ25JmnIfmNCeff-Oziwi9QqHoSS_s0RrkpNyhlFSKOMQM55CeM-rqSsdf4NJIfFtJHHMh8HuvZqw8evUXVfgPRcd_UOTGnlIJIyZh35Dnn51lF0Xv2NXWIWpM0PiX",
        "fA0Dux9BQa2J21f1OQPk-9:APA91bG7Vy7mj_yZI4L1YMQPXc4DRglVi_9PEF_sU1z93q6k8_KuxwW4TUz4Z50JTkh0e99nfXhuHXEZxQK4jBhfjLdAz-5vVxhX0ce9kI5oeij7EJ7g-cRlq4DNvdGL7cci_C7EerN0"
      ]      
    })
    // .then(fcmResult => {
    //   return res.status(200)
    //       .json({
    //         message: "test fcm",
    //         data: fcmResult
    //       })
    // })
    // .catch(fcmError => {
    //   return res.status(500)
    //       .json({
    //         message: "Error sending test fcm",
    //         data: fcmError.message
    //       })
    // });
    try {
      // sendNotification().then().catch();
    }catch(e) {
      console.log("error", e);
    }
});

/*end to be removed*/
app.get("/chat" , (req , res)=>{
  res.render("page");
});

app.use(Sentry.Handlers.errorHandler());
app.use(pageNotFound);
app.use(errorHandling);

//socket related task
// const io = new mySocketServer(httpServer, {
//   // path: "",
//   cors: {
//     origin: ["https://stage-admin.yatribhet.com", "http://stage-admin.yatribhet.com", "http://localhost:3000"],
//     methods: ["GET", "POST"],
//     allowedHeaders: ["header-X-header"]
//   }
// });
// io.use((socket, next) => {
//   // MIDDLEWARE-CHECK-HERE
//   const authHandshake = socket.handshake.auth;
//   const authHandshakeQuery = socket.handshake.query;
//   console.log({
//     type: "handshake",
//     data: socket.handshake.query
//   });

//   if (authHandshakeQuery.uuid && authHandshakeQuery.fullName) {
//     socket.uuid = authHandshakeQuery.uuid;
//     socket.fullName = authHandshakeQuery.fullName;
//     console.log("here inside");
//     next();
//   }
//   else {
//     console.log("Error");
//     socket.broadcast.emit("__CONNECTION_ERROR_CASE__", "unable to connect!!!")
//   }
// });

// let CHAT_SYS_LIST = {};
// // const ex = {
// //   "uuid": {
// //     "name": "",
// //     "id": "uuid",
// //     ...others,
// //     sids: [],
// //   }
// // }

// function uniqueUser(uuid) {
//   return CHAT_SYS_LIST.hasOwnProperty(uuid);
// };

// function createNewSet(uuid) {
//   CHAT_SYS_LIST[uuid] = new Set();
// }

// function addSocketToSet(socket) {
//   CHAT_SYS_LIST[socket.uuid].add(socket.id);
// }

// function removeSocketFromSet(socket) {
//   console.log("soc", socket.id)
//   CHAT_SYS_LIST[socket.uuid].delete(socket.id);
// }

// async function sendOfflineStateToMyCircle(socket) {
//   const list = await getMyFollowingList(socket.uuid);
//   if (!list) {
//     return [];
//   }

//   return list.followings ? list.followings : [];
// }

// const sendOnlineStateToMyCircle = async (socket) => {
//   const list = await getMyFollowingList(socket.uuid);
//   if (!list) {
//     return [];
//   }

//   return list.followings ? list.followings : [];
// }

// io.on("connection", async function (socket) {
//   console.log("socket connect", { sid: socket.id });

//   function sendMessageToRecipients(payload) {
//     const recipient = payload.receiver;
//     const sender = socket.uuid;
//     const recipientList = CHAT_SYS_LIST[recipient] !== undefined ? CHAT_SYS_LIST[recipient] : undefined;
//     const senderList = CHAT_SYS_LIST[sender] !== undefined ? CHAT_SYS_LIST[sender] : undefined;

//     console.log({
//       type: "message received to broadcast",
//       recipient,
//       sender,
//       recipientList,
//       senderList
//     });
    
//     if (recipientList) {
//       // socket.broadcast.emit("__BROADCAST_SENT_DIRECT_MESSAGE__", payload);
//       recipientList.forEach(recipient => {
//         socket
//           .broadcast
//           .to(recipient)
//           .emit("__BROADCAST_TO_RECIPIENTS__", {
//             message: payload.message,
//             sender: socket.uuid,
//             receiver: payload.receiver
//           })
//       })
//     }
    
//     if (senderList) {
//       // socket.broadcast.emit("__BROADCAST_SENT_DIRECT_MESSAGE__", payload);
//       senderList.forEach(sender => {
//         console.log({sender, sid: socket.id});
//         if (sender !== socket.id) {
//           socket
//             .broadcast
//             .to(sender)
//             .emit("__BROADCAST_TO_OTHER_SENDERS__", {
//               message: payload.message,
//               sender: socket.uuid,
//               receiver: payload.receiver
//             }) 
//         }
//       })
//     }
//   }

//   if (!uniqueUser(socket.uuid)) {
//     createNewSet(socket.uuid);
//     addSocketToSet(socket);
//   }
//   else {
//     addSocketToSet(socket);
//   }

//   console.log(CHAT_SYS_LIST);
//   let users;
//   let offlineUsers;

//   try {
//     users = await sendOnlineStateToMyCircle(socket);
//     if (users.length > 0) {
//       for (const user of users) {
//         let recipients = CHAT_SYS_LIST[user.userId] != undefined ? CHAT_SYS_LIST[user.userId] : undefined;
//         console.log({recipients, j: user.userId});
//         if (recipients) {
//           recipients.forEach(recipient => {
//             socket
//               .broadcast
//               .to(recipient)
//               .emit("__ONLINE_STATE__", {
//                 friend: socket.uuid
//               })
//           })
//         }
//       }
//     }
//   }
//   catch(e) {
//     console.log({e});
//   }

//   socket.on("__RECEIVE_DIRECT_MESSAGE__", function (payload) {
//     console.log(`DM received`, payload);
//   });

//   socket.on("__SEND_DIRECT_MESSAGE__", function (payload) {
//     console.log(`SEND received`, payload);
//     sendMessageToRecipients(payload);
//   });

//   socket.on("voice", function (data) {
//     var newData = data.split(";");
//     newData[0] = "data:audio/ogg;";
//     newData = newData[0] + newData[1];

//     for (const id in socketsStatus) {
//       if (id != socketId && !socketsStatus[id].mute && socketsStatus[id].online) {
//         socket.broadcast.to(id).emit("send", newData);
//       }
//     }
//   });

//   socket.on("userInformation", function (data) { 
//     console.log("test", data)
//     socketsStatus[socketId] = data;

//     io.sockets.emit("usersUpdate",socketsStatus);
//   });

//   socket.on("disconnect", async function () {
//     console.log("some one got disconnected", {
//       list: CHAT_SYS_LIST[user.userId],
//       user,
//       sid: socket.id
//     });
//     try {
//       users = await sendOfflineStateToMyCircle(socket);
//       if (users.length > 0) {
//         for (const user of users) {
//           let recipients = CHAT_SYS_LIST[user.userId] != undefined ? CHAT_SYS_LIST[user.userId] : undefined;
//           console.log({recipients, j: user.userId});
//           if (recipients) {
//             recipients.forEach(recipient => {
//               socket
//                 .broadcast
//                 .to(recipient)
//                 .emit("__OFFLINE_STATE__", {
//                   friend: socket.uuid
//                 })
//             })
//           }
//         }
//       }
//     }
//     catch(e) {
//       console.log({e});
//     }
//     removeSocketFromSet(socket);
//   });

// });


import chatSocket from "./socket";
import { uniqueCodeGen } from "./utils/lodashUtil";
import { forgotPasswordService } from "./services/userService";
chatSocket(httpServer);

httpServer.listen(serverConfig.PORT, () => {
  MY_DEBUGGER({ data: `Server has started on port ${serverConfig.PORT}`, action: "info" })
});
