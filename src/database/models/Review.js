import mongoose from "mongoose";
import Double from "@mongoosejs/double";

const reviewSchema = new mongoose.Schema(
  {
    userId: { type: "objectId", ref: "User", required: true },
    placeId: { type: "objectId", ref: "Place", required: true },
    rating: { type: Double, required: true },
    review: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

const reviewModel = mongoose.model("Review", reviewSchema);

export default reviewModel;
