import mongoose from 'mongoose';

const { Schema } = mongoose;
const SettingSchema = new Schema({
    androidVersion: {
        type: String,
        trim: true,
        required: true
    },
    iosVersion: {
        type: String,
        trim: true,
        required: true
    },
    androidForceUpdate: {
        type: Boolean,
        default: true
    },
    iosForceUpdate: {
        type: Boolean,
        default: true
    },

}, { timestamps: true });

const SettingModel = mongoose.model('Setting', SettingSchema);

export default SettingModel;
