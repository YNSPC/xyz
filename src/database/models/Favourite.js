import mongoose from 'mongoose';

const favouriteSchema = new mongoose.Schema(
    {
        userId: { type: "objectId", ref: "User" },
        placeId: { type: "objectId", ref: "Place" }
    },
    {
        timestamps: true
    }
);

const favouriteModel = mongoose.model('Favourite', favouriteSchema);

export default favouriteModel;
