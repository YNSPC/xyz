import mongoose from 'mongoose';
const schema = mongoose.Schema;

const moduleSchema = new schema({
        name: { type: String, required: true },
        description: { type: String },
        actions: [ { type: String } ]
    },
    {
        timestamps: true
    });

const moduleModel = mongoose.model('Module', moduleSchema);

export default moduleModel;
