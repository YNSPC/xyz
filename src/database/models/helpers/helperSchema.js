import mongoose from "mongoose";

export const locationSchema = new mongoose.Schema({
  address: { type: String },
  location: {
    type: {
      type: String,
      trim: true,
      default: "Point",
    },
    coordinates: { type: [Number, Number], index: "2dsphere" }
  }
});
