import mongoose from 'mongoose';

const { Schema } = mongoose;
const TokenSchema = new Schema({
    token: {
        type: String,
        trim: true,
        required: true,
        unique: true,
    },
    tokenHolder: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    tokenHolderType: {
        type: String,
        trim: true,
        enum: ["admin", "application"],
        required: true,
        default: "application"
    },
    tokenType: {
        type: String,
        enum: [ "verification_token", "forgot_password_token" ],// get this from constants
        trim: true,
        required: true
    },
    expiresAt: {
        type: Date,
        trim: true,
        required: true,
    },

}, { timestamps: true });

//TODO: tokenHolder and tokenType are unique combined. create index

const TokenModel = mongoose.model('Token', TokenSchema);

export default TokenModel;
