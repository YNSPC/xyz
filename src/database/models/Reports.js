import mongoose from "mongoose";

const reportSchema = new mongoose.Schema(
  {
    reportedBy: { type: "objectId", ref: "User", required: true },
    reportedOn: { type: String, required: true, enum: ['post', 'profile', 'comment'] },
    identity: { type: "objectId", required: true },
    comment: { type: String, required: true },
    state: { type: String, enum: ['pending', 'review', 'verified', 'ignored'], default: "pending" },
    silentUser: { type: Boolean, default: false },
    silentNotification: { type: Boolean, default: false },
  },
  {
    timestamps: true,
  }
);

const reportModel = mongoose.model("Report", reportSchema);

export default reportModel;
