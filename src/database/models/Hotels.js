import mongoose from 'mongoose';
import Double from '@mongoosejs/double';
import mongoosePaginate from 'mongoose-paginate-v2';
import mongooseAggregatePaginate from 'mongoose-aggregate-paginate-v2';
import placeModel from '../models/Places';

const facilitySchema = new mongoose.Schema(
    {
        name: { type: String, required: false },
        icon: { type: String, required: false },
        popular: { type: Boolean, default: false },
    }
);

const roomSchema = new mongoose.Schema(
    {
        roomType: { type: String, required: false },
        description: { type: String, required: false },
        displayImage: { type: String, trim: true, required: false },
        images: { type: [String], required: false },
        facilities: { type: [ facilitySchema ], default: [] },
    }
);

const hotelSchema = new mongoose.Schema(
    {
        placeId: { type: mongoose.Types.ObjectId, ref: placeModel, required: true },
        hospitalityType: { type: String, trim: true, enum: ['hotel', 'restaurant', 'cafe', 'resort'], required: true },
        location: { type: String, trim: true, required: true },//search field for itinerary
        address: { type: String, trim: true, required: true },//search field for itinerary
        name: { type: String, trim: true, required: true },
        rating: { type: Double, required: true },
        displayImage: { type: String, trim: true, required: true },
        images: { type: [String], required: true },
        information: { type: String, trim: true, required: true },
        facilities: { type: [ facilitySchema ], default: [] },
        rooms: { type: [ roomSchema ], default: [] },
        // startAmount: { type: Double, required: true },
        isInContact: { type: Boolean, default: true }
    },
    {
        timestamps: true
    }
);

hotelSchema.plugin(mongoosePaginate);
hotelSchema.plugin(mongooseAggregatePaginate);

const hotelModel = mongoose.model('Hotel', hotelSchema);

export default hotelModel;
