import mongoose from 'mongoose';
import userModel from "./Users";
import privateChatModel from "./PrivateChat";

const chatUserSchema = new mongoose.Schema(
  {
    user: { type: mongoose.Types.ObjectId, ref: userModel, required: true },
    chatUser: { type: mongoose.Types.ObjectId, ref: userModel, required: true },
    role: { type: String, required: true, enum: ["self", "friend"] },
    message: { type: mongoose.Types.ObjectId, ref: privateChatModel, required: false },
  },
  {
    timestamps: true
  }
);
const chatUserModel = mongoose.model('ChatUser', chatUserSchema);

export default chatUserModel;
