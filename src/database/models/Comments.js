import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
import mongooseAggregatePaginate from 'mongoose-aggregate-paginate-v2';
import userModel from "./Users";
import postModel from "./Posts";

const commentSchema = new mongoose.Schema(
    {
        post: { type: mongoose.Types.ObjectId, ref: postModel, required: true },
        commentedBy: { type: mongoose.Types.ObjectId, ref: userModel, required: true },
        replyTo:{ type: mongoose.Types.ObjectId, ref: 'comments', default: null },
        replyToUser:{ type: mongoose.Types.ObjectId, ref: userModel, required: false },
        comment: [
            {
                data: { type: String, required: true },
                dataType: { type: String, enums: ["text", "image"], required: true }
            }
        ],
        editedAt: { type: Date, default: null },
        likedBy: {
            type: [ { type: mongoose.Types.ObjectId, ref: userModel } ],
            default: []
        },
    },
    {
        timestamps: true
    }
);

commentSchema.plugin(mongoosePaginate);
commentSchema.plugin(mongooseAggregatePaginate);

// commentSchema.virtual('totalComments', {
//     ref: postModel, // The model to use
//     localField: 'post', // Find people where `localField`
//     foreignField: '_id', // is equal to `foreignField`
//     count: true // And only get the number of docs
// });

const commentModel = mongoose.model('Comment', commentSchema);

export default commentModel;
