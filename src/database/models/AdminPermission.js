import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
import aggregatePaginate from 'mongoose-aggregate-paginate-v2';
const schema = mongoose.Schema;

const PermissionSchema = new schema({
    module: { type: String },
    actions: [ { type: String } ]
});

const AdminPermissionSchema = new schema({
        role: { type: String, enum: ["super-admin", "admin", "hotel-owner"] },
        description: { type: String },
        permits: [ { type: PermissionSchema } ]
    },
    {
        timestamps: true
    });

AdminPermissionSchema.plugin(mongoosePaginate);
AdminPermissionSchema.plugin(aggregatePaginate);

const AdminPermissionModel = mongoose.model('AdminPermission', AdminPermissionSchema);

export default AdminPermissionModel;
