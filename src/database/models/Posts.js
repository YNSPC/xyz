import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
import mongooseAggregatePaginate from 'mongoose-aggregate-paginate-v2';
import { enumerations } from "../../helpers/enumerations";
import { enums } from "../../helpers/constants/database";
import { locationSchema } from "./helpers/helperSchema";
import userModel from "./Users";

const postSchema = new mongoose.Schema(
    {
        /*TODO: update the work*/
        postType: { type: String, enum: enumerations.PostType, required: true },
        location: { type: String, required: false,  },
        title: { type: String },
        description: { type: String },
        visibility: { type: String, enum: enumerations.Visibility, default: enums.VISIBILITY.PUBLIC },
        status: { type: Boolean, default: false },
        editedAt: { type: Number, default: null },
        totalShares: { type: Number, default: 0 },
        totalComments: { type: Number, default: 0 },
        totalLikes: { type: Number, default: 0 },
        sharedBy: {
            type: [ { type: mongoose.Types.ObjectId, ref: 'users' } ],
            default: []
        },
        likedBy: {
            type: [ { type: mongoose.Types.ObjectId, ref: 'users' } ],
            default: []
        },
        hiddenBy: {
            type: [ { type: mongoose.Types.ObjectId, ref: 'users' } ],
            default: []
        },
        reportedBy: {
            type: [ { type: mongoose.Types.ObjectId, ref: 'users' } ],
            default: []
        },
        tags: {
            type: [ { type: mongoose.Types.ObjectId, ref: 'tags' } ],
            default: []
        },
        author: { type: mongoose.Types.ObjectId, ref: userModel },
        media: {
            type: [
                {
                    mediaType: { type: String, enum: enumerations.Media_Type, required: true },
                    mediaUrl: {
                        smallUrl: { type: String, required: true },
                        mediumUrl: { type: String, required: true },
                        largeUrl: { type: String, required: true },
                        originalUrl: { type: String, required: true },
                    }
                }
            ],
            required: true,
        },
        // location: { type: locationSchema, required: false },
        // tripStart: { type: Date, default: null },
        tripCoverPhoto: {
            type: {
                mediaType: { type: String, enum: enumerations.Media_Type, required: false },
                mediaUrl: {
                    smallUrl: { type: String, required: false },
                    mediumUrl: { type: String, required: false },
                    largeUrl: { type: String, required: false },
                    originalUrl: { type: String, required: false }
                }
            }
        },
        tripInfo: {
            type: [
                {
                    currentDay: { type: String, required: false },
                    description: { type: String, required: false },
                    location: { type: locationSchema, required: false },
                    media: {
                        type: [
                            {
                                mediaType: { type: String, enum: ["image"], default: "image" },
                                mediaUrl: {
                                    smallUrl: { type: String, required: false },
                                    mediumUrl: { type: String, required: false },
                                    largeUrl: { type: String, required: false },
                                    originalUrl: { type: String, required: false }
                                }
                            }
                        ],
                        default: []
                    },
                    tags: {
                        type: [ { type: mongoose.Types.ObjectId, ref: 'tags' } ],
                        default: []
                    },
                }
            ],
            default: []
        },
    },
    {
        timestamps: true
    }
);

postSchema.plugin(mongoosePaginate);
postSchema.plugin(mongooseAggregatePaginate);

const postModel = mongoose.model('Post', postSchema);

export default postModel;
