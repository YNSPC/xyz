import mongoose from 'mongoose';
import userModel from "./Users";

const privateChatSchema = new mongoose.Schema(
  {
    messageId: { type: String, required: true },
    messageType: { type: String, required: true },
    sender: { type: mongoose.Types.ObjectId, ref: userModel, required: true },
    receiver: { type: mongoose.Types.ObjectId, ref: userModel, required: true },
    voice: { type: String, required: false },
    images: { type: [String], required: false },
    videos: { type: [String], required: false },
    message: { type: String, required: false },
    hideFromSender: { type: Boolean, default: false },
    hideFromReceiver: { type: Boolean, default: false },
    hasRead: { type: Boolean, default: false },
    badge: { type: Number, default: 0 },
    deletedUsers: { type: [mongoose.Types.ObjectId], ref: userModel, default: [] },
  },
  {
    timestamps: true
  }
);
const privateChatModel = mongoose.model('PrivateChat', privateChatSchema);

export default privateChatModel;
