import mongoose from "mongoose";
import mongoosePaginate from "mongoose-paginate-v2";
import aggregatePaginate from "mongoose-aggregate-paginate-v2";
import userModel from "./Users";

const { Schema } = mongoose;

const notificationSchema = new Schema({
    sender: {
        type: mongoose.Schema.Types.ObjectId,
        ref: userModel
    },
    receiver: {
        type: mongoose.Schema.Types.ObjectId,
        ref: userModel
    },
    relatedTo: {
        type: String,
        trim: true,
        enum: ["App", "Admin"],
        default: "App"
    },
    title: {
        type: String,
        trim: true,
        index: true,
        required: false,
    },
    data: {
        type: mongoose.Schema.Types.Mixed,
        trim: true,
        required: false,
    },
    hasRead: {
        type: Boolean,
        default: false,
    },

    metaData: {
        type: mongoose.Schema.Types.Mixed,
        required: false
    },
    sentCount: {
        type: Number,
        default: 0
    },
    notificationType: {
        type: String,
        trim: true,
        required: true,
        enum: ["follow", "like", "comment", "message", "newEvent", "joinEvent", "respondEvent", "inviteEvent"]
    },
    segment: {
        type: String,
        trim: true,
        default: "other",
    },
},
{ timestamps: true }
);

notificationSchema.plugin(mongoosePaginate);
notificationSchema.plugin(aggregatePaginate);

const notificationModel = mongoose.model("Notification", notificationSchema);

export default notificationModel;
