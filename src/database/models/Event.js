import mongoose from 'mongoose';
import aggregatePaginate from 'mongoose-aggregate-paginate-v2';
import { locationSchema } from './helpers/helperSchema';
const schema = mongoose.Schema;
  
const EventSchema = new schema(
    {
        name: { type: String, required: true },
        totalTravellers: { type: Number, required: true },
        date: { type: Date, required: true },
        picture: { type: String, required: true },
        duration: { type: Number, required: true },
        description: { type: String, required: false },
        from: { type: locationSchema, required: true },
        to: { type: locationSchema, required: true },
        organizer: { type: mongoose.Types.ObjectId, ref: 'users', index: true, required: true },
        status: { type: String, required: true },
        categories: {
            type: [ { type: mongoose.Types.ObjectId, ref: 'eventcategories' } ],
            default: []
        },
        // location: { type: String, required: true },
        private: { type: Boolean, default: false },
        members: [
            {
                userId: { 
                    type: mongoose.Types.ObjectId, 
                    ref: "users", 
                    index: true, 
                    required: false 
                },
                invitedOn: { type: Date, required: false },
                joinedOn: { type: Date, required: false },
                updatedAt: { type: Date, required: false },
                status: { type: String, required: false },
            }
        ]
    },
    {
        timestamps: true
    });

EventSchema.plugin(aggregatePaginate);

const EventModel = mongoose.model('Event', EventSchema);

export default EventModel;
