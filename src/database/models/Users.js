import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
import mongooseAggregatePaginate from 'mongoose-aggregate-paginate-v2';
import { enumerations } from '../../helpers/enumerations';
import {
    HashPassword,
    CompareHashWith,
    MakeDomainLowerCaseOfEmail
} from '../../helpers/common-scripts';
import {
    REGISTER
} from '../../helpers/constants/server-messages';
import { enums } from '../../helpers/constants/database';
import { projections } from '../../helpers/projections/projection';
import { awsConfig } from '../../config';

const userSchema = new mongoose.Schema(
    {
        email: { type: String, required: false, unique: true },
        /*email: {
            type: String,
            default: null,
            validate: {
                validator: function (email) {
                    return this.registeredFrom !== enums.REGISTERING_DEVICES.APP
                },
                message: () => `Email is a required field`
            },
            comment: "Making email a required field only for app registration. Do check for uniqueness later if needed."
        },*/
        // userName: { type: String, required: false, unique: true },
        //strictly available only in the case of the normal signup
        password: { type: String, required: false, select: false },
        /*password: {
            type: String,
            default: null,
            validate: {
                validator: function (password) {
                    return this.registeredFrom !== enums.REGISTERING_DEVICES.APP
                },
                message: () => `Password is a required field`
            },
            comment: "Making email a required field only for app registration. Do check for uniqueness later if needed."
        },*/
        firstName: { type: String, required: true },
        middleName: { type: String, default: null },
        lastName: { type: String, default: null },
        gender: { type: String, enum: enumerations.Gender, required: true },
        dateOfBirth: { type: Date, required: false },

        coverPhoto: { type: String, default: null },
        profilePicture: {
            type: String,
            default: null,
            /*get: (value) => {
                if (!value) return null;
                else if ( /http/.test(value) ) return value;
                return `${awsConfig.s3Bucket.urlPrefix}${value}`
            },
            transform: (value) => value,*/
        },
        address: { type: String, default: null },//paid service
        contactNumber: { type: String, default: null },

        registeredFrom: { type: String, enum: enumerations.RegisteredFrom, default: enums.REGISTERING_DEVICES.APP },
        status: { type: Number, enum: enumerations.UserStatus, default: enums.USER_STATUS.INACTIVE },
        hasProfileSetupCompleted: { type: Boolean, default: false },

        emailVerifiedAt: { type: Date, default: null },
        //this needs to be in separate model
        emailTokenUuid: { type: String, default: null },
        emailVerificationExpiry: { type: Date, default: null },
        //end of this model

        notificationBadge: { type: Number, default: 0 },
        facebookAccessToken: { type: String, default: null },
        googleAccessToken: { type: String, default: null },

        termsAndConditionAcceptedAt: { type: Date, default: null },
        privacyPolicyAcceptedAt: { type: Date, default: null },

        locale: { type: String, default: 'en' },
        favouriteTags: { type: [ { type: 'ObjectId', ref: 'tags' } ], default: [] },
        isFirstTimeLogin:{ type: Boolean, default: true },

        accountType: { type: String, enum: enumerations.AccountType, default: enums.ACCOUNT_TYPE.PUBLIC },
        followers: {
            type: [
                {
                    userId: {type: 'ObjectId', ref: 'users'},
                    followedAt: { type: String },
                    status: { type: String, enums: [ "accepted", "requested", "blocked" ], default: "requested" }
                }],
            default: []
        },
        followings: {
            type: [
                {
                    userId: {type: 'ObjectId', ref: 'users'},
                    followedAt: { type: String },
                    status: { type: String, enums: [ "accepted", "requested", "blocked" ], default: "requested" }
                }],
            default: []
        },
        blockedTo: [
            { 
                type: mongoose.Schema.Types.ObjectId, 
                ref: 'users', 
                default: [] 
            }
        ],
        chatWith: [
            { 
                type: mongoose.Schema.Types.ObjectId, 
                ref: 'users', 
                default: [] 
            }
        ]
    },
    {
        timestamps: true,
        /*toObject: {
            getters: true,
            transform: (doc, ret, options) => {
                doc.profilePicture = "test"
                ret.profilePicture = "test"
            }
        }*/
    }
);

userSchema.plugin(mongoosePaginate);
userSchema.plugin(mongooseAggregatePaginate);

userSchema.pre('save', async function (next) {
    this.email = MakeDomainLowerCaseOfEmail(this.email)
    const isAlreadyExists = await this.model('User').findOne({ email: this.email });
    if( isAlreadyExists ) {
        let error = new Error(REGISTER.EMAIL_EXISTS.message);
        error.status = REGISTER.EMAIL_EXISTS.httpCode;
        return next(error);
    }

    if ( this.registeredFrom === enums.REGISTERING_DEVICES.APP )
        this.password = await HashPassword(this.password);

    next();
});

userSchema.statics.didPasswordMatch = async function(password, hashedPassword) {
    return await CompareHashWith(password, hashedPassword);
};

userSchema.statics.details = async function(userId) {
    let stages = [];
    stages.push({
        $match: {
            _id: userId
        }
    });

    stages.push({
        $lookup: {
            from: 'tags',
            let: { 'favouriteTags': '$favouriteTags' },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $in: ["$_id", "$$favouriteTags"]
                        }
                    }
                }
            ],
            as: "favouriteTagsObject"
        }
    });

    stages.push({
      $lookup: {
        from: 'devices',
        let: { userId: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ["$$userId", "$user"]
              }
            }
          }
        ],
        as: "devicesObject"
      }
    });

    stages.push({
        $project: {
          ...projections.USER.PROFILE,
          fcmTokens: {
            $map:{
              input: "$devicesObject",
              as: "loggedDevice",
              in: "$$loggedDevice.deviceToken"
            }
          }
        }
    });
    let userInformation = await this.aggregate(stages);
    userInformation = userInformation.length === 1 ? userInformation.pop() : {};

    return userInformation;
};

const user = mongoose.model('User', userSchema);

export default user;
