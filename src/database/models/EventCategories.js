import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
import mongooseAggregatePaginate from 'mongoose-aggregate-paginate-v2';

const eventCategorySchema = new mongoose.Schema(
    {
        name: { $type: String, required: true, unique: true },
        // icon: { type: String },
        // color: { type: String },
    },
    {
        timestamps: true,
        typeKey: "$type"
    }
);

eventCategorySchema.plugin(mongoosePaginate);
eventCategorySchema.plugin(mongooseAggregatePaginate);

eventCategorySchema.statics.getCategories = function(options) {
    let stages = [];
    let filter;
    options = options || {};

    filter = {
        $match: {}
    }

    if (options.search) {
        filter.$match.name = {
            $regex: options.search,
            $options: 'i'
        };
    }

    stages.push(filter);

    stages.push({
        $project: {
            name: 1,
        }
    })

    return this.aggregate(stages);
};

const eventCategoryModel = mongoose.model('EventCategory', eventCategorySchema);

export default eventCategoryModel;
