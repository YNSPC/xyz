import mongoose from "mongoose";
import mongoosePaginate from "mongoose-paginate-v2";
import aggregatePaginate from "mongoose-aggregate-paginate-v2";
import Double from "@mongoosejs/double";

export const locationSchema = new mongoose.Schema({
  type: { type: String },
  coordinates: { type: [Number, Number], default: [] },
});

const subRouteSchema = new mongoose.Schema({
  name: { type: String },
  description: { type: String },
  starting: { type: String },
  ending: { type: String },
  position: { type: String },
  myCode: { type: String },
});

const routeSchema = new mongoose.Schema({
  name: { type: String },
  subRoutes: [{ type: subRouteSchema, default: [] }],
  myRouteUniqueCode: { type: String },
});

const placeSchema = new mongoose.Schema(
  {
    country: { type: String, required: true },
    name: { type: String, required: true, unique: true },
    description: { type: String, required: true },
    popularName: { type: String },
    location: { type: locationSchema, required: true },
    displayImage: { type: String, default: null },
    displayMap: { type: String, default: null },
    images: { type: Array, default: null },
    placeType: { type: String, required: true },
    state: { type: String, required: true },
    zone: { type: String, required: true },
    district: { type: String, required: true },
    myUniqueCode: { type: String, required: true },
    parentCode: { type: String, default: null },
    tags: { type: [{ type: "ObjectId", ref: "tags" }], required: true },
    famousRating: { type: Double, default: 1 },
    parent: { type: "ObjectId", ref: "places", default: null },
    routes: [{ type: routeSchema, default: [] }],
  },
  {
    timestamps: true,
  }
);

placeSchema.plugin(mongoosePaginate);
placeSchema.plugin(aggregatePaginate);

placeSchema.statics.getPlaceBy = async function (searchObject) {
  return this.findOne(searchObject);
};

placeSchema.statics.getPlaces = function (options) {
  let stages = [];
  let filter;
  options = options || {};

  filter = {
    $match: {},
  };

  if (options.search) {
    filter.$match.name = {
      $regex: options.search,
      $options: "i",
    };
  }

  stages.push(filter);

  stages.push({
    $sort: {
      famousRating: -1,
    },
  });

  stages.push({
    $lookup: {
      from: "tags",
      let: { tags: "$tags" },
      pipeline: [
        {
          $match: {
            $expr: {
              $in: ["$_id", "$$tags"],
            },
          },
        },
        {
          $project: {
            name: 1,
          },
        },
      ],
      as: "tags",
    },
  });

  stages.push({
    $lookup: {
      from: "favourites",
      let: { placeId: "$_id" },
      pipeline: [
        {
          $match: {
            $expr: {
              $and: [
                {
                  $eq: ["$placeId", "$$placeId"],
                },
                {
                  $eq: ["$userId", options.logger],
                },
              ],
            },
          },
        },
      ],
      as: "hasFavourite",
    },
  });

  /*3. sorting stage*/
  // if (typeof options.sortBy === 'string' && options.sortBy.length > 0) {
  //         stages.push({
  //                 $sort: {
  //                         [options.sortBy]: options.sortDesc ? -1 : 1
  //                 }
  //         });
  // }

  // stages.push({
  //         $sort: {
  //                 famousRating: -1
  //         }
  // });

  stages.push({
    $project: {
      myUniqueCode: 1,
      displayImage: 1,
      images: 1,
      tags: 1,
      parent: 1,
      country: 1,
      name: 1,
      description: 1,
      popularName: 1,
      placeType: 1,
      state: 1,
      zone: 1,
      district: 1,
      famousRating: 1,
      location: 1,
      isFavourite: {
        $cond: {
          if: {
            $isArray: "$hasFavourite",
          },
          then: {
            $cond: {
              if: {
                $eq: [{ $size: "$hasFavourite" }, 1],
              },
              then: true,
              else: false,
            },
          },
          else: false,
        },
      },
    },
  });

  return this.aggregate(stages);
};

const placeModel = mongoose.model("Place", placeSchema);

export default placeModel;
