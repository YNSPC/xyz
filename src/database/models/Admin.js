import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
import aggregatePaginate from 'mongoose-aggregate-paginate-v2';
import {
    CompareHashWith,
} from '../../helpers/common-scripts';
import adminPermissionModel from "./AdminPermission";
const schema = mongoose.Schema;

const AdminSchema = new schema(
    {
        firstName: { type: String, required: true },
        lastName: { type: String, required: true },
        profilePicture: { type: String, required: true },
        email: { type: String, required: true },
        password: { type: String, required: true, select: false },
        phone: { type: String, required: true },
        status: { type: Number, default: 0 },
        role: { 
            type: mongoose.Types.ObjectId, 
            ref: adminPermissionModel, 
            required: true 
        }
    },
    {
        timestamps: true
    });

AdminSchema.plugin(mongoosePaginate);
AdminSchema.plugin(aggregatePaginate);

AdminSchema.statics.didPasswordMatch = async function(password, hashedPassword) {
    return await CompareHashWith(password, hashedPassword);
};

const AdminModel = mongoose.model('Admin', AdminSchema);

export default AdminModel;
