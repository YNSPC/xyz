import mongoose from "mongoose";
import { dbConfig } from "../config/index";

export default class Database {
  connect() {
    return mongoose
      .connect(dbConfig.URI, dbConfig.OPTIONS)
      .then((client) => {
        MY_DEBUGGER( { data: `DB connected on: ${dbConfig.URI}`, action: "info" });
        return client;
      })
      .catch((error) => {
        MY_DEBUGGER( { data: `${error.message}`, action: "error" });
      });
  }
}

// import mongoose from 'mongoose';

// // Set up default mongoose connection
// const dbHost = process.env.DB_HOST;
// const dbPort = process.env.DB_PORT;
// const dbName = process.env.DB_NAME;
// const dbUser = process.env.DB_USER;
// const dbPass = encodeURIComponent(process.env.DB_PASS);

// let mongoDB = `mongodb://${dbUser}:${dbPass}@${dbHost}:${dbPort}/${dbName}`;

// // if(process.env.APP_ENV =='production') {
// //    mongoDB =`mongodb+srv://${dbUser}:${dbPass}@${dbHost}/${dbName}?retryWrites=true&w=majority`;
// // }

// const connectDb = function() {
//   mongoose.set('useFindAndModify', false);
//   mongoose.set('useUnifiedTopology', true);
//   mongoose.set('useNewUrlParser', true);
//   //mongoose.set('debug', true);
//   mongoose.connect(mongoDB);

//   mongoose.connection.on('connected', () => {
//     console.log('DB CONNECTED');
//   });

//   mongoose.connection.on('error', (err) => {
//     console.log(`DB CONNECTION ERROR  ${err}`);
//     process.exit(0);
//   });

//   mongoose.connection.on('disconnected', () => {
//     console.log('DB DISCONNECTED');
//   });

//   process.on('SIGINT', () => {
//     mongoose.connection.close(() => {
//       console.log('DB disconnected due to application termination');
//       process.exit(0);
//     });
//   });
// };

// export default connectDb;
