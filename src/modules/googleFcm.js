import firebase from "firebase-admin";

const serviceAccount = require("../../yatribhet-service-account-fcm.json");
firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount)
});
const messaging = firebase.messaging();
const PROJECT_ID =  serviceAccount.project_id;
const HOST = 'fcm.googleapis.com';
const PATH = '/v1/projects/' + PROJECT_ID + '/messages:send';
const MESSAGING_SCOPE = 'https://www.googleapis.com/auth/firebase.messaging';
const SCOPES = [MESSAGING_SCOPE];
const https = require('https');
const { google } = require('googleapis');

function getAccessToken() {
    return new Promise(function(resolve, reject) {
      const jwtClient = new google.auth.JWT(
        serviceAccount.client_email,
        null,
        serviceAccount.private_key,
        SCOPES,
        null
      );
      jwtClient.authorize(function(err, tokens) {
        if (err) {
          reject(err);
          return;
        }
        resolve(tokens.access_token);
      });
    });
}

export const sendMessage = data => {
    const notification = transformNotificationParams(data);
    getAccessToken().then(function(accessToken) {
        const options = {
          hostname: HOST,
          path: PATH,
          method: 'POST',
          // [START use_access_token]
          headers: {
            'Authorization': 'Bearer ' + accessToken,
          },
          // [END use_access_token]
        };

        for (const token of data.tokens) {
            const request = https.request(options, function(resp) {
              resp.setEncoding('utf8');
              resp.on('data', function(data) {
                console.log('Message sent to Firebase for delivery, response:');
                console.log(data);
              });
            });
        
            request.on('error', function(err) {
              console.log('Unable to send message to Firebase');
              console.log(err);
            });
        
            request.write(JSON.stringify({
                message: {   
                    token,
                    ...notification
                  },
            }));
            request.end();
        }
    });
}

export const sendMessage1 = data => {
    const notification = transformNotificationParams(data);
    return new Promise((resolve, reject) => {
        return messaging
            .sendMulticast({
                tokens: ['eGMdOJJFSAyTwd1osghdht:APA91bEPcvQKuQqZ25JmnIfmNCeff-Oziwi9QqHoSS_s0RrkpNyhlFSKOMQM55CeM-rqSsdf4NJIfFtJHHMh8HuvZqw8evUXVfgPRcd_UOTGnlIJIyZh35Dnn51lF0Xv2NXWIWpM0PiX'], 
                notification: {
                    title: 'title',
                    body: "hellow boss"
                }
            })
            .then(fcmResponses => {
                for( const response of fcmResponses.responses ){
                    if ( response.success === true ) return resolve(response);
                    return reject(response)
                }
            })
            .catch(error => {
                console.log("send error", error);
                return reject(error);
            });
    })
};

export const verifyToken = () => firebase.messaging().send({}, true);

export const transformNotificationParams1 = (data, silent = false) => {
    const payload = {
        tokens: data.tokens,
        badge: typeof data.badge !== "undefined" ? String(data.badge+1) : "1",
        notification: {
            body: data.message,
            title: data.title,
        },
        apns: {
            headers: {
                "apns-priority": "10",
            },
            payload: {
                aps: {
                    sound: "default",
                    badge: typeof data.badge !== "undefined" ? parseInt(( data.badge ) + 1 ) : 0
                },
                priority: 10,
            },
        },
        android: {
            priority: "high",
            notification: {
                sound: "default"
            },
        },
        data: {
            body: data.message,
            title: data.title,
            sound: "default",
            fromId: "",//data.notificationData.fromId ? data.notificationData.fromId : "",
            type: "if required type",//data.notificationType,
            badge: "1",//typeof data.badge !== "undefined" ? String(data.badge+1) : "1",
            ...data.notificationData
        },
    };
    if (silent) {
        delete payload.notification;
    }

    return payload;
};

export const parseMessage = (messageToParse, mapObject) => {
    const regularExpression = new RegExp(Object.keys(mapObject).join("|"),"gi");

    return messageToParse.replace(regularExpression, function(matched){
        return mapObject[matched.toLowerCase()];
    });
};

export const transformNotificationParams = (data, silent = false) => {
    const payload = {
        notification: {
            body: 'message' in data ? data.message : 'test body',
            title: 'title' in data ? data.title : 'test title',
        },
        data: {
            body: 'message' in data ? data.message : 'test body',
            title: 'title' in data ? data.title : 'test title',
            sound: "default",
            fromId: "",//data.notificationData.fromId ? data.notificationData.fromId : "",
            type: "if required type",//data.notificationType,
            badge: "1",//typeof data.badge !== "undefined" ? String(data.badge+1) : "1",
            ...data.notificationData
        },
    };
    if (silent) {
        delete payload.notification;
    }

    return payload;
};

export const getTokens = (tokens) => {
    if (typeof tokens === "string") {
        return tokens;
    }

    return tokens.filter(token => token);
}