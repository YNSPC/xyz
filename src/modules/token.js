import Jwt from 'jsonwebtoken';
import { tokenConfig } from '../config/index';
import { system } from '../helpers/constants/database';

export const generateAccessToken = async (user) => {
    try {
        const expiry = Math.floor(Date.now()/1000) + parseInt(tokenConfig.expiryAccessToken);
        const accessToken = await Jwt.sign(
            {
                iss: '',
                sub: user,
                iat: new Date().getTime(),
                exp: expiry
            },
            tokenConfig.secretAccessToken
        );

        return {
            accessToken,
            expiry
        }
    }
    catch (error) {
        console.log('error', error);
    }
};

export const generateRefreshToken = (user) => {
    try {
        const expiry = Math.floor(Date.now()/1000) + parseInt(tokenConfig.expiryRefreshToken);
        return Jwt.sign(
            {
                iss: '',
                sub: user,
                iat: new Date().getTime(),// current time in milliseconds
                exp: expiry //milliseconds
            },
            tokenConfig.secretRefreshToken
        );
    }
    catch (error) {
        console.log('error', error);
    }
};

export const generateJwtToken = async (jwtPayload, action) => {
    let jwtBody = {};
    switch (action) {
      case system.TOKEN.ACCESS_TOKEN:
        jwtBody = {
          payload: {
            iss: "",
            sub: jwtPayload,
            iat: new Date().getTime(),
            exp:
              Math.floor(Date.now() / 1000) +
              parseInt(tokenConfig.expiryAccessToken),
          },
          secret: tokenConfig.secretAccessToken,
          config: {
            expiresIn: tokenConfig.expiryAccessToken,
          },
        };
        break;

      case system.TOKEN.REFRESH_TOKEN:
        jwtBody = {
          payload: {
            iss: "",
            sub: jwtPayload,
            iat: new Date().getTime(),
            exp:
              Math.floor(Date.now() / 1000) +
              parseInt(tokenConfig.expiryRefreshToken),
          },
          secret: tokenConfig.secretRefreshToken,
          config: {
            expiresIn: tokenConfig.expiryRefreshToken,
          },
        };
        break;

      case system.TOKEN.PASSWORD_RESET_TOKEN:
        jwtBody = {
          payload: {
            iss: "",
            sub: jwtPayload,
            iat: new Date().getTime(),
            exp:
              Math.floor(Date.now() / 1000) +
              parseInt(tokenConfig.expiryPasswordResetToken),
          },
          secret: tokenConfig.secretPasswordResetToken,
          config: {
            expiresIn: tokenConfig.expiryPasswordResetToken,
          },
        };
        break;

      case system.TOKEN.FORGOT_PASSWORD_TOKEN.ACTION:
        jwtBody = {
          payload: {
            iss: "",
            sub: jwtPayload,
            iat: new Date().getTime(),
            exp:
              Math.floor(Date.now() / 1000) +
              parseInt(tokenConfig.expiryForgotPasswordToken)
          },
          secret: tokenConfig.secretForgotPasswordToken,
          config: {
            expiresIn: parseInt(tokenConfig.expiryForgotPasswordToken)
          },
        };
        break;

      case system.TOKEN.VERIFICATION_TOKEN.ACTION:
        jwtBody = {
          payload: {
            iss: "",
            sub: jwtPayload,
            iat: Math.floor(Date.now() / 1000),
            exp:
              Math.floor(Date.now() / 1000) +
              parseInt(tokenConfig.expiryAccountVerificationToken),
          },
          secret: tokenConfig.secretAccountVerificationToken,
          config: {
            expiresIn: tokenConfig.expiryAccountVerificationToken,
          },
        };
        break;
    }

    return Jwt.sign(jwtBody.payload, jwtBody.secret);
};

export const verifyJwtToken = async (token, action) => {
    try {
        let secretForVerification = "";
        switch (action) {
          case system.TOKEN.VERIFICATION_TOKEN.ACTION:
            secretForVerification = tokenConfig.secretAccountVerificationToken;
            break;

          case system.TOKEN.FORGOT_PASSWORD_TOKEN.ACTION:
            secretForVerification = tokenConfig.secretForgotPasswordToken;
            break;
        }

        return Jwt.verify(token, secretForVerification, (err, decoded) => {
            if (err)
                return {
                    success: false,
                    code: 47001,
                    message: err.message,
                    data: {}
                };

            return {
                success: true,
                code: 47002,
                message: "jwt-verified",
                data: decoded
            };
        });
    } catch (err) {
        return { exception: err };
    }
};