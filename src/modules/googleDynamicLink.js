import urlBuilder from "build-url";
import axios from "axios";
import { serverConfig, firebaseConfig } from "../config";

export const dynamicLongLink = payload => {
    const redirectUrl = `${firebaseConfig.dynamicLink.protocol}://${serverConfig.URL}/??referralLink/??${payload}`;
    const dynamicLinkBaseUrl = firebaseConfig.dynamicLink.dynamicLinkBaseUrl;
    const androidPackageName = firebaseConfig.dynamicLink.androidPackageName;
    const iosBundleId = firebaseConfig.dynamicLink.iosBundleId;

    return urlBuilder(dynamicLinkBaseUrl, {
        queryParams: {
            redirectUrl,
            apn: androidPackageName,
            ibi: iosBundleId,
        },
    });
};

export const shortDynamicLink = async longDynamicLink => {
    try {
        const firebaseApiKey = firebaseConfig.dynamicLink.firebaseApiKey;
        const firebaseShortLinkApi = `${firebaseConfig.dynamicLink.shorteningUrl}${firebaseApiKey}`;
        const firebaseResponse = await axios.post(firebaseShortLinkApi, { longDynamicLink });

        return {
            success: true,
            data: firebaseResponse.data.shortLink
        };
    }
    catch (exception) {
        return {
            success: false,
            reason: exception.message
        };
    }
};
