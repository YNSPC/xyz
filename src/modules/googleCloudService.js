import { Storage } from "@google-cloud/storage";
import path from "path";
import uuid from "uuid/v1";
import { format } from "util";
import { googleConfig } from "../config";

const GOOGLE_CLOUD_PROJECT_ID = googleConfig.bucket.projectId;
const GOOGLE_CLOUD_KEY_FILE= path.join(__dirname, `../../${googleConfig.bucket.keyFile}`);
const GOOGLE_PUBLIC_URL = googleConfig.bucket.publicUrl;

const storage = new Storage({
    projectId: GOOGLE_CLOUD_PROJECT_ID,
    keyFilename: GOOGLE_CLOUD_KEY_FILE
});

export const copyFileToGCS = (fileToUpload, bucketNaming='yatribhet', options={}) => {
  return new Promise((resolve, reject) => {
    let bucketName = process.env.NODE_ENV != 'live' ? bucketNaming : 'live-yatribhet';
    options = options || {};
    const {originalname, buffer} = fileToUpload;
  
    const bucket = storage.bucket(bucketName);
    const blob = bucket.file(`posts/test.png`);

    // return bucket.upload(localFilePath, options)
    //     .then(() => { blob.makePublic() })
    //     .then(() => { `${GOOGLE_PUBLIC_URL}${bucketName}/${fileName}` })
    //     .catch((error) => {
    //         console.log({error})
    //     })

    const blobStream = blob.createWriteStream({
      resumable: false
    })
    blobStream.on('finish', async () => {
      const publicUrl = format(`${GOOGLE_PUBLIC_URL}${bucket.name}/${blob.name}`);
      // try {
      //   // Make the file public
      //   await bucket.file(originalname).makePublic();
      // } 
      // catch(error) {
      //   console.log(error);
      //   return reject(`Uploaded the file successfully: ${originalname}, but public access is denied!, ${publicUrl}`);
      // }
      return resolve(publicUrl);
    })
    .on('error', (err) => {
      console.log("error", err);
      reject(`Unable to upload image, something went wrong`)
    })
    .end(buffer)
  });

};

export const uploadFilesToGCS = (filesToUpload, options={}, bucketNaming='yatribhet') => {
  // let promises = [];
  let urlKeys = [];
  let bucketName = process.env.NODE_ENV != 'live' ? bucketNaming : 'live-yatribhet';
  const bucket = storage.bucket(bucketName);
  const folder = options.folder;

  for (const fileToUpload of filesToUpload) {
    const {buffer, originalname} = fileToUpload;
    const key = `${folder}/${uuid()}${path.extname(originalname)}`;
    const blob = bucket.file(key);
    new Promise((resolve, reject) => {
      const blobStream = blob.createWriteStream({
        resumable: false
      })
      blobStream.on('finish', async () => {
        const publicUrl = format(`${GOOGLE_PUBLIC_URL}${bucket.name}/${blob.name}`);
        return resolve(publicUrl);
      })
      .on('error', (err) => {
        console.log("error", err);
        reject(`Unable to upload image, something went wrong`)
      })
      .end(buffer)
    });

    // promises.push(uploadPromise);
    urlKeys.push(key);
  }
  
  // return Promise.all(promises);
  return urlKeys;
};

export const uploadFileToGCS = (fileToUpload, options={}, bucketNaming='yatribhet') => {

  let bucketName = process.env.NODE_ENV != 'live' ? bucketNaming : 'live-yatribhet';
  const bucket = storage.bucket(bucketName);
  const folder = options.folder;

  return new Promise((resolve, reject) => {
    const {buffer, originalname} = fileToUpload;
    const key = `${folder}/${uuid()}${path.extname(originalname)}`;
    const blob = bucket.file(key);
    const blobStream = blob.createWriteStream({ resumable: false });
    blobStream.on('finish', async () => {
      const publicUrl = format(`${GOOGLE_PUBLIC_URL}${bucket.name}/${blob.name}`);
      return resolve({ 
        key,
        publicUrl
      });
    })
    .on('error', (err) => {
      console.log("error", err);
      reject(`Unable to upload image, something went wrong`)
    })
    .end(buffer)
  });
};

export const generateGoogleSignedUrl = async (key, contentType) => {
  const bucketName = process.env.NODE_ENV != 'live' 
    ? googleConfig.bucket.name 
    : googleConfig.bucketLive.name;
 
  const options = {
    version: 'v4',
    action: 'write',
    expires: Date.now() + 5 * 60 * 1000,
    contentType: contentType
  };

  try {
    const [url] = await storage
      .bucket(bucketName)
      .file(key, {generation: undefined})
      .getSignedUrl(options);

    return {
      key,
      url
    };
  }
  catch (error) {
    throw new Error(error.message||"unable to generate signedurl GOOGLE");
  }
};
