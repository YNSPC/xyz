import { google } from 'googleapis';
import fs from 'fs';
import path from 'path';

const clientId = '410603519842-8068s9jgoh6jk6g1hcjr1ei0e3l6b5d3.apps.googleusercontent.com';
const clientSecret = 'IyDwlIARv0KqPHq1FEixBd8K';
const redirectUri = 'https://developers.google.com/oauthplayground';
const refreshToken = '1//04f5NVy9MiTTXCgYIARAAGAQSNwF-L9IrhhMAN0iUPZn9WgHp3HfQ7KkaYmb879bhWX5tOYH5H8CYmGidou-jVqCQ2LEuzFzYCQw';

const oauth2client = new google.auth.OAuth2(
    `${clientId}`,
    `${clientSecret}`,
    `${redirectUri}`,
);

oauth2client.setCredentials({
    refresh_token: refreshToken
});

const drive = google.drive({
    version: 'v3',
    auth: oauth2client
});
const filePath = path.join(__dirname, 'square.png');

export const generatePublicUrl = async () => {
    try {
        const response = await drive.files.create({
            requestBody: {
                name: 'shape.png',
                mimeType: 'image/png'
            },
            media: {
                mimeType: 'image/png',
                body: fs.createReadStream(filePath)
            }
        });

        console.log(response.data)
    }
    catch (e) {
        console.log(e.message)
    }
}
