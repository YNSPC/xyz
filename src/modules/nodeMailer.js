import { createTransport } from 'nodemailer';
import Hogan from "hogan.js";
import path from "path";
import fs from "fs";
import { emailConfig } from '../config/index';

export const sendMail = (data) => {
    const options = {
        transporter: {
            host: emailConfig.host,
            port: emailConfig.port,
            secure:  false,
            auth: {
                user: emailConfig.authUserName,
                pass: emailConfig.authPassword
            }
        },
        defaults: {}
    };
    const transport = createTransport(options.transporter);
    const appDir = path.dirname(require.main.filename);
    const templatesDir = path.join(appDir, "./../src/templates/email_templates");
    const { from, to, subject, template, emailDataObject: { attachments, ...dataPayload } } = data;
    const readTemplate = fs.readFileSync(`${templatesDir}/${template}.hjs`, "utf-8");
    const compiledTemplate = Hogan.compile(readTemplate);
    const mailPayload = {
        from,
        to,
        subject,
        html: compiledTemplate.render(dataPayload),
        attachments: attachments
    };

    return new Promise((resolve, reject) => {
        transport.sendMail(mailPayload, (error, info) => {
            if ( error ){
                console.log('error', error);
                return reject({
                    success: false,
                    message: error.message
                });
            }
            console.log('envelope', info.envelope);
            console.log('messageId', info.messageId);
            return resolve({
                success: true,
                message: "Email sent."
            });
        });
    })
};
