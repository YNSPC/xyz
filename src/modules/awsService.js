import S3 from 'aws-sdk/clients/s3';
import { awsConfig } from "../config";

 const s3 = new S3({
  region: awsConfig.s3Bucket.region,
  accessKeyId: awsConfig.s3Bucket.accessKeyId,
  secretAccessKey: awsConfig.s3Bucket.secretAccessKey,
  signatureVersion: awsConfig.s3Bucket.version
});

export const generateAwsS3SignedUrl = async (key, contentType) => {
  const bucketName = process.env.NODE_ENV != 'live'
    ? awsConfig.s3Bucket.name
    : awsConfig.s3BucketLive.name;

  try {
    const presignedPUTURL = await s3.getSignedUrlPromise('putObject', {
      Bucket: bucketName,
      Key: key,
      Expires: 3600,
      ContentType: contentType,
    });

    return {
      key,
      url: presignedPUTURL
    };
  }
  catch (error) {
    throw new Error(error.message||"unable to generate signedurl Aws s3");
  }
};

export const generateAwsS3ViewSignedUrl = async (key) => {
  const bucketName = process.env.NODE_ENV != 'live'
    ? awsConfig.s3Bucket.name
    : awsConfig.s3BucketLive.name;

  try {
    const presignedGetUrl = await s3.getSignedUrlPromise('getObject', {
      Bucket: bucketName,
      Key: key,
      Expires: 120,
    });

    return {
      key,
      url: presignedGetUrl
    };
  }
  catch (error) {
    throw new Error(error.message||"unable to generate signedurl Aws s3");
  }
};

export const deleteAwsS3Content = async (key) => {
  const bucketName = process.env.NODE_ENV != 'live'
      ? awsConfig.s3Bucket.name
      : awsConfig.s3BucketLive.name;

  try {
    s3.deleteObject({
      Bucket: bucketName,
      Key: key,
    }, (error, data) => {
      console.log('delete object', error, data)
    }); 
  }
  catch (error) {
    throw new Error(error.message||"unable to delete signedurl Aws s3");
  }
}

export const deleteFromAllVersions = async (key) => {
  const bucketName = process.env.NODE_ENV != 'live'
      ? awsConfig.s3Bucket.name
      : awsConfig.s3BucketLive.name;

  s3.listObjectVersions({
    Bucket: bucketName,
    Prefix: key
  }, (error, data) => {
    if (!error) {
      for (const version of data.Versions) {
        s3.deleteObject({
          Bucket: bucketName,
          Key: key,
          VersionId: version.VersionId
        }, (error, data) => {
          console.log('delete object', error, data)
        });
      }
    }
  }); 
}
