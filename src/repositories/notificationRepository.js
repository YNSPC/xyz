import NotificationModel from "../database/models/Notifications";
import { projections } from "../helpers/projections/projection";

export const createNotification = async payload => {
    const notificationObject = new NotificationModel(payload);
    await notificationObject.save();

    return notificationObject;
};

export const listNotifications = async (options, user) => {
    const aggregateOption = {
        page: options.page || 1,
        limit: options.limit || 10,
        sort: {
            createdAt: -1
        },
    };

    let stages = [];

    stages.push({
        $match: {
            receiver: user._id
        }
    });

    stages.push({
        $lookup: {
            from: "users",
            let: { "sender": "$sender" },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: [ "$_id", "$$sender" ]
                        }
                    }
                },
                {
                    $project: {
                        ...projections.USER.PROFILE_PICTURE,
                        _id: 1,
                        userName: 1,
                        fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
                    }
                }
            ],
            as: "sender"
        }
    });

    stages.push({
        $unwind: "$sender"
    });

    stages.push({
        $lookup: {
            from: "users",
            let: { "receiver": "$receiver" },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: [ "$_id", "$$receiver" ]
                        }
                    }
                },
                {
                    $project: {
                        ...projections.USER.PROFILE_PICTURE,
                        _id: 1,
                        userName: 1,
                        fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
                    }
                }
            ],
            as: "receiver"
        }
    });

    stages.push({
        $unwind: "$receiver"
    });

    stages.push({
        $project: {
            _id: 0
        }
    });

    const aggregatedNotifications = NotificationModel.aggregate(stages);
    return NotificationModel.aggregatePaginate(aggregatedNotifications, aggregateOption);
};

export const readNotification = async notificationId => {
    return NotificationModel.findOneAndUpdate(
        {
            _id: notificationId
        },
        {
            $set: {
                hasRead: true
            }
        },
        {
            new: true,
            useFindAndModify: false
        }
    );
};

export const notificationDetail = async notificationId => NotificationModel.findOne({ _id: notificationId });

export const deleteNotifications = async userId => NotificationModel.deleteMany({
    $or: [
        { sender: userId },
        { receiver: userId }
    ]
});