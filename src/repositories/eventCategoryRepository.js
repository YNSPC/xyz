import EventCategoryModel from "../database/models/EventCategories";

export const listEventCategory = async (payload) => {
    return await EventCategoryModel.getCategories(payload);
};
