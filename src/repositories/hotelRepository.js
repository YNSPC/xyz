import HotelModel from "../database/models/Hotels";
import { getBaseUrl } from "../helpers/common-scripts";

const hotelById = async hotelId => {
    const hotelInfo = await HotelModel.findOne(
        { 
            _id: hotelId 
        },
        {
            placeId: 1,            
            hospitalityType: 1,
            location: 1,
            address: 1,
            name: 1,
            rating: 1,
            displayImage: { $concat: [ getBaseUrl(), "$displayImage" ] },
            images: {
                $cond: {
                  if: { $ne: [{$size: "$images"}, 0] },
                  then: {
                    $map: {
                      input: "$images",
                      as: "image",
                      in: {$concat: [getBaseUrl(), "$$image"]}
                    }
                  },
                  else: []
                }
            },
            information: 1,
            facilities: {
                $cond: {
                    if: { $eq: [{$size: "$facilities"}, 0] },
                    then: [],
                    else: {
                        $map: {
                            input: "$facilities",
                            as: "facility",
                            in: {
                                popular: "$$facility.popular",
                                name: "$$facility.name",
                                icon: { $concat: [getBaseUrl(), "$$facility.icon"] }
                            }
                        }
                    }
                }
            },
            rooms: {
                $cond: {
                    if: { $eq: [{$size: "$rooms"}, 0] },
                    then: [],
                    else: {
                        $map: {
                            input: "$rooms",
                            as: "room",
                            in: {
                                roomType: "$$room.roomType",
                                displayImage: { $concat: [getBaseUrl(), "$$room.displayImage"] },
                                images: {
                                    $cond: {
                                        if: { $eq: [{$size: "$$room.images"}, 0] },
                                        then: [],
                                        else: {
                                            $map: {
                                                input: "$$room.images",
                                                as: "image",
                                                in: { $concat: [getBaseUrl(), "$$image"] }
                                            }
                                        }
                                    }
                                },
                                facilities: {
                                    $cond: {
                                        if: { $eq: [{$size: "$$room.facilities"}, 0] },
                                        then: [],
                                        else: {
                                            $map: {
                                                input: "$$room.facilities",
                                                as: "facility",
                                                in: {
                                                    popular: "$$facility.popular",
                                                    name: "$$facility.name",
                                                    icon: { $concat: [getBaseUrl(), "$$facility.icon"] }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            isInContact: 1
        }
    );

    return hotelInfo;
};

const getHotelsOfGivePlace = (place, options) => {
    let stages = [];
    let filter;

    options = options || {};

    filter = {
        $match: {
            placeId: place._id
        }
    };

    if ( options.search ) {
        filter.$match.name = {
            $regex: options.search,
            $options: 'i'
        };
    }

    if (options.minimumOfPriceRange && options.maximumOfPriceRange ) {
        filter.$match.$and = [];
        filter.$match.$and.push({ startAmount: { $gte: parseFloat(options.minimumOfPriceRange) } });
        filter.$match.$and.push({ startAmount: { $lt: parseFloat(options.maximumOfPriceRange) } });
    }

    if (options.rating) { filter.$match.rating = { $gte: parseInt(options.rating) }; }

    stages.push(filter);

    stages.push({
        $sort: {
            rating: -1,
            priceRange: 1
        }
    });

    stages.push({
        $project: {
            placeId: 1,
            hospitalityType: 1,
            location: 1,
            address: 1,
            name: 1,
            rating: 1,
            displayImage: { $concat: [ getBaseUrl(), "$displayImage" ] },
            information: 1,
            isInContact: 1,
            images:{
                $map:{
                    input: "$images",
                    as: "image",
                    in: { $concat: [ getBaseUrl(), "$$image" ] },
                }
            },
            facilities: {
                $map:{
                    input: "$facilities",
                    as: "row",
                    in: {
                        name: "$$row.name",
                        popular: "$$row.popular",
                        icon: { $concat: [ getBaseUrl(), "$$row.icon" ] }
                    }
                }
            },
            rooms: {
                $map:{
                    input: "$rooms",
                    as: "room",
                    in: {
                        roomType: "$$room.roomType",
                        description: "$$room.description",
                        displayImage: { $concat: [ getBaseUrl(), "$$room.displayImage" ] },
                        images:{
                            $map:{
                                input: "$images",
                                as: "image",
                                in: { $concat: [ getBaseUrl(), "$$image" ] },
                            }
                        },
                        facilities: {
                            $map:{
                                input: "$facilities",
                                as: "row",
                                in: {
                                    name: "$$row.name",
                                    popular: "$$row.popular",
                                    icon: { $concat: [ getBaseUrl(), "$$row.icon" ] }
                                }
                            }
                        },
                    }
                }
            }                       
        }
    });

    return HotelModel.aggregate(stages);
};

//TODO: Join with place table to list hotels based on the popularity of the place.
const listHotels = async (search, filters, options) => {
    const stages = [];
    const aggregateOption = {
        page: options.page,
        limit: options.limit,
        sort: options.sorting,
    };
    let filter = { $match: {} };

    if (search) {
        filter.$match.$or = [];
        filter.$match.$or.push({
            location: {
                $regex: search,
                $options: "i",
            },
        });
        filter.$match.$or.push({
            name: {
                $regex: search,
                $options: "i",
            },
        });
    }

    if ( filters.rating ) filter.$match.rating = filters.rating;

    if ( filters.amount !== undefined ) {
        if ( ("minimum" in filters.amount && filters.amount.minimum ) ||  "maximum" in filters.amount && filters.amount.maximum )
            filter.$match.$and = [];
        if ( "minimum" in filters.amount && filters.amount.minimum )
            filter.$match.$and.push({ startAmount: { $gte: parseFloat(filters.amount.minimum) } });
        if ( "maximum" in filters.amount && filters.amount.maximum )
            filter.$match.$and.push({ startAmount: { $lt: parseFloat(filters.amount.maximum) } });
    }

    stages.push(filter);

    stages.push({
        $project: {
            name: 1,
            rating: 1,
            address: 1,
            hospitalityType: 1,
            displayImage: { $concat: [ getBaseUrl(), "$displayImage" ] },
            facilities: {
                $cond: {
                    if: { $eq: [{$size: "$facilities"}, 0] },
                    then: [],
                    else: {
                        $map: {
                            input: "$facilities",
                            as: "facility",
                            in: {
                                popular: "$$facility.popular",
                                name: "$$facility.name",
                                icon: { $concat: [getBaseUrl(), "$$facility.icon"] }
                            }
                        }
                    }
                }
            },
        }
    })

    const aggregatedPlaces = HotelModel.aggregate(stages);

    return HotelModel.aggregatePaginate(aggregatedPlaces, aggregateOption);
};

export const adminHotelList = (options) => {
    let stages = [];
    let filter;

    const aggregateOption = {
        page: options.page,
        limit: options.limit,
        sort: options.sorting,
    };

    filter = {
        $match: {},
    };

    if (options.search) {
        filter.$match.$or = [];

        filter.$match.$or.push({
            name: {
                $regex: options.search.trim(),
                $options: "i",
            },
        });
    }

    stages.push(filter);

    const aggregatedTags = HotelModel.aggregate(stages);

    return HotelModel.aggregatePaginate(aggregatedTags, aggregateOption);
};

export const createHotel = async (payload) => {
    const hotelObject = new HotelModel(payload);
    await hotelObject.save();

    return hotelObject.toObject();
};

export default {
    listHotels,
    hotelById,
    getHotelsOfGivePlace
};
