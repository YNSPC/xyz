import ReportModel from "../database/models/Reports";
import PlaceModel from "../database/models/Places";
import {getBaseUrl} from "../helpers/common-scripts";

export const createReport = async (payload) => {
    const reportObject = new ReportModel(payload);
    await reportObject.save();
    return reportObject;
};

export const paginatedListing = async (options) => {
    let stages = [];
    let filter = {
        $match: {},
    };

    if (options.search) {
        filter.$match.$or = [];
        filter.$match.$or.push({
            zone: {
                $regex: options.search,
                $options: "i",
            },
        });

        filter.$match.$or.push({
            state: {
                $regex: options.search,
                $options: "i",
            },
        });

        filter.$match.$or.push({
            district: {
                $regex: options.search,
                $options: "i",
            },
        });

        filter.$match.$or.push({
            name: {
                $regex: options.search.trim(),
                $options: "i",
            },
        });
    }

    // stages.push(filter);

    stages.push({
        $lookup: {
            from: "users",
            let: { reporter: "$reportedBy" },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: [ "$_id", "$$reporter" ]
                        }
                    }
                },
                {
                    $project: {
                        _id: 1,
                        profilePicture: {
                            $cond: {
                                if: { $regexMatch: { input: "$profilePicture", regex: /http/ } },
                                then: "$profilePicture",
                                else: {
                                    $concat: [getBaseUrl(), "$profilePicture"]
                                }
                            }
                        },
                        fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
                    }
                }
            ],
            as: "reporterDetail"
        }
    });

    stages.push({
        $unwind: "$reporterDetail"
    });

    stages.push({
        $facet: {
            pagination: [
                {
                    $count: "totalDocs"
                },
                {
                    $addFields: {
                        totalPages: {
                            $ceil: {
                                $divide: [ { $toInt: "$totalDocs" } , { $toInt: options.limit } ]
                            }
                        }
                    }
                },
                {
                    $addFields: {
                        page: parseInt(options.page),
                        limit: parseInt(options.limit),
                        hasNextPage: {
                            $cond: {
                                if: {
                                    $gt: ["$totalPages", parseInt(options.page)]
                                },
                                then: true,
                                else: false
                            }
                        },
                        hasPrevPage: {
                            $cond: {
                                if: {
                                    $eq: [parseInt(options.page), 1]
                                },
                                then: false,
                                else: true
                            }
                        }
                    }
                }
            ],
            docs: [
                {
                    $skip: (parseInt(options.page) - 1) * (parseInt(options.limit) || 10)
                },
                {
                    $limit: parseInt(options.limit) || 10
                }
            ],
        }
    });

    const data = await ReportModel
        .aggregate(stages)
        .allowDiskUse(true);

    const desiredDocs = data[0].docs ? data[0].docs : [];
    const pagination =
        data[0].pagination && data[0].pagination[0] !== undefined
            ? data[0].pagination[0]
            : {
                total: 0,
                page: parseInt(options.pageNumber),
            };

    return {
        pagination,
        docs: desiredDocs,
    };
};

export const deleteReports = async userId => {
    return ReportModel.deleteMany({
        reportedBy: userId
    })
}