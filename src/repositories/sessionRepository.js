import SettingModel from "../database/models/Settings";

export const findRemoteConfig = async () => {
    const settings = await SettingModel.find().exec();
    
    return settings.pop();
}
