import ChatUserModel from "../database/models/chatUser";
import PrivateChatModel from "../database/models/PrivateChat";
import UserModel from "../database/models/Users";
import { objectId } from "../helpers/common-scripts";
import { projections } from "../helpers/projections/projection";

export const createChat = async payload => {
  const chatObject = new PrivateChatModel(payload);
  await chatObject.save();

  return chatObject;
};

export const paginatedChatHistory = async (options) => {
  const stages = [];

  stages.push({
    $match: {
      $expr: {
        $and: [
          {
            $or: [
              {
                $and: [
                  { $eq: ["$sender", options.loggedUser._id] },
                  { $eq: ["$receiver", objectId(options.friendId)] }
                ]
              },
              {
                $and: [
                  { $eq: ["$receiver", options.loggedUser._id] },
                  { $eq: ["$sender", objectId(options.friendId)] }
                ]
              }
            ],
          },
          // {
          //   deletedUsers: {
          //     $not: {
          //       $in : [options.loggedUser._id, objectId(options.friendId)]
          //     }
          //   }
          //   // $not: {
          //   //   $in : [[options.loggedUser._id, objectId(options.friendId)], [{$ifNull:["$deletedUsers", []]}]]
          //   // }
          // }
        ]
      }
    }
  });

  stages.push({
    $lookup: {
      from: "users",
      let: {
        userId: "$sender"
      },
      pipeline: [
        {
          $match: {
            $expr: {
              $eq: ["$$userId", "$_id"]
            }
          }
        },
        {
          $project: {
            ...projections.USER.PROFILE_PICTURE,
            id: "$_id",
            name: { $concat: ["$firstName", " ", "$lastName"] },
            _id: 0
          }
        }
      ],
      as: "sender"
    }
  });

  stages.push({
    $unwind: "$sender"
  });

  stages.push({
    $sort: {
      createdAt: -1
    }
  });

  // TODO: rather test using addFields to add other fields
  stages.push({
    $project: {
      messageId: 1,
      messageType: 1,
      sender: 1,
      receiver: 1,
      voice: 1,
      images: 1,
      videos: 1,
      message: 1,
      hideFromSender: 1,
      hideFromReceiver: 1,
      hasImage: { 
        $cond: {
          if: { $gt: [{ $size: "$images" }, 0] },
          then: true,
          else: false
        }
      },
      hasVideo: { 
        $cond: {
          if: { $gt: [{ $size: "$videos" }, 0] },
          then: true,
          else: false
        }
      }
    }
  });

  stages.push({
    $facet: {
      pagination: [
        {
          $count: "totalDocs"
        },
        {
          $addFields: {
            totalPages: {
              $ceil: {
                $divide: [{ $toInt: "$totalDocs" }, { $toInt: options.limit }]
              }
            }
          }
        },
        {
          $addFields: {
            page: parseInt(options.page),
            limit: parseInt(options.limit),
            hasNextPage: {
              $cond: {
                if: {
                  $gt: ["$totalPages", parseInt(options.page)]
                },
                then: true,
                else: false
              }
            },
            hasPrevPage: {
              $cond: {
                if: {
                  $eq: [parseInt(options.page), 1]
                },
                then: false,
                else: true
              }
            }
          }
        }
      ],
      docs: [
        {
          $skip: (parseInt(options.page) - 1) * parseInt(options.limit)
        },
        {
          $limit: parseInt(options.limit)
        }
      ],
    }
  });

  const data = await PrivateChatModel
    .aggregate(stages)
    .allowDiskUse(true);

  const desiredDocs = data[0].docs ? data[0].docs : [];
  const pagination =
    data[0].pagination && data[0].pagination[0] !== undefined
      ? data[0].pagination[0]
      : {
        totalDocs: 0,
        totalPages: 0,
        page: parseInt(options.page),
        limit: parseInt(options.paglimit),
        hasNextPage: false,
        hasPrevPage: false
      };

  return {
    pagination,
    docs: desiredDocs,
  };
};

export const paginatedChatUserListing = async options => {
  const stages = [];
console.log({options});
  stages.push({
    $match: {
      _id: options.loggedUser._id
    }
  });

  stages.push({
    $project: {
      _id: 0,
      chatWith: 1
    }
  });

  stages.push({
    $unwind: "$chatWith"
  });

  stages.push({
    $lookup: {
      from: "users",
      let: {
        userId: "$chatWith"
      },
      pipeline: [
        {
          $match: {
            $expr: {
              $eq: ["$$userId", "$_id"]
            }
          }
        },
        {
          $project: {
            ...projections.USER.PROFILE_PICTURE,
            _id: 1,
            fullName: { $concat: ["$firstName", " ", "$lastName"] }
          }
        }
      ],
      as: "friend"
    }
  });

  stages.push({
    $unwind: "$friend"
  });

  stages.push({
    $replaceRoot: { newRoot: "$friend" }
  });

  stages.push({
    $lookup: {
      from: "privatechats",
      let: {
        friendId: "$_id"
      },
      pipeline: [
        {
          $match: {
            $expr: {
              $and: [
                {
                  $not: {
                    $in : [options.loggedUser._id, { $ifNull: ["$deletedUsers",[]] } ]
                  }
                },
                {
                  $or: [
                    {
                      $and: [
                        { $eq: ["$sender", options.loggedUser._id] },
                        { $eq: ["$receiver", "$$friendId"] },
                      ]
                    },
                    {
                      $and: [
                        { $eq: ["$sender", "$$friendId"] },
                        { $eq: ["$receiver", options.loggedUser._id] },
                      ]
                    }
                  ]
                }
              ]
            }
          }
        },
        {
          $sort: {
            createdAt: -1
          }
        },
        {
          $limit: 1
        },
        {
          $project: {
            _id: 1,
            message: 1,
            updatedAt: 1
          }
        }
      ],
      as: "message"
    }
  });

  stages.push({
    $unwind: "$message"
  });

  stages.push({
    $sort: {
      "message.updatedAt": -1
    }
  })

  stages.push({
    $facet: {
      pagination: [
        {
          $count: "totalDocs"
        },
        {
          $addFields: {
            totalPages: {
              $ceil: {
                $divide: [{ $toInt: "$totalDocs" }, { $toInt: options.limit }]
              }
            }
          }
        },
        {
          $addFields: {
            page: parseInt(options.page),
            limit: parseInt(options.limit),
            hasNextPage: {
              $cond: {
                if: {
                  $gt: ["$totalPages", parseInt(options.page)]
                },
                then: true,
                else: false
              }
            },
            hasPrevPage: {
              $cond: {
                if: {
                  $eq: [parseInt(options.page), 1]
                },
                then: false,
                else: true
              }
            }
          }
        }
      ],
      docs: [
        {
          $skip: (parseInt(options.page) - 1) * parseInt(options.limit)
        },
        {
          $limit: parseInt(options.limit)
        }
      ],
    }
  });

  const data = await UserModel
    .aggregate(stages)
    .allowDiskUse(true);

  const desiredDocs = data[0].docs ? data[0].docs : [];
  const pagination =
    data[0].pagination && data[0].pagination[0] !== undefined
      ? data[0].pagination[0]
      : {
        totalDocs: 0,
        totalPages: 0,
        page: parseInt(options.page),
        limit: parseInt(options.paglimit),
        hasNextPage: false,
        hasPrevPage: false
      };

  return {
    pagination,
    docs: desiredDocs,
  };
};

export const isFirstChat = async ({ sender, receiver }) => {
  const chatObject = await PrivateChatModel.findOne({
    $or: [
      {
        $and: [
          { sender: objectId(sender.id) },
          { receiver: objectId(receiver) }
        ]
      },
      {
        $and: [
          { sender: objectId(receiver) },
          { receiver: objectId(sender.id) }
        ]
      }
    ]
  });

  return chatObject ? false : true;
};

export const updateChatUserList = async (filter, updateQuery) => {
  return UserModel.findOneAndUpdate(
    {
      ...filter
    },
    {
      ...updateQuery
    },
    {
      new: true
    }
  );
};

export const updateOrCreateUserHistory = async (payload) => {
  return ChatUserModel.findByIdAndUpdate(
    {
      user: objectId(payload.user),
      chatUser: objectId(payload.chatUser),
    },
    {
      $set: {
        role: payload.role,
        message: payload.message
      }
    },
    {
      upsert: true,
      new: true
    }
  )
};

export const deletePrivateMessages = async userId => {
  return PrivateChatModel.deleteMany({
    $or: [
      { sender: userId },
      { receiver: userId }
    ]
  })
}

export const removeChatFromMyself = async (userId, friendId) => {
  return await PrivateChatModel.updateMany({
    $or: [
      { sender: userId, receiver: friendId },
      { sender: friendId, receiver: userId }
    ]
  }, {
    $addToSet: {
      deletedUsers: userId
    }
  })
}