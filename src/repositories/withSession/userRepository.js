import moment from "moment";
import UserModel from "../../database/models/Users";
import NotificationModel from "../../database/models/Notifications";
import { objectId } from "../../helpers/common-scripts";
import {projections} from "../../helpers/projections/projection";

//TODO: try to get rid of session transaction on the listing api
export const userDetailWithFcmWithSession = async ({ userId }, { showTag= true, showFcm = true}, session) => {
    const stages = [];
    const followedUserId = typeof userId === "string" ? objectId(userId) : userId;
    stages.push({
        $match: {
            _id: followedUserId
        }
    });

    if ( showTag )
        stages.push({
            $lookup: {
                from: 'tags',
                let: { 'favouriteTags': '$favouriteTags' },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $in: ["$_id", "$$favouriteTags"]
                            }
                        }
                    },
                    /*{
                        $project: {

                        }
                    }*/
                ],
                as: "favouriteTagsObject"
            }
        });

    if ( showFcm )
        stages.push({
        $lookup: {
            from: 'devices',
            let: { userId: '$_id' },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: ["$$userId", "$user"]
                        }
                    }
                },
               /* {
                    $project: {

                    }
                }*/
            ],
            as: "devicesObject"
        }
    });

    stages.push({
        $project: {
            ...projections.USER.PROFILE,
            fcmTokens: {
                $map:{
                    input: "$devicesObject",
                    as: "loggedDevice",
                    in: "$$loggedDevice.deviceToken"
                }
            }
        }
    });

    const result = await UserModel.aggregate(stages);

    return result.length > 0 ? result.pop() : {};
};

export const followUnfollowRemoveUser = async (randomUser, logger, action, session) => {
    let userInfo;

    switch (action) {
        case "follow":
            userInfo = await UserModel.findOneAndUpdate(
                {
                    _id: logger._id
                },
                {
                    $addToSet: {
                        followings: {
                            userId: randomUser._id,
                            followedAt: moment.utc().unix()
                        }
                    }
                },
                {
                    new: true,
                    session
                }
            );

            await UserModel.findOneAndUpdate(
                {
                    _id: randomUser._id
                },
                {
                    $addToSet: {
                        followers: {
                            userId: logger._id,
                            followedAt: moment.utc().unix()
                        }
                    }
                },
                {
                    new: true,
                    session
                }
            );

            break;

        case "unfollow":
            userInfo = await UserModel.findOneAndUpdate(
                {
                    _id: logger._id
                },
                {
                    $pull: {
                        followings: {
                            userId: randomUser._id
                        }
                    }
                },
                {
                    new: true,
                    session
                }
            );

            await UserModel.findOneAndUpdate(
                {
                    _id: randomUser._id
                },
                {
                    $pull: {
                        followers: {
                            userId: logger._id
                        }
                    }
                },
                {
                    session
                }
            );
            break;

        case "remove_follower":
            userInfo = await UserModel.findOneAndUpdate(
                {
                    _id: logger._id
                },
                {
                    $pull: {
                        followers: {
                            userId: randomUser._id
                        }
                    }
                },
                {
                    new: true,
                    session
                }
            );

            await UserModel.findOneAndUpdate(
                {
                    _id: randomUser._id
                },
                {
                    $pull: {
                        followings: {
                            userId: logger._id
                        }
                    }
                },
                {
                    session
                }
            );
            break;
    }

    return userInfo;
};

export const followUserInFollowings = async (randomUser, logger, session) => {
    return await UserModel.updateOne(
        {
            _id: logger._id
        },
        {
            $addToSet: {
                followings: {
                    userId: randomUser._id,
                    followedAt: moment.utc().unix()
                }
            }
        },
        {
            session
        }
    )
};

export const followUserInFollowers = async (randomUser, logger, session) => {
    return await UserModel.updateOne(
        {
            _id: randomUser._id
        },
        {
            $addToSet: {
                followers: {
                    userId: logger._id,
                    followedAt: moment.utc().unix()
                }
            }
        },
        {
            session
        }
    );
};

export const createNotification = async (payload, session) => {
    const notificationObject = new NotificationModel(payload);
    await notificationObject.save({ session });

    return notificationObject;
};