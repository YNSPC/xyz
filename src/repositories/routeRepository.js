import PlaceModel from "../database/models/Places";
import CsvFileTrackerModel from "../database/models/helpers/csvFileTracker";

export const storeRoutes = async (routeList, uploadObject) => {
    let uploadedResult = {
        model: "places",
        uploadedFileName: uploadObject.originalname,
        totalProcessingTime: 0,
        uploadedFrom: "browser name needed for sessions",
        totalRows: routeList.length,
        totalEmptyRows: 0,
        totalInsertedRows: 0,
        totalCorruptedRows: 0,
        totalDuplicateRows: 0,
        corruptedData: [],
        duplicatedData: [],
    };
    let insertCount = 0;
    let duplicateCount = 0;
    let duplicateData = [];

    if (routeList.length === 0) return 0;

    for (const route of routeList) {
        const routeFound = await PlaceModel.findOne({ myUniqueCode: route.placeId });
        if (!routeFound) {
            //there is no place for this route to insert
            // duplicateCount++;
            // duplicateData.push({
            //     rowPosition: 0,
            //     columnPosition: 0,
            //     reason: `Route with name ${routeFound.name} was already stored.`,
            // });
        }
        // else if ( routeFound.routes && routeFound.routes.name.includes(route.name) ) {
        //     //there is already a route with this name for this place
        //     // insertCount++;
        //     // const tagDataModel = new PlaceModel(tag);
        //     // await tagDataModel.save();
        // }
        else {
            insertCount++;
            await PlaceModel.findOneAndUpdate(
                {
                    myUniqueCode: route.placeId
                },
                {
                    $push: {
                        "routes": {
                            "name": route.name,
                            "myRouteUniqueCode": route.myRouteUniqueCode
                        }
                    }
                },
                {
                    new: true
                }
            );
        }
    }

    uploadedResult.totalDuplicateRows = duplicateCount;
    uploadedResult.totalInsertedRows = insertCount;
    uploadedResult.duplicatedData = duplicateData;

    // const tracker = new CsvFileTrackerModel(uploadedResult);
    // await tracker.save();

    return uploadedResult;
};

export const storeSubRoutes = async (subRouteList, uploadObject) => {
    let uploadedResult = {
        model: "places",
        uploadedFileName: uploadObject.originalname,
        totalProcessingTime: 0,
        uploadedFrom: "browser name needed for sessions",
        totalRows: subRouteList.length,
        totalEmptyRows: 0,
        totalInsertedRows: 0,
        totalCorruptedRows: 0,
        totalDuplicateRows: 0,
        corruptedData: [],
        duplicatedData: [],
    };
    let insertCount = 0;
    let duplicateCount = 0;
    let duplicateData = [];

    if (subRouteList.length === 0) return 0;

    for (const subRoute of subRouteList) {
        const subRouteFound = await PlaceModel.findOne({ "routes.myRouteUniqueCode": subRoute.myRouteUniqueCode });
        if (!subRouteFound) {
            //there is no place for this route to insert
            // duplicateCount++;
            // duplicateData.push({
            //     rowPosition: 0,
            //     columnPosition: 0,
            //     reason: `Route with name ${routeFound.name} was already stored.`,
            // });
        }
        // else if ( subRouteFound.routes && subRouteFound.routes.name.includes(subRoute.name) ) {
        //     //there is already a route with this name for this place
        //     // insertCount++;
        //     // const tagDataModel = new PlaceModel(tag);
        //     // await tagDataModel.save();
        // }
        else {
            insertCount++;
            const data = await PlaceModel.findOneAndUpdate(
                {
                    "routes.myRouteUniqueCode": subRoute.myRouteUniqueCode
                },
                {
                    $push: {
                        'routes.$[route].subRoutes': {
                            name: subRoute.name,
                            description: subRoute.description,
                            starting: subRoute.starting,
                            ending: subRoute.ending,
                            myCode: subRoute.myCode,
                            position: subRoute.position
                        }
                    }
                },
                {
                    useFindAndModify: false,
                    arrayFilters: [
                        {
                            "route.myRouteUniqueCode": { $eq: subRoute.myRouteUniqueCode }
                        },
                    ]
                }
            );
        }
    }

    uploadedResult.totalDuplicateRows = duplicateCount;
    uploadedResult.totalInsertedRows = insertCount;
    uploadedResult.duplicatedData = duplicateData;

    // const tracker = new CsvFileTrackerModel(uploadedResult);
    // await tracker.save();

    return uploadedResult;
};
