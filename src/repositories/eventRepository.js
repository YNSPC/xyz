import EventModel from "../database/models/Event";
import { getBaseUrl, objectId } from "../helpers/common-scripts";

export const createEvent = async (payload) => {
    const eventObject = new EventModel(payload);
    await eventObject.save();

    return eventObject.toObject();
};

export const eventById = async (eventId) => {
    const eventDetail = await EventModel.findById(eventId);

    return eventDetail.toObject();
};

export const listEvent = async (options) => {
    const stages = [
       {
            $match: {
                date: { $gte: new Date() }
            }
       }
    ];
    const aggregateOption = {
        page: options.page,
        limit: options.limit,
        // sort: options.sorting,
    };
    // let filter = { $match: {} };

    // if (search) {
    //     filter.$match.$or = [];
    //     filter.$match.$or.push({
    //         location: {
    //             $regex: search,
    //             $options: "i",
    //         },
    //     });
    //     filter.$match.$or.push({
    //         name: {
    //             $regex: search,
    //             $options: "i",
    //         },
    //     });
    // }

    // stages.push(filter);

    // stages.push({
    //     $lookup: {
    //         from: "users",
    //         localField: "members.userId",
    //         foreignField: "_id",
    //         as: "members",
    //         pipeline: [
    //             {
    //                 $project: {
    //                     _id: 1,
    //                     firstName: 1,
    //                     profilePicture: { $concat: [ getBaseUrl("aws"), "$profilePicture" ]}
    //                 }
    //             }
    //         ]
    //     }
    // });

    stages.push({
        $lookup: {
            from: "users",
            localField: "members.userId",
            foreignField: "_id",
            as: "memberDetail",
            pipeline: [
                {
                    $project: {
                        _id: 1,
                        firstName: 1,
                        profilePicture: { $concat: [ getBaseUrl("aws"), "$profilePicture" ]}
                    }
                }
            ]
        }
    });

    stages.push({
        $addFields: {
            joinedMembers: {
                $map: {
                    input: '$members',
                    in: {
                            $mergeObjects: [
                                '$$this',
                                {
                                    $arrayElemAt: [
                                        '$memberDetail',
                                        {
                                            $indexOfArray: ['$memberDetail._id', '$$this.userId'],
                                        },
                                    ],
                                },
                            ],
                    },
                },
            },
        },
    })

    stages.push({
        $lookup: {
            from: "users",
            localField: "organizer",
            foreignField: "_id",
            as: "organizer",
            pipeline: [
                {
                    $project: {
                        _id: 1,
                        firstName: 1,
                        lastName: 1,
                        profilePicture: { $concat: [ getBaseUrl("aws"), "$profilePicture" ]}
                    }
                }
            ]
        }
    });

    stages.push({
        $lookup: {
            from: "eventcategories",
            localField: "categories",
            foreignField: "_id",
            as: "categories",
            pipeline: [
                {
                    $project: {
                        _id: 1,
                        name: 1,
                        // profilePicture: { $concat: [ getBaseUrl("aws"), "$profilePicture" ]}
                    }
                }
            ]
        }
    });

    stages.push({
        $sort: {
            createdAt: -1
        }
    });

    stages.push({
        $project: {
            name: 1,
            totalTravellers: 1,
            date: 1,
            duration: 1,
            description: 1,
            from: 1,
            to: 1,
            organizer: { $first: "$organizer" },
            status: 1,
            private: 1,
            members: {
                $filter: {
                    input: "$joinedMembers",
                    as: "member",
                    cond: { $eq: [ "$$member.status", "joined" ] }
                }
            },
            categories: 1,
            picture: { $concat: [ getBaseUrl("aws"), "$picture" ] }
        }
    })

    const aggregatedEvents = EventModel.aggregate(stages);

    return EventModel.aggregatePaginate(aggregatedEvents, aggregateOption);
};

export const updateEvent = async (filter, updatePayload) => {
    return EventModel.findOneAndUpdate(
        {
            ...filter
        },
        {
            $set: { ...updatePayload }
        },
        {
            useFindAndModify: false,
            new: true
        }
    )
}

export const deleteEvent = async (filter) => {
    return await EventModel.deleteOne(filter);
}

export const eventDetail = async (eventId, loggedUser) => {
    const stages = [
        {
            $match: {
                _id: objectId(eventId)
            }
        }
    ];
    
    stages.push({
        $lookup: {
            from: "users",
            localField: "members.userId",
            foreignField: "_id",
            as: "memberDetail",
            pipeline: [
                {
                    $project: {
                        _id: 1,
                        firstName: 1,
                        lastName: 1,
                        profilePicture: { $concat: [ getBaseUrl("aws"), "$profilePicture" ]}
                    }
                }
            ]
        }
    });

    stages.push({
        $addFields: {
            allMembers: {
            $map: {
                input: '$members',
                in: {
                    $mergeObjects: [
                    '$$this',
                    {
                        $arrayElemAt: [
                        '$memberDetail',
                        {
                            $indexOfArray: ['$memberDetail._id', '$$this.userId'],
                        },
                        ],
                    },
                    ],
                },
                },
            },
        },
    });

    stages.push({
        $lookup: {
            from: "users",
            localField: "organizer",
            foreignField: "_id",
            as: "organizer",
            pipeline: [
                {
                    $project: {
                        _id: 1,
                        firstName: 1,
                        lastName: 1,
                        profilePicture: { $concat: [ getBaseUrl("aws"), "$profilePicture" ]}
                    }
                }
            ]
        }
    });

    stages.push({
        $lookup: {
            from: "eventcategories",
            localField: "categories",
            foreignField: "_id",
            as: "categories",
            pipeline: [
                {
                    $project: {
                        _id: 1,
                        name: 1,
                        // profilePicture: { $concat: [ getBaseUrl("aws"), "$profilePicture" ]}
                    }
                }
            ]
        }
    });

    stages.push({
        $project: {
            name: 1,
            totalTravellers: 1,
            date: 1,
            duration: 1,
            description: 1,
            from: 1,
            to: 1,
            organizer: { $first: "$organizer" },
            status: 1,
            private: 1,
            members: {
                $filter: {
                    input: "$allMembers",
                    as :"member",
                    cond: { $eq: [ "$$member.status", "joined" ] }
                }
            },
            totalJoined: { 
                $size: {
                    $filter: {
                        input: "$allMembers",
                        as :"member",
                        cond: { $eq: [ "$$member.status", "joined" ] }
                    }
                } 
            },
            totalInvited: { 
                $size: {
                    $filter: {
                        input: "$allMembers",
                        as :"member",
                        cond: { $eq: [ "$$member.status", "invited" ] }
                    }
                } 
            },
            categories: 1,
            picture: { $concat: [ getBaseUrl("aws"), "$picture" ] },
            joiningStatus: {
                $switch: {
                  branches: [
                    {
                      case: {
                        $gt: [
                          {
                            $size: {
                              $filter: {
                                input: "$allMembers",
                                as: "member",
                                cond: {
                                  $and: [
                                    {
                                      $eq: ["$$member._id", loggedUser._id]
                                    },
                                    {
                                      $eq: ["$$member.status", "joined"]
                                    }
                                  ]
                                }
                              }
                            }
                          }, 0
                        ]
                      },
                      then: "joined"
                    },
                          {
                      case: {
                        $gt: [
                          {
                            $size: {
                              $filter: {
                                input: "$allMembers",
                                as: "member",
                                cond: {
                                  $and: [
                                    {
                                      $eq: ["$$member._id", loggedUser._id]
                                    },
                                    {
                                      $eq: ["$$member.status", "invited"]
                                    }
                                  ]
                                }
                              }
                            }
                          }, 0
                        ]
                      },
                      then: "invited"
                    }
                  ],
                    default: "none"
                }, 
            }
        }
    })

    const detail = await EventModel.aggregate(stages);

    return detail.length > 0 ? detail.pop() : null;
};

export const joinTheEvent = async(eventId, joiningUser) => {
    return EventModel.updateOne(
        { _id: eventId },
        {
            $addToSet: {
                members: {
                    userId: joiningUser._id,
                    joinedOn: new Date(),
                    status: "joined"
                }
            }
        },
        {
            useFindAndModify: false,
            new: true
        }
    )
}

export const inviteToTheEvent = async(eventId, invitedUserId, invitedBy) => {
    return EventModel.updateOne(
        { _id: eventId },
        {
            $addToSet: {
                members: {
                    userId: invitedUserId,
                    invitedOn: new Date(),
                    status: "invited"
                }
            }
        },
        {
            useFindAndModify: false,
            new: true
        }
    )
}