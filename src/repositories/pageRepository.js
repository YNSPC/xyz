import PageModel from "../database/models/Pages";

export const pageById = async pageId => (await PageModel.findOne({_id: pageId}).exec()).toObject();

export const adminPageList = (options) => {
    let stages = [];
    let filter;

    const aggregateOption = {
        page: options.page,
        limit: options.limit,
        sort: options.sorting,
    };

    filter = {
        $match: {},
    };

    if (options.search) {
        filter.$match.$or = [];

        filter.$match.$or.push({
            name: {
                $regex: options.search.trim(),
                $options: "i",
            },
        });
    }

    stages.push(filter);

    const aggregatedTags = PageModel.aggregate(stages);

    return PageModel.aggregatePaginate(aggregatedTags, aggregateOption);
};

export const createPage = async (payload) => {
    const pageObject = new PageModel(payload);
    await pageObject.save();

    return pageObject.toObject();
};
