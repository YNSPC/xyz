import TagModel from "../database/models/Tags";
import CsvFileTrackerModel from "../database/models/helpers/csvFileTracker";
import sharp from "sharp";
import path from "path";
import fs from "fs";
import { fullURL } from "../helpers/common-scripts";

//storing list of tags from csv
const storeTags = async (tagList, uploadObject) => {
  let uploadedResult = {
    model: "tags",
    uploadedFileName: uploadObject.originalname,
    totalProcessingTime: 0,
    uploadedFrom: "browser name needed for sessions",
    totalRows: tagList.length,
    totalEmptyRows: 0,
    totalInsertedRows: 0,
    totalCorruptedRows: 0,
    totalDuplicateRows: 0,
    corruptedData: [],
    duplicatedData: [],
  };
  let insertCount = 0;
  let duplicateCount = 0;
  let duplicateData = [];

  if (tagList.length === 0) return 0;

  for (const tag of tagList) {
    const tagFound = await TagModel.findOne({ name: tag.name });
    if (tagFound) {
      duplicateCount++;
      duplicateData.push({
        rowPosition: 0,
        columnPosition: 0,
        reason: `Tag with name ${tagFound.name} was already stored.`,
      });
    } else {
      insertCount++;
      const tagDataModel = new TagModel(tag);
      await tagDataModel.save();
    }
  }

  uploadedResult.totalDuplicateRows = duplicateCount;
  uploadedResult.totalInsertedRows = insertCount;
  uploadedResult.duplicatedData = duplicateData;

  const tracker = new CsvFileTrackerModel(uploadedResult);
  await tracker.save();

  return uploadedResult;
};

const listTags = async () => {
  return TagModel.find({}, { _id: 1, name: 1 });
};

const getTagByNames = async (tagNames) => {
  return TagModel.aggregate([
    {
      $match: {
        name: {
          $in: tagNames,
        },
      },
    },
    {
      $project: {
        _id: 1,
      },
    },
  ]);
};

const uploadImages = async (identifier, uploadObject) => {
  await sharp(uploadObject[0].path)
    .webp()
    .toFile(
      path.resolve(
        `${uploadObject[0].destination}`,
        "sharp/",
        `${uploadObject[0].filename.split(".")[0]}.webp`
      )
    );

  fs.unlinkSync(uploadObject[0].path);

  console.log(fullURL("tags", uploadObject[0].filename));
  return TagModel.findOneAndUpdate(
    { _id: identifier },
    { $set: { icon: fullURL("tags", uploadObject[0].filename) } },
    { new: true, useFindAndModify: false }
  );
};

export const adminTagList = (options) => {
  let stages = [];
  let filter;

  const aggregateOption = {
    page: options.page,
    limit: options.limit,
    sort: options.sorting,
  };
  console.log(aggregateOption);

  filter = {
    $match: {},
  };

  if (options.search) {
    filter.$match.$or = [];

    filter.$match.$or.push({
      name: {
        $regex: options.search.trim(),
        $options: "i",
      },
    });
  }

  stages.push(filter);

  const aggregatedTags = TagModel.aggregate(stages);

  return TagModel.aggregatePaginate(aggregatedTags, aggregateOption);
};

export const adminTagListForDropDown = async () => {
  return await TagModel.find({}, { name: 1, _id: 1 }).sort({
    name: 1,
  });

  return data.reduce(
    (obj, tag) => Object.assign(obj, { [tag._id]: tag.name }),
    {}
  );
};

export const getTagById = async (tagId) => (await TagModel.findOne({_id: tagId}).exec()).toObject();

export const storeTagViaApi = async (payload) => {
  //results into { _id: ..., ...payload }
  const tagObject = new TagModel(payload);
  console.log("obj", tagObject);
  await tagObject.save();
  
  return tagObject.toObject();
};

export const updateTagViaApi = async (tagId, payload) => {
  return await TagModel.findOneAndUpdate(
    {
      _id: tagId, //lets make this always a mongoId
      // status: false
    },
    {
      $set: payload
    },
    {
      new: true,
      returnNewDocument: true,
      // strict: true, //this option can let user add un-declared field of schema 'TRUE'
      // strictQuery: false, // Turn off strict mode for query filters
                          // In Mongoose 6, strictQuery is equal to strict by default.
                          // use mongoose.set('strictQuery', false); instead
      // multi: false, // update only one document 
      // upsert: false,  // insert a new document, if no existing document match the query 
      // rawResult: false, // tracing success
    }
  );
};

export const createTag = async (payload) => {
  const tagObject = new TagModel(payload);
  await tagObject.save();

  return tagObject.toObject();
};

export const deleteTagRepo = async (tagId, payload) => {
  return await TagModel.findOneAndDelete(
    {
      _id: tagId
    },
  ).exec();
};

export default {
  storeTags,
  listTags,
  uploadImages,
  getTagByNames,
};
