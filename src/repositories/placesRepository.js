import PlaceModel from "../database/models/Places";
import TagModel from "../database/models/Tags";
import FavouriteModel from "../database/models/Favourite";
import CsvFileTrackerModel from "../database/models/helpers/csvFileTracker";
import { isValidPlaceRow, fullURL, objectId, getBaseUrl } from "../helpers/common-scripts";
import { enums } from "../helpers/constants/database";
import fs from "fs";
import path from "path";
import sharp from "sharp";
import mongoose from "mongoose";

const getPlaces = (options) => {
  let stages = [];
  let filter;

  const optAggregate = {
    page: options.page || 1,
    limit: options.limit || 10,
    sort: {
      famousRating: -1,
      name: 1,
    },
  };

  filter = {
    $match: {},
  };

  if (options.search) {
    filter.$match.name = {
      $regex: options.search,
      $options: "i",
    };
  }

  stages.push(filter);

  stages.push({
    $lookup: {
      from: "tags",
      let: { tags: "$tags" },
      pipeline: [
        {
          $match: {
            $expr: {
              $in: ["$_id", "$$tags"],
            },
          },
        },
        {
          $project: {
            name: 1,
          },
        },
      ],
      as: "tags",
    },
  });

  stages.push({
    $lookup: {
      from: "favourites",
      let: { placeId: "$_id" },
      pipeline: [
        {
          $match: {
            $expr: {
              $and: [
                {
                  $eq: ["$placeId", "$$placeId"],
                },
                {
                  $eq: ["$userId", options.logger],
                },
              ],
            },
          },
        },
      ],
      as: "hasFavourite",
    },
  });

  stages.push({
    $project: {
      myUniqueCode: 1,
      displayImage: {
        $cond: {
          if: { $ifNull: ["$displayImage", true ] },
          then: { $concat: [getBaseUrl(), "$displayImage"] },
          else: null
        }
      },
      images: {
        $cond: {
          if: {$ne: [{$size: "$images"}, 0]},
          then: {
            $map: {
              input: "$images",
              as: "image",
              in: {$concat: [getBaseUrl(), "$$image"]}
            }
          },
          else: []
        }
      },
      tags: 1,
      parent: 1,
      country: 1,
      name: 1,
      description: 1,
      popularName: 1,
      placeType: 1,
      state: 1,
      zone: 1,
      district: 1,
      famousRating: 1,
      location: 1,
      isFavourite: {
        $cond: {
          if: {
            $isArray: "$hasFavourite",
          },
          then: {
            $cond: {
              if: {
                $eq: [{ $size: "$hasFavourite" }, 1],
              },
              then: true,
              else: false,
            },
          },
          else: false,
        },
      },
    },
  });

  const aggregatedPlaces = PlaceModel.aggregate(stages);

  return PlaceModel.aggregatePaginate(aggregatedPlaces, optAggregate);
};

const placeById = async (placeId) =>
  PlaceModel.findOne({ _id: placeId }).exec();

const storePlaces = async (placesList, uploadObject) => {
  let uploadedResult = {
    model: "places",
    uploadedFileName: uploadObject.originalname,
    totalProcessingTime: 0,
    uploadedFrom: "browser name needed for sessions",
    totalRows: placesList.length,
    totalEmptyRows: 0,
    totalInsertedRows: 0,
    totalCorruptedRows: 0,
    totalDuplicateRows: 0,
    corruptedData: [],
    duplicatedData: [],
  };
  let insertCount = 0;
  let corruptedCount = 0;
  let duplicateCount = 0;
  let duplicateData = [];
  let corruptedData = [];

  if (placesList.length === 0) return 0;

  for (const place of placesList) {
    if (!isValidPlaceRow(place)) {
      corruptedCount++;
      corruptedData.push({
        rowPosition: 0,
        columnPosition: 0,
        reason: `the row itself is not in correct format.`,
      });
      continue;
    }

    const found = await PlaceModel.getPlaceBy({ name: place.name });

    if (found) {
      duplicateCount++;
      duplicateData.push({
        rowPosition: 0,
        columnPosition: 0,
        reason: `Place with name ${found.name} was already stored.`,
      });
      continue;
    }

    insertCount++;

    if (place.parentCode) {
      const placeObject = await PlaceModel.getPlaceBy({
        myUniqueCode: place.parentCode,
      });
      if (!placeObject) {
        corruptedCount++;
        corruptedData.push({
          rowPosition: 0,
          columnPosition: 0,
          reason: `unable to find the parent data.`,
        });
        continue;
      }
      place.parent = placeObject._id;
    }

    if (place.tags) {
      const tags = place.tags.split(",");
      place.tags = await TagModel.getTagFromArray(tags);
    }

    place.location = {
      coordinates: [place.latitude, place.longitude],
    };

    // console.log(place);

    const placeDataModel = new PlaceModel(place);
    await placeDataModel.save();
  }

  uploadedResult.totalDuplicateRows = duplicateCount;
  uploadedResult.totalCorruptedRows = corruptedCount;
  uploadedResult.totalInsertedRows = insertCount;
  uploadedResult.duplicatedData = duplicateData;
  uploadedResult.corruptedData = corruptedData;

  const tracker = CsvFileTrackerModel(uploadedResult);
  await tracker.save();

  return uploadedResult;
};

const uploadImages = async (identifier, type, uploadObject) => {
  const selectedPlace = await PlaceModel.findOne({ myUniqueCode: identifier });
  let updateObject;
  let images = [];

  if (type === enums.UPLOAD_IMAGE_TYPE.DISPLAY_IMAGE) {
    await sharp(uploadObject[0].path)
      .webp()
      .toFile(
        path.resolve(
          `${uploadObject[0].destination}`,
          "sharp/",
          `${uploadObject[0].filename.split(".")[0]}.webp`
        )
      );

    fs.unlinkSync(uploadObject[0].path);

    updateObject = {
      displayImage: fullURL("places", uploadObject[0].filename),
    };
  } else {
    for (let data of uploadObject) {
      //upload the optimized image
      await sharp(data.path)
        .webp()
        .toFile(
          path.resolve(
            `${data.destination}`,
            "sharp/",
            `${data.filename.split(".")[0]}.webp`
          )
        );

      images.push(fullURL("places", data.filename));
      fs.unlinkSync(data.path);
    }
    updateObject = {
      images: [...selectedPlace.images, ...images],
    };
  }

  const updatedRecord = await PlaceModel.findOneAndUpdate(
    {
      myUniqueCode: identifier,
    },
    {
      $set: updateObject,
    },
    {
      new: true,
      useFindAndModify: false,
    }
  );

  return updatedRecord;
};

const listRecommendedPlaces = async ({ tags, placeId }) => {
  let stages = [];
  const placeDetail = await PlaceModel.findOne({ _id: placeId });
  let hasChild = true;
  let parentId = placeDetail._id;
  if (!placeDetail.parent) {
    // child places where self is parent 
    const childPlaces = await PlaceModel.countDocuments({ parent: placeDetail._id });
    if (childPlaces === 0) {
      hasChild = false;
    } else {
      parentId = placeDetail._id;
    }
  } else {
    // child places with other place as parent
    const childPlaces = await PlaceModel.countDocuments({ 
      parent: placeDetail.parent,
      _id: { $ne: placeDetail._id } 
    });
    if (childPlaces === 0) {
      hasChild = false;
    } else {
      parentId = placeDetail.parent;
    }
  }

  stages.push({
    $match: {
      ...( hasChild ? 
        {
          parent: parentId
        } :
        {
          tags: { $in: tags }
        }
      ),
      ...(placeId ? { _id: { $ne: placeId } } : {}),
    },
  });
  stages.push({
    $lookup: {
      from: "tags",
      let: { tags: "$tags" },
      pipeline: [
        {
          $match: {
            $expr: {
              $in: ["$_id", "$$tags"],
            },
          },
        },
        {
          $project: {
            name: 1,
          },
        },
      ],
      as: "tags",
    },
  });
  
  stages.push({
    $limit: 10,
  });

  stages.push({
    $project: {
      _id: 1,
      name: 1,
      displayImage: {
        $cond: {
          if: { $ifNull: ["$displayImage", true ] },
          then: { $concat: [getBaseUrl(), "$displayImage"] },
          else: null
        }
      },
    }
  })

  return PlaceModel.aggregate(stages);
};

const placeDetail = async ({ placeId, logger }) => {
  let stages = [];
  stages.push({
    $match: {
      _id: mongoose.Types.ObjectId(placeId),
    },
  });

  stages.push({
    $lookup: {
      from: "tags",
      let: { tags: "$tags" },
      pipeline: [
        {
          $match: {
            $expr: {
              $in: ["$_id", "$$tags"],
            },
          },
        },
      ],
      as: "tagsObject",
    },
  });

  stages.push({
    $lookup: {
      from: "favourites",
      let: { placeId: "$_id" },
      pipeline: [
        {
          $match: {
            $expr: {
              $and: [
                {
                  $eq: ["$placeId", "$$placeId"],
                },
                {
                  $eq: ["$userId", logger],
                },
              ],
            },
          },
        },
      ],
      as: "hasFavourite",
    },
  });

  stages.push({
    $lookup: {
      from: "reviews",
      let: { placeId: "$_id" },
      pipeline: [
        {
          $match: {
            $expr: {
              $and: [
                {
                  $eq: ["$placeId", "$$placeId"],
                },
                {
                  $eq: ["$userId", logger],
                },
              ],
            },
          },
        },
      ],
      as: "reviewDetail",
    },
  });

  stages.push({
    $project: {
      country: 1,
      name: 1,
      description: 1,
      popularName: 1,
      location: 1,
      displayImage: {
        $cond: {
          if: { $ifNull: ["$displayImage", true ] },
          then: { $concat: [getBaseUrl(), "$displayImage"] },
          else: null
        }
      },
      displayMap: {
        $cond: {
          if: { $ifNull: ["$displayMap", true ] },
          then: { $concat: [getBaseUrl(), "$displayMap"] },
          else: null
        }
      },
      images: {
        $cond: {
          if: {$ne: [{$size: "$images"}, 0]},
          then: {
            $map: {
              input: "$images",
              as: "image",
              in: {$concat: [getBaseUrl(), "$$image"]}
            }
          },
          else: []
        }
      },
      placeType: 1,
      state: 1,
      zone: 1,
      district: 1,
      myUniqueCode: 1,
      parentCode: 1,
      famousRating: 1,
      parent: 1,
      tags: 1,
      tagsObject: 1,
      isFavourite: {
        $cond: {
          if: {
            $isArray: "$hasFavourite",
          },
          then: {
            $cond: {
              if: {
                $eq: [{ $size: "$hasFavourite" }, 1],
              },
              then: true,
              else: false,
            },
          },
          else: false,
        },
      },
      isReviewed: {
        $cond: {
          if: {
            $isArray: "$reviewDetail",
          },
          then: {
            $cond: {
              if: {
                $eq: [{ $size: "$reviewDetail" }, 1],
              },
              then: true,
              else: false,
            },
          },
          else: false,
        },
      },
      routes: 1
    },
  });

  const placeDetail = await PlaceModel.aggregate(stages);

  return placeDetail.length > 0 ? placeDetail.pop() : null;
};

const toggleFavouritePlace = async (userId, placeId) => {
  const favouriteDetail = await FavouriteModel.findOne({
    userId: userId,
    placeId: placeId,
  });

  if (!favouriteDetail) {
    const favouriteObject = new FavouriteModel({ userId, placeId });
    await favouriteObject.save();

    return true;
  }

  await FavouriteModel.findByIdAndDelete({ _id: favouriteDetail._id });
  return false;
};

const listFavouritePlace = async (userId) => {
  const stages = [];

  stages.push({
    $match: {
      userId: userId,
    },
  });

  stages.push({
    $lookup: {
      from: "places",
      let: { placeId: "$placeId" },
      pipeline: [
        {
          $match: {
            $expr: {
              $eq: ["$_id", "$$placeId"],
            },
          },
        },
      ],
      as: "placeDetail",
    },
  });

  stages.push({
    $unwind: "$placeDetail",
  });

  stages.push({
    $project: {
      favouriteId: "$_id",
      _id: "$placeDetail._id",
      name: "$placeDetail.name",
      displayImage: { $concat: [ getBaseUrl(), "$placeDetail.displayImage" ] },
    },
  });

  return FavouriteModel.aggregate(stages);
};

const listPlaceByTag = async (tagId) => {
  const stages = [];

  stages.push({
    $match: {
      tags: {
        $in: [mongoose.Types.ObjectId(tagId)],
      },
    },
  });

  stages.push({
    $lookup: {
      from: "favourites",
      let: { placeId: "$_id" },
      pipeline: [
        {
          $match: {
            $expr: {
              $eq: ["$placeId", "$$placeId"],
            },
          },
        },
      ],
      as: "hasFavourite",
    },
  });

  stages.push({
    $project: {
      country: 1,
      name: 1,
      description: 1,
      popularName: 1,
      location: 1,
      displayImage: 1,
      images: 1,
      placeType: 1,
      state: 1,
      zone: 1,
      district: 1,
      myUniqueCode: 1,
      parentCode: 1,
      famousRating: 1,
      parent: 1,
      tags: 1,
      tagsObject: 1,
      isFavourite: {
        $cond: {
          if: {
            $isArray: "$hasFavourite",
          },
          then: {
            $cond: {
              if: {
                $eq: [{ $size: "$hasFavourite" }, 1],
              },
              then: true,
              else: false,
            },
          },
          else: false,
        },
      },
    },
  });

  return PlaceModel.aggregate(stages);
};

export const adminPlaceList = (options) => {
  let stages = [];
  let filter;

  const aggregateOption = {
    page: options.page,
    limit: options.limit,
    sort: options.sorting,
  };

  filter = {
    $match: {},
  };

  if (options.search) {
    filter.$match.$or = [];
    filter.$match.$or.push({
      zone: {
        $regex: options.search,
        $options: "i",
      },
    });

    filter.$match.$or.push({
      state: {
        $regex: options.search,
        $options: "i",
      },
    });

    filter.$match.$or.push({
      district: {
        $regex: options.search,
        $options: "i",
      },
    });

    filter.$match.$or.push({
      name: {
        $regex: options.search.trim(),
        $options: "i",
      },
    });
  }

  stages.push(filter);

  stages.push({
    $project: {
      myUniqueCode: 1,
      displayImage: 1,
      images: 1,
      tags: 1,
      parent: 1,
      country: 1,
      name: 1,
      description: 1,
      popularName: 1,
      placeType: 1,
      state: 1,
      zone: 1,
      district: 1,
      famousRating: 1,
      location: 1,
    },
  });

  const aggregatedPlaces = PlaceModel.aggregate(stages);

  return PlaceModel.aggregatePaginate(aggregatedPlaces, aggregateOption);
};

export const adminPlaceListForDropdown = async () => {
  return await PlaceModel.find({}, { _id: 1, name: 1 }).sort({ name: 1 });
  return data.reduce(
    (obj, item) => Object.assign(obj, { [item._id]: item.name }),
    {}
  );
};

export const adminPlaceCreate = async (body) => {
  try {
    const placeObject = new PlaceModel({...body, location: { coordinates: [1,2] }});
    await placeObject.save();

    return placeObject.toObject();
  }catch (e) {
    throw new Error(e.message);
  }
};

export const adminPlaceUpdate = async (placeId, payload) => {
  try {
    return await PlaceModel.updateOne(
      { _id: placeId },
      {
        $set: { 
          ...payload, 
          location: { coordinates: [ 1.1, 2.1 ] }
        }
      },
      {
        new: true
      }
    );
  }catch (e) {
    throw new Error(e.message);
  }
};

export const deleteFavourites = async (userId) => {
    return await FavouriteModel.deleteMany({ 
      userId: userId 
    });
};

export const deletePlaceWithId = async (placeId) => {
  return await PlaceModel.deleteOne(
    { _id: placeId },
  );
}

export const searchedPlace = async (search) => {
  return PlaceModel.find({
      name: {
          $regex: search,
          $options: 'i'
      }
  }, {
      _id: 1,
      name: 1,
      displayImage: {
          $concat: [ getBaseUrl("aws") , "$displayImage" ]
      }
  }).limit(5);
}

export default {
  placeById,
  storePlaces,
  uploadImages,
  placeDetail,
  listPlaceByTag,
  listFavouritePlace,
  toggleFavouritePlace,
  listRecommendedPlaces,

  getPlaces,
};
