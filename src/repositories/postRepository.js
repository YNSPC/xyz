import PostModel from "../database/models/Posts";
import {undefinedCase, getBaseUrl, objectId} from "../helpers/common-scripts";
import { projections } from "../helpers/projections/projection";

const getAllPost = (options, loggedUser, guestLogin= false) => {
    const currentTime = new Date()
    const followingIds = !loggedUser 
        ? [] 
        : loggedUser.followings.map( following => following.userId );

    const blockedIds = !loggedUser 
        ? [] 
        : loggedUser.blockedTo;

    // const filterFromFollowings = followingIds.filter(following => !blockedIds.includes(following));
    // filterFromFollowings.push(loggedUser._id);
    
    const optAggregate = {
        page: options.page || 1,
        limit: options.limit || 10,
        // sort: {
        //     createdAt: -1
        // },
    };

    let stages = [];

    stages.push({
        $lookup: {
            from: "users",
            let: { "userId": "$author" },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: [ "$_id", "$$userId" ]
                        }
                    }
                },
                {
                    $project: {
                        ...projections.USER.PROFILE_PICTURE,
                        _id: 1,
                        userName: 1,
                        fullName: { $concat: [ "$firstName", " ", "$lastName" ] }
                    }
                }
            ],
            as: "authorDetail"
        }
    });

    stages.push({
        $unwind: "$authorDetail"
    });

    stages.push({
        $lookup: {
            from: "comments",
            localField: "_id",
            foreignField: "post",
            as: "commentsInPost"
        }
    });

    stages.push({
        $match: {
            visibility: "public"
        }
    });

    stages.push({
        $lookup: {
            from: "tags",
            localField: "tags",
            foreignField: "_id",
            as: "tags"
        }
    });

    if ( !guestLogin ) {
        if (blockedIds.length > 0) {
            stages.push({
                $match: {
                    author: {
                        $nin: blockedIds
                    }
                }
            });
        }
    }

    stages.push({
        $project: {
            _id: 1,
            postType: 1,
            location: 1,
            title: 1,
            description: 1,
            visibility: 1,
            status: 1,
            editedAt: 1,
            sharedBy: 1,
            likedBy: 1,
            hiddenBy: 1,
            reportedBy: 1,
            tags: 1,
            author: 1,
            tripCoverPhoto: 1,
            tripInfo: 1,
            createdAt: 1,
            media: {
                $map:{
                    input: "$media",
                    as: "row",
                    in: {
                        _id: "$$row._id",
                        mediaType: "$$row.mediaType",
                        mediaUrl: {
                            originalUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.originalUrl" ] },
                            smallUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.smallUrl" ] },
                            mediumUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.mediumUrl" ] },
                            largeUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.largeUrl" ] },
                        },
                    }
                  }
            },
            authorDetail: 1,
            totalLikes: { $size: "$likedBy" },
            totalComments: { $size: "$commentsInPost" },
            hasLiked: {
                $cond: {
                    if: { $eq: [ guestLogin, true ] },
                    then: false,
                    else: {
                        $in: [ undefinedCase(loggedUser, ["_id"], null), "$likedBy" ]
                    }
                }
            },
            hasFollowed: {
                $cond: {
                    if: { $eq: [ guestLogin, true ] },
                    then: false,
                    else: {
                        $cond: {
                            if: { $eq: [ undefinedCase(loggedUser, ["_id"], null), "$authorDetail._id" ] },
                            then: true,
                            else: {
                                $in: [ "$authorDetail._id", followingIds ]
                            }
                        }
                    }
                }
            },
            sortCond: {
                $cond: {
                    if: {
                        $eq: [
                            {
                                $cond: {
                                    if: { $eq: [ guestLogin, true ] },
                                    then: false,
                                    else: {
                                        $cond: {
                                            if: { $eq: [ undefinedCase(loggedUser, ["_id"], null), "$authorDetail._id" ] },
                                            then: true,
                                            else: {
                                                $in: [ "$authorDetail._id", followingIds ]
                                            }
                                        }
                                    }
                                }
                            }, 
                            true
                        ]
                    },
                    then: {
                        $sum: [
                            {
                                $sum: [
                                    172800000,
                                    { $toLong: "$createdAt" },
                                ]
                            },
                            {
                                $multiply: [
                                    86400000,
                                    { $size: "$likedBy" }
                                ]
                            }
                        ]
                    },
                    else: {
                        $sum: [
                            { $toLong: "$createdAt" }, 
                            {  
                                $multiply: [
                                    86400000,
                                    { $size: "$likedBy" }
                                ]
                            }
                        ]
                    },
                }
            }
        }
    })

    stages.push({
        $sort : {
            // hasFollowed: -1,
            // sortCond: -1,
            createdAt: -1,
            // totalLikes : -1,
        }
    });

    const aggregatedPosts = PostModel.aggregate(stages);
    return PostModel.aggregatePaginate(aggregatedPosts, optAggregate);
};

const findPostById = async (postId) => {
    return await PostModel.findOne({_id: postId});
};

const deletePostById = async postId => {
    return PostModel.findByIdAndRemove(postId);
};

const updatePostRepository = async ( filter, { set, unset, pull, push, addToSet }, options ) => {
    return PostModel.updateOne(
        {
            ...filter
        },
        {
            $set: { ...set },
            $unset: { ...unset },
            $currentDate: { ...currentDate },
            $inc: { ...inc },
            $min: { ...min },
            $max: { ...max },
            $mul: { ...mul },
            $rename: { ...rename },
            $setOnInsert: { ...inc },
        
            /** @see https://docs.mongodb.com/manual/reference/operator/update-array/ */
            // $addToSet?: OnlyFieldsOfType<TSchema, any[], any> & AnyObject;
            // $pop?: OnlyFieldsOfType<TSchema, ReadonlyArray<any>, 1 | -1> & AnyObject;
            // $pull?: OnlyFieldsOfType<TSchema, ReadonlyArray<any>, any> & AnyObject;
            // $push?: OnlyFieldsOfType<TSchema, ReadonlyArray<any>, any> & AnyObject;
            // $pullAll?: OnlyFieldsOfType<TSchema, ReadonlyArray<any>, any> & AnyObject;
        },
        {
            ...options
        }
    )
};

export const findPostByIdPromise = (postId) => {
    return PostModel.findOne({
        _id: postId
    });
};


export const getReportDetailViaPost = async (postId) => {
    const stages = [];

    stages.push({
        $match: {
            _id: objectId(postId)
        }
    });

    stages.push({
        $lookup: {
            from: "reports",
            let: { postId: "$_id" },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: [ "$identity", "$$postId" ]
                        }
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        let: { userId: "$reportedBy" },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $eq: [ "$_id", "$$userId" ]
                                    }
                                }
                            },
                            {
                                $project: {
                                    ...projections.USER.PROFILE_PICTURE,
                                    _id: 1,
                                    fullName: { $concat: [ "$firstName", " ", "$lastName" ] }
                                }
                            }
                        ],
                        as: "reporter"
                    }
                },
                {
                    $unwind: "$reporter"
                }
            ],
            as: "reportList"
        }
    });

    stages.push({
        $lookup: {
            from: "users",
            let: { userId: "$author" },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: [ "$_id", "$$userId" ]
                        }
                    }
                },
                {
                    $project: {
                        ...projections.USER.PROFILE_PICTURE,
                        _id: 1,
                        fullName: { $concat: [ "$firstName", " ", "$lastName" ] }
                    }
                }
            ],
            as: "author"
        }
    });

    stages.push({
        $unwind: {
            path: "$author",
            preserveNullAndEmptyArrays: true
        }
    });

    stages.push({
        $project: {
            _id: 1,
            title: 1,
            author: 1,
            media: {
                $map:{
                    input: "$media",
                    as: "row",
                    in: {
                        _id: "$$row._id",
                        mediaType: "$$row.mediaType",
                        mediaUrl: {
                            originalUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.originalUrl" ] },
                            smallUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.smallUrl" ] },
                            mediumUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.mediumUrl" ] },
                            largeUrl: { $concat: [ getBaseUrl("aws"), "$$row.mediaUrl.largeUrl" ] },
                        },
                    }
                }
            },
            description: 1,
            location: 1,
            totalLikes: { $size: "$likedBy" },
            reportList: 1
        }
    });

    const reportDetail = await PostModel.aggregate(stages);

    return reportDetail.length > 0 ? reportDetail.pop() : null;
};

const deletePosts = async userId => {
    return PostModel.deleteMany({
        author: userId
    });
};
export {
    findPostById,
    getAllPost,
    deletePostById,
    deletePosts
};
