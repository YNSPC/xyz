import ReviewModel from "../database/models/Review";
import mongoose from "mongoose";

const listReviewsOfPlace = async placeId => {
    let stages = [];

    stages.push({
        $match: {
            placeId: mongoose.Types.ObjectId(placeId)
        }
    });

    stages.push({
        $lookup: {
            from: "users",
            let: { uid: "$userId" },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: ["$_id", "$$uid"]
                        }
                    }
                }
            ],
            as: "userDetail"
        }
    });

    stages.push({
        $sort: {
            createdAt: -1
        }
    });

    stages.push({
        $project: {
            reviewId: "$_id",
            userDetail: { $ifNull: [ { $arrayElemAt: [ "$userDetail", 0 ] }, null ] },
            reviewedAt: "$createdAt",
            comment: "$review",
            rating: 1
        }
    });

    return ReviewModel.aggregate(stages);
};

const createReview = async (payload, user) => {
    const reviewObject = new ReviewModel({ userId: user._id, ...payload });
    return reviewObject.save();
};

const updateReview = async (reviewId, payload) => {
    return ReviewModel.findByIdAndUpdate(
        { _id: reviewId },
        {
            $set: payload
        }
    );
}

const deleteReview = async reviewId => {
    return ReviewModel.findByIdAndDelete(
        { _id: reviewId },
    );
}

const hasThisPlaceBeenReviewed = async (placeId, user) => {
    return ReviewModel.findOne(
        {
            placeId,
            userId: user._id
        }
    );
}

const deleteReviews = async (userId) => {
    return ReviewModel.deleteMany(
        { userId: userId },
    );
}

export default {
    createReview,
    updateReview,
    deleteReview,
    listReviewsOfPlace,
    hasThisPlaceBeenReviewed,
    deleteReviews
};
