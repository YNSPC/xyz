import TokenModel from "../database/models/Token";

export const createToken = async tokenObject => {
    // const tokenObj = new TokenModel(tokenObject);
    // const tokenRow = await tokenObj.save();
    // return tokenRow.toObject();
    const {
        tokenHolderType,
        tokenType,
        tokenHolder,
        ...rest
    } = tokenObject
    return TokenModel.updateOne({
        tokenHolderType,
        tokenType,
        tokenHolder
    }, {
        $set: {
            ...rest
        }
    }, {
        upsert: true,
        new: true
    })
};

export const findByToken = async token => TokenModel.findOne({ token }).exec();

export const findByTokenHolderAndType = async (tokenHolder, tokenType) => (await TokenModel.findOne({
    tokenHolder,
    tokenType
}).exec()).toObject();

export const removeToken = async token => TokenModel.findOneAndDelete({ token }).exec();
