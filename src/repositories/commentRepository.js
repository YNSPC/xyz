import CommentModel from "../database/models/Comments";
import { getBaseUrl } from "../helpers/common-scripts";

export const findCommentById = async (commentId) => {
    return (await CommentModel.findOne({_id: commentId}).exec()).toObject();
};

export const getAllComments = (post, options, loggedUser) => {
    const optAggregate = {
        page: options.page || 1,
        limit: options.limit || 10,
        sort: {
            createdAt: 1
        },
    };

    let stages = [];

    stages.push({
        $match: {
            post: post._id,
            $or: [
                {
                    replyTo: { $exists: false },
                    replyTo: null
                }
            ]
        }
    });

    stages.push({
        $lookup: {
            from: "users",
            let: { "userId": "$commentedBy" },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: [ "$_id", "$$userId" ]
                        }
                    }
                },
                {
                    $project: {
                        _id: 1,
                        profilePicture: {
                            $cond: {
                                if: { $regexMatch: { input: "$profilePicture", regex: /http/ } },
                                then: "$profilePicture",
                                else: {
                                    $concat: [getBaseUrl(), "$profilePicture"]
                                }
                            }
                        },
                        fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
                    }
                }
            ],
            as: "commenterDetail"
        }
    });

    stages.push({
        $unwind: "$commenterDetail"
    });

    stages.push({
        $lookup: {
            from: "comments",
            let: { commentId: "$_id" },
            pipeline: [
                {
                    $match: {
                        $expr: { $eq: [ "$replyTo" , "$$commentId" ] }
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        let: { "userId": "$commentedBy" },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $eq: [ "$_id", "$$userId" ]
                                    }
                                }
                            },
                            {
                                $project: {
                                    _id: 1,
                                    profilePicture: {
                                        $cond: {
                                            if: { $regexMatch: { input: "$profilePicture", regex: /http/ } },
                                            then: "$profilePicture",
                                            else: {
                                                $concat: [getBaseUrl(),"$profilePicture"]
                                            }
                                        }
                                    },
                                    fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
                                }
                            }
                        ],
                        as: "commenterDetail"
                    }
                },
                {
                    $project: {
                        comment: 1,
                        commentedBy: { $arrayElemAt: [ "$commenterDetail", 0 ] },
                        createdAt: 1,
                        likedBy: 1,
                        totalLike: { $size: "$likedBy" },
                        hasLiked: {
                            $in: [ loggedUser._id, "$likedBy" ]
                        }
                    }
                }
            ],
            as: "repliesOnComment"
        }
    });

    stages.push({
        $project: {
            comment: 1,
            commentedBy: "$commenterDetail",
            createdAt: 1,
            likedBy: 1,
            totalLike: { $size: "$likedBy" },
            hasLiked: {
                $in: [ loggedUser._id, "$likedBy" ]
            },
            repliesOnComment: "$repliesOnComment"
        }
    });

    const aggregatedPosts = CommentModel.aggregate(stages);
    return CommentModel.aggregatePaginate(aggregatedPosts, optAggregate);
};

export const deleteComments = userId => {
    return CommentModel.deleteMany({
        commentedBy: userId
    })
}
