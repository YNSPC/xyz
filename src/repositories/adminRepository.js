import AdminModel from "../database/models/Admin";

export const adminDetail = async adminId => {
    console.log("here", adminId);
    let stages = [];
    stages.push({
        $match: {
            _id: adminId
        }
    });

    stages.push({
        $lookup: {
            from: "adminpermissions",
            localField: "role",
            foreignField: "_id",
            as: "role"
        }
    });

    stages.push({
        $unwind: {
            path: "$role"
        }
    });
    
    stages.push({
        $project: {
            password: 0
        }
    });

    const adminDetail = await AdminModel.aggregate(stages);

    return adminDetail.length > 0 ? adminDetail.pop() : null;
};
