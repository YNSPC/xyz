import moment from "moment";
import mongoose from "mongoose";
import UserModel from "../database/models/Users";
import { getBaseUrl, objectId } from "../helpers/common-scripts";
import {projections} from "../helpers/projections/projection";

export const userProfile = async (userId, loggedUser) => {
    const followedIds = loggedUser.followings.map(following => following.userId)
    const stages = [];
    stages.push({
        $match: {
            _id: userId
        }
    });

    stages.push({
        $lookup: {
            from: "tags",
            let: {tags: "$favouriteTags" },
            pipeline: [
                {
                    $match: {
                      $expr: {
                        $in: ["$_id", "$$tags"],
                      },
                    },
                },
                {
                    $project: {
                        _id: 1,
                        name: 1,
                        color: 1,
                        isPremium: 1,
                        icon: {
                            $concat: [ getBaseUrl("aws"), "$icon" ]
                        }
                    }
                }
            ],
            as: "favouriteTagsObject"
        }
    })

    // stages.push({
    //     $lookup: {
    //         from: "users",
    //         let: { userId: "$_id" },
    //         pipeline: [],
    //         as: "following"
    //     }
    // })

    // stages.push({
    //     $project: {
    //         followers: 1,
    //         followings: 1
    //     }
    // });

    stages.push({
        $project: {
            ...projections.USER.PROFILE,
            hasFollowed: { $in: [ "$_id", followedIds ] }
        }
    });

    const detail = await UserModel.aggregate(stages);
    return detail.length > 0 ? detail.pop() : null;
};

export const findUserById = async userId => UserModel.findOne({_id: userId});

export const findUserByEmail = async email => UserModel.findOne({email});

export const userDetail = async userId => {
    let stages = [];
    stages.push({
        $match: {
            _id: userId
        }
    });

    //new field to give the list of followings
    stages.push({
        $addFields: {
            followingList: {
                $map:
                    {
                        input: "$followings",
                        as: "following",
                        in: "$$following.userId"
                    }
            }
        }
    });

    const userDetail = await UserModel.aggregate(stages);

    return userDetail.length > 0 ? userDetail.pop() : null;
};

export const getUserWithFcm = async userId => {
    const stages = [];

    stages.push({
        $match: {
            _id: objectId(userId)
        }
    });

    stages.push({
        $lookup: {
            from: 'devices',
            let: { userId: '$_id' },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: ["$user", "$$userId"]
                        }
                    }
                }
            ],
            as: "devicesObject"
        }
    });

    const userData = await UserModel.aggregate(stages);

    return userData.length === 1 ?
        userData.pop().toObject() :
        null;
};

export const saveUser = async payload => {
    const user = new UserModel(payload);
    await user.save();
    return user.toObject();
};

export const followUnfollowRemoveUser = async (randomUser, logger, action) => {
    let session = await mongoose.startSession();
    let userInfo;

    switch (action) {
        case "follow":
            session.startTransaction();

            userInfo = await UserModel.findOneAndUpdate(
                {
                    _id: logger._id
                },
                {
                    $addToSet: {
                        followings: {
                            userId: randomUser._id,
                            followedAt: moment.utc().unix()
                        }
                    }
                },
                {
                    new: true,
                    useFindAndModify: false
                }
            );

            await UserModel.findOneAndUpdate(
                {
                    _id: randomUser._id
                },
                {
                    $addToSet: {
                        followers: {
                            userId: logger._id,
                            followedAt: moment.utc().unix()
                        }
                    }
                },
                {
                    new: true,
                    useFindAndModify: false
                }
            );

            await session.commitTransaction();
            break;

        case "unfollow":
            session.startTransaction();

            userInfo = await UserModel.findOneAndUpdate(
                {
                    _id: logger._id
                },
                {
                    $pull: {
                        followings: {
                            userId: randomUser._id
                        }
                    }
                },
                {
                    new: true,
                    useFindAndModify: false
                }
            );

            await UserModel.findOneAndUpdate(
                {
                    _id: randomUser._id
                },
                {
                    $pull: {
                        followers: {
                            userId: logger._id
                        }
                    }
                }
            );

            await session.commitTransaction();
            break;

        case "remove_follower":
            session.startTransaction();

            userInfo = await UserModel.findOneAndUpdate(
                {
                    _id: logger._id
                },
                {
                    $pull: {
                        followers: {
                            userId: randomUser._id
                        }
                    }
                },
                {
                    new: true,
                    useFindAndModify: false
                }
            );

            await UserModel.findOneAndUpdate(
                {
                    _id: randomUser._id
                },
                {
                    $pull: {
                        followings: {
                            userId: logger._id
                        }
                    }
                }
            );

            await session.commitTransaction();
            break;
    }

    session.endSession();

    return userInfo;
};

export const listFollowersFollowings = async (user, action) => {
    let stages = [];

    stages.push({
        $match: {
            _id: user._id
        }
    });

    if ( action === 'followings' ) {
        stages.push({
            $lookup: {
                from: "users",
                let: {
                    followings: {
                        $cond: {
                            if: { $isArray: "$followings" },
                            then: {
                                $map: {
                                    input: "$followings",
                                    as: "followings",
                                    in: "$$followings.userId"
                                }
                            },
                            else: []
                        }
                    }
                },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $in: ["$_id", "$$followings"]
                            }
                        }
                    },
                    {
                        $project: {
                            _id: 1,
                            firstName: 1,
                            lastName: 1,
                            userName: 1,
                            profilePicture: {
                                $concat: [ getBaseUrl("aws") , "$profilePicture" ]
                            }
                        }
                    }
                ],
                as: "myFollowings"
            }
        });
        stages.push({
            $project: {
                _id: 0,
                email: 1,
                myFollowings: 1
            }
        });
    }
    else {
        stages.push({
            $lookup: {
                from: "users",
                let: {
                    followers: {
                        $cond: {
                            if: { $isArray: "$followers" },
                            then: {
                                $map: {
                                    input: "$followers",
                                    as: "followers",
                                    in: "$$followers.userId"
                                }
                            },
                            else: []
                        }

                    }
                },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $in: ["$_id", "$$followers"]
                            }
                        }
                    },
                    {
                        $project: {
                            _id: 1,
                            firstName: 1,
                            lastName: 1,
                            userName: 1,
                            profilePicture: {
                                $concat: [ getBaseUrl("aws") , "$profilePicture" ]
                            }
                        }
                    }
                ],
                as: "myFollowers"
            }
        });
        stages.push({
            $project: {
                _id: 0,
                myFollowers: 1
            }
        });
    }

    const listing = await UserModel.aggregate(stages);

    if ( listing.length === 0 ) return null;

    const foundRow = listing.pop();

    return action === "followings" ? foundRow.myFollowings : foundRow.myFollowers;
};

export const updateUserModel = async (condition, payload) => UserModel.findOneAndUpdate(
    { ...condition },
    {
        $set: { ...payload }
    },
    {
        useFindAndModify: false,
        new: true,
        fields: {
            ...projections.USER.PROFILE
        }
    }
);

export const getUserDeviceToken = async userId => {
    try {
        return UserModel.aggregate([
            { $match: { _id: objectId(userId) } },
            {
                $lookup: {
                    from: 'devices',
                    localField: '_id',
                    foreignField: 'user',
                    as: 'deviceTokens',
                },
            },
            {
                $project: {
                    badge: 1,
                    deviceTokens: 1,
                    fcmTokens: {
                        $map: { input: '$deviceTokens', as: 'ar', in: '$$ar.deviceToken' },
                    },
                },
            },
        ]);
    } catch (err) {
        return { exception: err };
    }
};

export const userProfileDetail = async loggedUser => {
    const userDetail = await UserModel.findOne(
        { _id: loggedUser._id }
    )

    return userDetail;
};

export const updateUserProfile = async (payload, loggedUser) => {
    return UserModel.findOneAndUpdate(
        { _id: loggedUser._id },
        {
            $set: payload
        },
        {
            useFindAndModify: false,
            new: true,
            fields: {
                ...projections.USER.PROFILE
            }
        }
    );
};

export const userDetailWithFcm = async ({ userId }, { showTag= true, showFcm = true}) => {
    const stages = [];
    const followedUserId = typeof userId === "string" ? objectId(userId) : userId;
    stages.push({
        $match: {
            _id: followedUserId
        }
    });

    if ( showTag )
        stages.push({
            $lookup: {
                from: 'tags',
                let: { 'favouriteTags': '$favouriteTags' },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $in: ["$_id", "$$favouriteTags"]
                            }
                        }
                    },
                    /*{
                        $project: {

                        }
                    }*/
                ],
                as: "favouriteTagsObject"
            }
        });

    if ( showFcm )
        stages.push({
        $lookup: {
            from: 'devices',
            let: { userId: '$_id' },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: ["$$userId", "$user"]
                        }
                    }
                },
               /* {
                    $project: {

                    }
                }*/
            ],
            as: "devicesObject"
        }
    });

    stages.push({
        $project: {
            ...projections.USER.PROFILE,
            fcmTokens: {
                $map:{
                    input: "$devicesObject",
                    as: "loggedDevice",
                    in: "$$loggedDevice.deviceToken"
                }
            }
        }
    });

    const result = await UserModel.aggregate(stages);

    return result.length > 0 ? result.pop() : {};
}

export const commonUserWithFcm = async () => {
    const stages = [];
    stages.push({
        $lookup: {
            from: 'devices',
            let: { userId: '$_id' },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: ["$$userId", "$user"]
                        }
                    }
                }
            ],
            as: "devicesObject"
        }
    });

    stages.push({
        $project: {
            ...projections.USER.PROFILE,
            fcmTokens: {
                $map:{
                    input: "$devicesObject",
                    as: "loggedDevice",
                    in: "$$loggedDevice.deviceToken"
                }
            }
        }
    });

    return UserModel.aggregate(stages);
};

export const userOfPostWithFcm = async (postId, usersNotWanted = []) => {
    const stages = [];

    stages.push({
        $match: {
            _id: { $nin: usersNotWanted }
        }
    });

    stages.push(
        {
            $lookup: {
                from: "posts",
                let: { userId: "$_id" },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    { 
                                        $eq: [ "$_id", postId ]
                                    },
                                    {
                                        $eq: [ "$author", "$$userId" ]
                                    }
                                ]
                            }
                        }
                    }
                ],
                as: "postDetail"
            }
        }
    );

    stages.push( {
        $unwind: "$postDetail"
    });

    stages.push({
        $lookup: {
            from: 'devices',
            let: { userId: '$_id' },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: ["$$userId", "$user"]
                        }
                    }
                }
            ],
            as: "devicesObject"
        }
    });

    stages.push({
        $project: {
            ...projections.USER.PROFILE,
            fcmTokens: {
                $map:{
                    input: "$devicesObject",
                    as: "loggedDevice",
                    in: "$$loggedDevice.deviceToken"
                }
            }
        }
    });

    const list = await UserModel.aggregate(stages);
    
    return list.length > 0 ? list.pop() : null;
};

export const updateUserWithArrayModel = async (condition, payload) => UserModel.findOneAndUpdate(
    { ...condition },
    {
        $addToSet: { ...payload }
    },
    {
        useFindAndModify: false,
        new: true
    }
);

export const blockedUserList = async (loggedUser) => {
    const stages = [];

    stages.push({
        $match: {
            _id: loggedUser._id
        }
    });

    stages.push({
        $lookup: {
            from: 'users',
            let: { 'blockedList': '$blockedTo' },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $in: ["$_id", "$$blockedList"]
                        }
                    }
                },
                {
                    $project: {
                        ...projections.USER.PROFILE_PICTURE,
                        _id: 1,
                        fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
                    }
                }
            ],
            as: "blockedUserDetail"
        }
    });

    stages.push({
        $project: {
            _id: 0,
            blockedUserDetail: 1
        }
    });

    const blockedList = await UserModel.aggregate(stages);

    return blockedList.length === 0 ? null : blockedList.pop();
};

export const updateUserWithArrayModelPull = async (condition, payload) => UserModel.findOneAndUpdate(
    { ...condition },
    {
        $pull: { ...payload }
    },
    {
        useFindAndModify: false,
        new: true
    }
);

export const getMyFollowingList = async (userId) => {
    return UserModel.findOne(
        {
            _id: userId
        },
        {
            followings: 1
        }
    );
};

export const loggedUserSuggestion = async(user, search) => {
    const stages = [];
    const loggedUserFollowings = user.followings.length === 0 
        ? []
        : user.followings.map(user => user.userId);

    stages.push({
        $match: {
            _id: { $in: loggedUserFollowings }
        }
    });

    if (search && search.length >= 2) {
        stages.push({
            $match: {
                $or: [
                    {
                        firstName: {
                            $regex: search,
                            $options: "i"
                        }
                    }
                ]
            }
        })
    }

    stages.push({
        $project: {
            ...projections.USER.PROFILE_PICTURE,
            _id: 1,
            fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
        }
    });
    return UserModel
        .aggregate(stages)
        .allowDiskUse(true);
};

export const deleteAccount = async userId => {
    return UserModel.deleteOne({ _id: userId })
};

export const updatePassword = async (password, userId) => {
    return UserModel.updateOne({ _id: userId }, {
        $set: {
            password
        }
    })
}

export const getMyFollowerList = async (userId) => {
    return UserModel.findOne(
        {
            _id: userId
        },
        {
            followers: 1,
            firstName: 1,
            lastName: 1
        }
    );
};

export const usersFcmTokens = async (userIds) => {
    const stages = [];

    stages.push({
        $match: {
            _id: { 
                $in: userIds
            }
        }
    });

    stages.push({
        $lookup: {
            from: 'devices',
            let: { userId: '$_id' },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $eq: ["$$userId", "$user"]
                        }
                    }
                },
            ],
            as: "devicesObject"
        }
    });

    stages.push({
        $project: {
            ...projections.USER.PROFILE,
            fcmTokens: {
                $map:{
                    input: "$devicesObject",
                    as: "loggedDevice",
                    in: "$$loggedDevice.deviceToken"
                }
            }
        }
    });

    return await UserModel.aggregate(stages);
}

export const searchedUser = async (search, searcher) => {
    return UserModel.find({
        firstName: {
            $regex: search,
            $options: 'i'
        },
        _id: { 
            $ne: searcher._id 
        }
    }, {
        _id: 1,
        fullName: { $concat: [ "$firstName", " ", "$lastName" ] },
        profilePicture: {
            $concat: [ getBaseUrl("aws") , "$profilePicture" ]
        },
        totalPost: '19',
        totalFollower:{ $size: "$followers" },
        hasFollowed: {
            $cond: {
                if: { $in: [ "$_id", searcher.followings.map(row => row.userId) ] },
                then: true,
                else: false
            }
        }
    }).limit(5);
}